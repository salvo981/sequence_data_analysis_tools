using BenchmarkTools: @btime

root = "/home/salvocos/work_repos/sequence_data_analysis_tools"
modulePath = joinpath(root, "src/modules/seq_tools.jl")
include(modulePath)

@show root
# set the main variables and filter
dataRt = joinpath(root, "test_data/")
@show dataRt
fname = "final.contigs.500bps.fa"
# fname = "out_consensusScaffold.500bps.fa"
inpath = joinpath(dataRt, "$(fname)")
# outpath = joinpath(dataRt, "$(bname).$(minlen)bps.fa")
debug = false

@btime seq_tools.compute_fasta_stats(inpath, debug)
