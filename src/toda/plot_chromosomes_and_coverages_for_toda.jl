""" Plot coverages for Toda """

using Plots: plot, savefig, plot!
using CSV
using DataFrames
using Query
using DataStructures: OrderedDict
using DocOpt
using Logging
using Distributed

# Command line interface
doc = """
Usage: plot_coverages_toda.jl (--out-dir <directory>) (--chr-mapping <mapping>) (--raw-cov <cov-dir>) [-d]

Options:
    --chr-mapping <mapping>       TSV file with sequence names and chromosome numbers
    --out-dir <directory)>        Output directory
    --raw-cov <cov-dir>           Directory with the raw TSV coverage tables to be merged
    -d, --debug                   Debug mode.
"""



"""Generate a single TSV from multipple samtools coverage output files"""
function merge_coverage_datapoints(rawTsvDir::String, seq2chrDict::OrderedDict{String, Int}, outpath::String)::Nothing
    @debug "Merging coverage tables" rawTsvDir length(seq2chrDict) outpath

    # create the output file
    ofd::IOStream = open(outpath, "w")
    # Obtain the tables
    rawTsvPaths::Array{String, 1} = String[]
    tmpFlds::Array{String, 1} = String[]
    tmpPath::String = smplName = hdr = tmpSeq = ""
    for f in readdir(rawTsvDir)
        tmpPath = joinpath(rawTsvDir, f)
        for ln in eachline(tmpPath)
            # Skip the header
            if ln[1] == '#'
                # tables have the following names
                # ex: samtools.coverage.egg_rep1_vs_osativa.embl.tsv
                smplName = rsplit(f, "_vs_", limit=2)[1]
                # remove the file name prefix
                smplName = rsplit(smplName, ".", limit=2)[2]
                smplName = replace(smplName, "_rep" => " rep ")
                smplName = uppercasefirst(smplName)
                # write the header
                if length(hdr) == 0
                    tmpFlds = split(ln, "\t", limit=2)
                    hdr = "sample\tchr\tseq_name\t$(tmpFlds[2])"
                    write(ofd, "$(hdr)\n")
                end
            else
                # Write the information in the merged table
                tmpSeq = split(ln, "\t", limit=2)[1]
                write(ofd, "$(smplName)\t$(seq2chrDict[tmpSeq])\t$(ln)\n")
            end
        end
    end

    close(ofd)
    return nothing
end



"""Plot datapoints generated using samtools coverage"""
function plot_idxstats(idxstats::String, outpath::String)::Nothing
    @debug "plot_idxstats :: START" idxstats outpath

    # Load idx stats
    df::DataFrame = CSV.read(idxstats)
    chrs::Array{String, 1} = ["$(x)" for x in df[:Chr]]
    @show chrs

    # Plot
    plot(chrs, df[:egg_rep1_vs_osativa], label="Egg: rep 1", xlabel="Chromosome", ylabel="Portion of mapping reads", legend=:topleft)
    plot!(chrs, df[:egg_rep2_vs_osativa], label="Egg: rep 2")
    plot!(chrs, df[:sperm_rep1_vs_osativa], label="Sperm: rep 1")
    plot!(chrs, df[:sperm_rep2_vs_osativa], label="Sperm: rep 2")

    savefig(outpath)

    return nothing
end



"""Plot datapoints generated using samtools coverage"""
function plot_info_from_samtools_coverage(covdts::String, column::Symbol, xlab::String, ylab::String, outpath::String)::Nothing
    @debug "plot_info_from_samtools_coverage :: START" covdts column xlab ylab outpath

    # Load the coverages and perform the plot
    dfcov::DataFrame = CSV.read(covdts)
    smplNames::Array{String, 1} =  [x for x in Set(dfcov[:sample])]
    smplNames = sort!(smplNames)

    chrs::Array{String, 1} = String[]

    for (i, smpl) in enumerate(smplNames)
    # extract the data related to the current sample name
        smplDf = dfcov    |>
        @filter(_.sample == smpl) |>
        @map({_.chr, _.coverage, _.meandepth, _.meanbaseq, _.meanmapq,}) |> DataFrame
        sort!(smplDf)
        sort!(smplDf, [:chr], rev=false);

        # Create X labels
        if length(chrs) == 0
            chrs = ["$(x)" for x in smplDf[:chr]]
        end
        # generate a plot and save it
        if i == 1
            plot(chrs, smplDf[column], label=smpl, xlabel=xlab, ylabel=ylab, legend=:topleft)
        else
            plot!(chrs, smplDf[column], label=smpl)
        end
    end

    savefig(outpath)

    return nothing
end




#### MAIN ####

# obtain CLI arguments
args = docopt(doc)
covDir = realpath(args["--raw-cov"])
seq2chrTbl = realpath(args["--chr-mapping"])
outDir = abspath(args["--out-dir"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Creation of plots with mapping stats will be performed with the following parameters" covDir seq2chrTbl outDir

# deploy the workers
# addprocs(threads)

# import the modules
@everywhere include(joinpath(dirname(@__DIR__), "modules/systools.jl"))

# Prepare datapoints  
# The Raw Coverage TVS files from samtools coverage
# need to merged into a single TSV file."
idxstats = seq2chrTbl

# Load Chromosome numbers
seq2chrDict = OrderedDict{String, Int}()
tmpFlds = Array{String, 1}()
for ln in eachline(idxstats)
    if ln[1:3] == "Chr"
        continue
    end
    global tmpFlds = split(ln, "\t", limit=3)
    seq2chrDict[tmpFlds[1]] = parse(Int, tmpFlds[2])
end

# create output dir
systools.makedir(outDir)

# Merge the tables
covdts = joinpath(outDir, "coverage_datapoints.tsv")
merge_coverage_datapoints(covDir, seq2chrDict, covdts)


# Load the coverages and perform the plot
dfcov = CSV.read(covdts)
smplNames =  [x for x in Set(dfcov[:sample])]
smplNames = sort!(smplNames)
# print(describe(dfcov))

# set the output path for the plots
plotPath = ""
# set the output path for the plots
xLabel = "Chromosome"
yLabel = "Mean coverage"
# These are the columns that can be plotted
covColumns = [:coverage, :meandepth, :meanbaseq, :meanmapq]

for colname in covColumns
    global plotPath = joinpath(outDir, "mapping_coverages_toda.$(colname).png")
    plot_info_from_samtools_coverage(covdts, colname, xLabel, "$(colname)", plotPath)
end

# Load idx stats
df = CSV.read(idxstats)
chrs = ["$(x)" for x in df[:Chr]]
plotPath = joinpath(outDir, "idxstats_mapping_ratios.png")
plot_idxstats(idxstats, plotPath)



#=
for (i, smpl) in enumerate(smplNames)
# extract the data related to the current sample name
    smplDf = dfcov    |>
    @filter(_.sample == smpl) |>
    @map({_.chr, _.coverage}) |> DataFrame

    sort!(smplDf)
    sort!(smplDf, [:chr], rev=false);
    print(smplDf)

    # generate a plot and save it
    if i == 1
        plot(chrs, smplDf[:coverage], label=smpl, xlabel="Chromosome", ylabel="Mean coverage", legend=:topleft)
    else
        plot!(chrs, smplDf[:coverage], label=smpl)
    end
    # plot!(chrs, df[:sperm_rep1_vs_osativa], label="Sperm: rep 1")
    # plot!(chrs, df[:sperm_rep2_vs_osativa], label="Sperm: rep 2")

end

savefig(covPlotPath)
=#