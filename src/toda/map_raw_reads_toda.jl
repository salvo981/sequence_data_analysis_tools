#=
Map preprocessed reads agains the reference genome
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging
using DataStructures: OrderedDict


# Command line interface
doc = """
Usage: clean_reads_toda.jl (-m <mapping>) (-r <root>) (-g <ref_genome>) [-d -t <cpus>]

Options:
    -r <root)>                          Root directory in which the analysis was perfomed
    -m <mapping>                        Table file with sample names
    -g <ref_genome)>                    Path to the Botie2 DB with the reference sequences
    -t <cpus>, --threads <cpus>         Number of threads [default: 4]
    -d, --debug                         Debug mode.
"""



"""Read table with sample names.
    Prepare dictionary with directories in the analysis.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    smplName::String = ""
    tmpdir::String = ""

    # process the metatable
    # Each line contain lines as follow
    # old_smpl_name new_smpl_name lane raw_r1 raw_r2 renamed_r1 renamed_r2
    # 1egg_gdna1 egg_rep1 1 /path/to/raw/read/r1.qf.fastq.gz /path/to/raw/read/r2.qf.fastq.gz egg_rep1_l1_r1.fastq egg_rep1_l1_r2.fastq
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1:5] == "old_s"
            continue
        end

        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=7)
        smplName = flds[2]
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, smplName)
        if !isdir(tmpDir)
            @warn "The run $(smplName) is missing" tmpDir
        end
        # Add the Path to the dictionary even if it is missing
        outDict[smplName] = Dict{String, String}("clean_reads"=>joinpath(outRoot, "$(smplName)/clean_reads"), "mapping"=>joinpath(outRoot, "$(smplName)/mapping"))
    end

    return outDict
end


#### MAIN ####

# obtain CLI arguments
args = docopt(doc)
rootDir = realpath(args["-r"])
raw2smplTbl = realpath(args["-m"])
refGenome = abspath(args["-g"])
threads = parse(Int, args["--threads"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Reads mapping will be performed with the following parameters" rootDir raw2smplTbl refGenome threads

# add Path to the modules directory
# push!(LOAD_PATH, joinpath(dirname(@__DIR__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# import the modules
@everywhere include(joinpath(dirname(@__DIR__), "modules/systools.jl"))
@everywhere include(joinpath(dirname(@__DIR__), "modules/seq_tools.jl"))
@everywhere include(joinpath(dirname(@__DIR__), "modules/readAlignments.jl"))


# Create the output directory
systools.makedir(rootDir)

# Define tmp variables
cleanL1R1 = cleanL1R2 = cleanL2R1 = cleanL2R2 = concatR1 = concatR2 = ""
smplDir = workDir = tmpPath =""
rawSamPath = ""
rcnt1 = rcnt2 = 0
# Directory for current processing step (e.g., preprocessing)
procDir = ""

# Set directory structure
smplPathsDict = extract_run_names(raw2smplTbl, rootDir)
# For each sample name associate the raw reads that should be concatenated
mergedLanesPaths = OrderedDict{String, NamedTuple}()
# Prepare the paths of reads that should be concatenated
for (k, v) in smplPathsDict
    if !haskey(mergedLanesPaths, k)
        tmpDir = joinpath(v["clean_reads"])
        # check the clean read files exis
        global cleanL1R1 = joinpath(tmpDir, "$(k)_l1_r1.clean.fastq.gz")
        global cleanL1R2 = joinpath(tmpDir, "$(k)_l1_r2.clean.fastq.gz")
        global cleanL2R1 = joinpath(tmpDir, "$(k)_l2_r1.clean.fastq.gz")
        global cleanL2R2 = joinpath(tmpDir, "$(k)_l2_r2.clean.fastq.gz")
        # add the tuple to the dictionary
        if isfile(cleanL1R1) && isfile(cleanL1R2) && isfile(cleanL2R1) && isfile(cleanL2R2)
            mergedLanesPaths[k] = (l1r1=cleanL1R1, l1r2=cleanL1R2, l2r1=cleanL2R1, l2r2=cleanL2R2)
        else
            @error "One of the clean read sets us missing!" cleanL1R1 cleanL1R2 cleanL2R1 cleanL2R2
            exit(-2)
        end
    end
end

# Start the mapping
for (smplName, readsToConcat) in mergedLanesPaths

    # Prepare the mapping directory
    global procDir = smplPathsDict[smplName]["mapping"]
    systools.makedir(procDir)
    global smplDir = joinpath(rootDir, smplName)
    println("\nMapping raw reads for sample $(smplName)...")
    # Temporary work directory
    global workDir = mktempdir(procDir, prefix="mapping_work_$(smplName)_", cleanup=true)
    # Concatenate R1 datasets
    global concatR1 = joinpath(workDir, "$(smplName)_r1.clean.fastq")
    global rcnt1 = seq_tools.merge_reads(String[readsToConcat.l1r1, readsToConcat.l2r1], concatR1, "fastq", maxreads=0, threads=threads, compress=false)
    # Concatenate R2 datasets
    global concatR2 = joinpath(workDir, "$(smplName)_r2.clean.fastq")
    global rcnt2 = seq_tools.merge_reads(String[readsToConcat.l1r2, readsToConcat.l2r2], concatR2, "fastq", maxreads=0, threads=threads, compress=false)

    # Set the path to raw alignment file
    global rawSamPath = joinpath(workDir, "$(smplName)_vs_$(basename(refGenome)).sam")
    @info "Bowtie2 execution" rcnt1 rcnt2 refGenome rawSamPath
    # Perform the mapping
    readAlignments.map_reads_bowtie2(concatR1, concatR2, refGenome, rawSamPath, threads=threads)

    # Move the Sam file and log file into the mapping directory
    global tmpPath = joinpath(procDir, basename(rawSamPath))
    mv(rawSamPath, tmpPath, force=true)

    # Move the log files
    for f in readdir(workDir)
        if occursin(".bowtie2.", f)
            mv(joinpath(workDir, f), joinpath(procDir, f), force=true)
        end
    end

end
