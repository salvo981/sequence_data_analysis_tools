#=
Pre-process RAW reads for the project from Erika Toda from Tokyo Metropolitan university
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging
using DataStructures


# Command line interface
doc = """
Usage: clean_reads_toda.jl (-m <mapping>) (-r <reads_dir>) (-o <out_dir>) [-d -t <cpus>]

Options:
    -r <reads_dir)>                             Directory with raw reads
    -m <mapping>                                Table file for sample names
    -o <out_dir>                                Output directory [default: .]
    -t <cpus>, --threads <cpus>                 Number of threads [default: 4]
    -d, --debug                                 Debug mode.
"""

"""Maps original raw reads names to sample names containing the meta-data
   Outputs a table with the sample name mapping (only for the samples with existing raw reads)
"""
function map_raw_reads_names(mappingtbl::String, readsdir::String, outdir::String)::String
    @debug "map_raw_reads_names :: START" mappingtbl readsdir outdir

    # open the outpuyt file
    outTbl::String = joinpath(outdir, "toda_mapped_raw_dataset_names.tsv")
    ofd::IOStream = open(outTbl, "w")
    write(ofd, "old_smpl_name\tnew_smpl_name\tlane\traw_r1\traw_r2\trenamed_r1\trenamed_r2\n")

    # tmp variables
    smplIdx::String = smplType = smplRep = smplDir = laneNumber = ""
    newSmplName::String = mappingSmplName = newNameR1 = newNameR2 = ""
    oldSmplName::String = oldRawPathR1 = oldRawPathR2 = ""
    flds::Array{String, 1} = String[]

    # each line contains the following information
    # smpl_name: original sample name
    # index: used to match the input directory
    # type: sperm or egg
    # replicate: 1 or 2
    # directory: directory containing the original dataset
    # lane: sequencer's lane
    # original_r1: path to original raw read file for R1
    # original_r2: path to original raw read file for R2
    # new_smpl_name: sample name used for the processed data
    for ln in eachline(open(mappingtbl, "r"))
        # skip hdr
        if ln[1:5] == "smpl_"
            continue
        end

        # The raw reads are all named based on the apttern 
        # s_X_Y_sequence.qf.fastq.gz where:
        # X is the lane number
        # Y is one between R1 or R2 in the paured-end set
        # extract the required information for the each sample
        flds = split(ln, "\t")
        # match the pair of read files for each sample
        oldSmplName = flds[1]
        smplIdx = flds[2]
        smplType = flds[3]
        smplRep = flds[4]
        smplDir = flds[5]
        laneNumber = flds[6]
        oldRawPathR1 = joinpath(readsdir, "$(smplDir)/$(flds[7])")
        oldRawPathR2 = joinpath(readsdir, "$(smplDir)/$(flds[8])")
        mappingSmplName = flds[9]
        # Set the new sample name
        newSmplName = "$(smplType)_rep$(smplRep)"

        # Make sure the new sample name and the one in the mapping table are matching
        if "$(newSmplName)_l$(laneNumber)" != mappingSmplName
            @error "The sample name in the mapping table is the different from the inferred one" newSmplName laneNumber "$(newSmplName)_l$(laneNumber)" mappingSmplName
            exit(-6)
        end

        # Make sure the directory is the correct one
        # For example if index is U5
        # then the directory must be TaKaRa-UDI-U5_ATCCACTG-AGGTGCGT
        if !occursin("-UDI-$(smplIdx)", smplDir)
            @error "The sample index and directory name are not matching" smplIdx smplDir
            exit(-6)
        end

        # Set the new names for the raw reads
        newNameR1 = "$(newSmplName)_l$(laneNumber)_r1.fastq"
        newNameR2 = "$(newSmplName)_l$(laneNumber)_r2.fastq"

        # Write the output line only if both read files exist
        if (isfile(oldRawPathR1) && isfile(oldRawPathR2))
            write(ofd, @sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\n", oldSmplName, newSmplName, laneNumber, oldRawPathR1, oldRawPathR2, newNameR1, newNameR2))
        else
            if !isfile(oldRawPathR1)
                @warn "Missing raw read set R1" oldRawPathR1
            end
            if !isfile(oldRawPathR2)
                @warn "Missing raw read set R2" oldRawPathR2
            end
        end
    end

    # close output and return the path to the table
    close(ofd)
    return outTbl

end



"""Read table with sample names.
    Prepare dictionary with directories in the analysis.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    smplName::String = ""
    tmpdir::String = ""

    # process the metatable
    # Each line contain lines as follow
    # old_smpl_name new_smpl_name lane raw_r1 raw_r2 renamed_r1 renamed_r2
    # 1egg_gdna1 egg_rep1 1 /path/to/raw/read/r1.qf.fastq.gz /path/to/raw/read/r2.qf.fastq.gz egg_rep1_l1_r1.fastq egg_rep1_l1_r2.fastq
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1:5] == "old_s"
            continue
        end

        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=7)
        smplName = flds[2]
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, smplName)
        if !isdir(tmpDir)
            @warn "The run $(smplName) is missing" tmpDir
        end
        # Add the Path to the dictionary even if it is missing
        outDict[smplName] = Dict{String, String}("clean_reads"=>joinpath(outRoot, "$(smplName)/clean_reads"), "mapping"=>joinpath(outRoot, "$(smplName)/mapping"))
    end

    return outDict
end



# obtain CLI arguments
args = docopt(doc)
readsDir = realpath(args["-r"])
mappingTbl = realpath(args["-m"])
outDir = abspath(args["-o"])
threads = parse(Int, args["--threads"])
debug = args["--debug"]

# Create and activate the logger

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Reads cleaning will be performed with the following parameters" readsDir mappingTbl outDir threads

# add Path to the modules directory
# push!(LOAD_PATH, joinpath(dirname(@__DIR__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# import the modules
# @everywhere include(joinpath(dirname(@__FILE__), "modules/mmseqs_tools.jl"))
@everywhere include(joinpath(dirname(@__DIR__), "modules/systools.jl"))
@everywhere include(joinpath(dirname(@__DIR__), "modules/preproc_reads.jl"))

# Create the output directory
systools.makedir(outDir)

# Map metadata
raw2smplTbl = map_raw_reads_names(mappingTbl, readsDir, outDir)
# Set directory structure
smplPathsDict = extract_run_names(raw2smplTbl, outDir)

# Define tmp variables
rawR1 = rawR2 = newRawR1 = newRawR2 = ""
smplName = smplDir = workDir = lane = ""

# Directory for current processing step (e.g., preprocessing)
procDir = ""

# Set directory with test files
testDataDir = joinpath(rsplit(@__DIR__, "/", limit=3)[1], "test_data")

# Load the file with the path and clean the reads
for ln in eachline(raw2smplTbl)
    # Skip hdr
    if ln[1:5] == "old_s"
        continue
    end
    
    # extract the info and perform the trimming
    tmpFlds = split(ln, "\t", limit=8)
    global smplName = string(tmpFlds[2])
    global lane = tmpFlds[3]
    global rawR1 = string(tmpFlds[4])
    global rawR2 = string(tmpFlds[5])
    global newRawR1 = string(tmpFlds[6])
    global newRawR2 = string(tmpFlds[7])

    # Create the directories if needed
    smplDir = joinpath(outDir, smplName)
    systools.makedir(smplDir)
    procDir = smplPathsDict[smplName]["clean_reads"]
    systools.makedir(procDir)

    println("\nCleaning raw reads for sample $(smplName)_l$(lane)...")
    # Temporary work directory
    global workDir = mktempdir(procDir, prefix="fastp_work_$(smplName)_l$(lane)_", cleanup=true)
    
    # @show rawR1
    # @show rawR2

    # TEST
    tr1 = joinpath(testDataDir, "fastq/metagenome_10K_R1.fastq.gz")
    tr2 = joinpath(testDataDir, "fastq/metagenome_10K_R2.fastq.gz")

    @show tr1 tr2 newRawR1 newRawR2

    # set complete path for the new read names
    newRawR1 = joinpath(workDir, "$(newRawR1).gz")
    newRawR2 = joinpath(workDir, "$(newRawR2).gz")
    @show newRawR1 newRawR2
    cp(rawR1, newRawR1)
    cp(rawR2, newRawR2)
    # tpl = preproc_reads.clean_fastp(tr1, tr1, workDir, outPrefix="$(smplName)_l$(lane)", avgQual=28, threads=threads, repTitle="$(smplName)_l$(lane)", outHtml=true)
    tpl = preproc_reads.clean_fastp(newRawR1, newRawR2, workDir, outPrefix="$(smplName)_l$(lane)", avgQual=28, threads=threads, repTitle="$(smplName)_l$(lane)", outHtml=true)
    
    # Store processed reads
    preproc_reads.store_fastp_output(workDir, procDir, smplName, compress=true, threads=threads)
end
