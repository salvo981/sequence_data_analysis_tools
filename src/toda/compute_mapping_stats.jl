#=
Compute mapping stats
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging
using DataStructures: OrderedDict, SortedDict


# Command line interface
doc = """
Usage: compute_mapping_stats.jl (-m <mapping>) (-r <root>) (-g <ref_genome>) (-i <genome_info_tbl>) [-d -t <cpus>]

Options:
    -r <root)>                    Root directory in which the analysis was perfomed
    -m <mapping>                  Table file with sample names
    -i <genome_info_tbl>              Table file with sequences and chromosomes names and sizes
    -g <ref_genome)>              Path to the FASTA reference sequences
    -t <cpus>, --threads <cpus>   Number of threads [default: 4]
    -d, --debug                   Debug mode.
"""



"""Read table with sample names.
    Prepare dictionary with directories in the analysis.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    smplName::String = ""
    tmpdir::String = ""

    # process the metatable
    # Each line contain lines as follow
    # old_smpl_name new_smpl_name lane raw_r1 raw_r2 renamed_r1 renamed_r2
    # 1egg_gdna1 egg_rep1 1 /path/to/raw/read/r1.qf.fastq.gz /path/to/raw/read/r2.qf.fastq.gz egg_rep1_l1_r1.fastq egg_rep1_l1_r2.fastq
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1:5] == "old_s"
            continue
        end

        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=7)
        smplName = flds[2]
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, smplName)
        if !isdir(tmpDir)
            @warn "The run $(smplName) is missing" tmpDir
        end
        # Add the Path to the dictionary even if it is missing
        outDict[smplName] = Dict{String, String}("clean_reads"=>joinpath(outRoot, "$(smplName)/clean_reads"), "mapping"=>joinpath(outRoot, "$(smplName)/mapping"))
    end

    return outDict
end



#=
""" Store files generated from preprocessing pipeline into the appropriate directories."""
function store_clean_reads(workDir::String, outDir::String, smplName::String; compress::Bool=true, threads::Int=4)::Nothing
    @debug "store_annotation_files :: START" workDir outDir smplName compress threads
    
    tmpOutPath::String = tmpInPath = ""
    # Make sure input and output directories differ
    if workDir == outDir
        @error "Input and output directories must be different." workDir outDir
        exit(-6)
    end

    # For each file in the work directory, if it starts
    # with the sample name than it should be kept
    for f in readdir(workDir)
        if startswith(f, "$(smplName)")
            tmpInPath = joinpath(workDir, f)
            # kofamscan file
            if occursin(r".clean.fastq", f)
                tmpOutPath = joinpath(outDir, f)
            # log file
            elseif endswith(f, r".fastp.log.txt")
                tmpOutPath = joinpath(outDir, f)
            else
                continue
            end
            # Move or compress the file
            if compress
                systools.pigz_compress(tmpInPath, tmpOutPath, level=9, keep=true, force=true, useGzip=true, errLog=false, threads=threads)
            else
                mv(tmpInPath, tmpOutPath, force=true)
            end
        elseif f == "fastp_reports"
            tmpInPath = joinpath(workDir, f)
            tmpOutPath = joinpath(outDir, f)
            mv(tmpInPath, tmpOutPath, force=true)
        end
    end

end
=#


# obtain CLI arguments
args = docopt(doc)
rootDir = realpath(args["-r"])
raw2smplTbl = realpath(args["-m"])
genInfoTbl = realpath(args["-i"])
refGenome = abspath(args["-g"])
threads = parse(Int, args["--threads"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Mapping stats computation will be performed with the following parameters" rootDir raw2smplTbl genInfoTbl refGenome threads

# add Path to the modules directory
# push!(LOAD_PATH, joinpath(dirname(@__DIR__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# import the modules
@everywhere include(joinpath(dirname(@__DIR__), "modules/systools.jl"))
# @everywhere include(joinpath(dirname(@__DIR__), "modules/seq_tools.jl"))
@everywhere include(joinpath(dirname(@__DIR__), "modules/readAlignments.jl"))

# Create the output directory
systools.makedir(rootDir)
# Short name for the reference genome
refGenName = rsplit(basename(refGenome), ".", limit=2)[1]

# Define tmp variables
rawSamPath = bamSorted = bamIdx = tmpPath = tmpStr = ""
smplDir = workDir = ""
# Directory for current processing step (e.g., preprocessing)
procDir = ""

# Set directory structure
smplPathsDict = extract_run_names(raw2smplTbl, rootDir)
# Load chromosome names amd sizes
# Create table with chromosome sizes
seq2sizeTbl = joinpath(rootDir, "chromosome_sizes.tsv")
tmpFlds = String[]
# Lines in the table are as follows
# accession sequence-name sequence-length sequence-role replicon-name replicon-type assembly-unit
# AP014957.1 1 43270923 assembled-molecule 1 Chromosome Primary Assembly
# Headers in the FASTA file and BAM file have the format
# ENA|AP014957|AP014957.1
# So extra part on the left must be added to the dicitonary key
cnt = 1
ofd = open(seq2sizeTbl, "w")
# Extract the sizes for the first 12 sequences
for ln in eachline(genInfoTbl)
    # skip hdr
    if ln[1] == 'a'
        continue
    end
    global tmpFlds = split(ln, "\t", limit=4)
    if cnt <= 12
        write(ofd, "ENA|$(tmpFlds[1][1:end-2])|$(tmpFlds[1])\t$(tmpFlds[3])\n")
        global cnt += 1
    else
        break
    end
end
close(ofd)
cnt = 0

# Stat files
statPaths = String[]

# Start the mapping
for (smplName, runDirs) in smplPathsDict
    # Prepare the mapping directory
    global procDir = smplPathsDict[smplName]["mapping"]
    global rawSamPath = joinpath(runDirs["mapping"], "$(smplName)_vs_$(refGenName).sam")
    systools.makedir(procDir)
    global smplDir = joinpath(rootDir, smplName)
    println("\nComputing mapping stats for sample $(smplName)...")
    # Temporary work directory
    global workDir = mktempdir(procDir, prefix="mapping_stats_work_$(smplName)_", cleanup=true)
    global bamSorted = joinpath(workDir, "$(smplName)_vs_$(refGenName).bam")
    global bamIdx = joinpath(workDir, "$(smplName)_vs_$(refGenName).bam.bai")
    readAlignments.samtools_sort(rawSamPath, bamSorted, indexbam=true, threads=threads)
    # Compute main stats
    global statPaths = readAlignments.samtools_compute_stats(bamSorted, workDir, outprefix="$(smplName)_vs_$(refGenName)", refpath=refGenome, plotstats=true, threads=threads)
    
    
    # Compute  genome-wide coverage
    global tmpPath = readAlignments.samtools_coverage(bamSorted, workDir, refGenome, outprefix="$(smplName)_vs_$(refGenName)")
    pushfirst!(statPaths, tmpPath)

    # Compute genome coverages using different window sizes
    for wsize in [5000, 10000, 25000, 50000, 100000]
        global tmpPath = readAlignments.samtools_coverage_windowed(bamSorted, workDir, seq2sizeTbl, refGenome, window=wsize, outprefix="$(smplName)_vs_$(refGenName)")
        pushfirst!(statPaths, tmpPath)
    end
    # Move stat files to the main directory
    for p in statPaths
        mv(p, joinpath(procDir, basename(p)))
    end

    # Reset paths Array
    empty(statPaths)
    # Move BAM  and BAM idx to the main output directory
    mv(bamSorted, joinpath(procDir, basename(bamSorted)))
    mv(bamIdx, joinpath(procDir, basename(bamIdx)))

    # break

end
