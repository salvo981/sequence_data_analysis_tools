#=
Complete pipeline for each sample using MetaWRAP to automate many of the steps
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging
using DataStructures

# Command line interface
doc = """
Usage: metagenomic_analysis_saijo.jl (-i <in_tbl>) (-r <reads_dir>) (-o <out_dir>) (--dbs <dir_path>) [-d -t <cpus> -m <mem> -s <reads> --min-bin-comp <minbcomp> --max-bin-contami <maxbcontami>]

Options:
    -r <reads_dir)>, --reads-dir <reads_dir>    Directory with raw reads
    -i <metadata>, --tbl                        Table with metadata regarding the raw reads
    --dbs <dir_path>                            Directory in which the databases should be stored
    -m <mem>, --memory <mem>                    Avaliable memory in gigabytes [default: 250]
    -o <out_dir>, --output-dir                  Output directory [default: .]
    -t <cpus>, --threads <cpus>                 Number of threads [default: 4]
    -s <reads>, --subset <reads>                Number of reads subset of original input to be used [default: 0]
    --min-bin-comp <minbcomp>                   Minimum binning completeness (used for bin refinement) [default: 70]
    --max-bin-contami <maxbcontami>             Maximum binning cocontamination (used for bin refinement) [default: 10]
    -d, --debug                                 Debug mode.
"""



"""Read the files names in the input reads directory.
    Extract the exeprimental condition and sample names, create a list with the sets that should be merged.
    There must be 6 read sets for each experimental condition.
"""
function create_mergeable_sets(metaTbl::String, readsdir::String, outTbl::String, debug::Bool=false)
    if debug
        println("\ncreate_mergeable_sets@$(@__MODULE__) :: START")
        @show metaTbl
        @show readsdir
        @show outTbl
    end

    # output directory
    outdir::String = dirname(outTbl)
    # Dictionary reads info
    mergedSmplDict::OrderedDict{String, Queue{Tuple{String, String, String}}} = OrderedDict{String, Queue{Tuple{String, String, String}}}()
    # open the outpuyt file
    ofd::IOStream = open(outTbl, "w")
    write(ofd, "set_name\texperimental_condition\ts1r1\ts1r2\ts2r1\ts2r2\ts3r1\ts3r2\n")
    completeSmplName::String = ""
    flds::Array{String, 1} = []

    # read the metadata table for the clean dataset.
    # Following are the HDR and example line for the input table
    # original_id sample r1 r2
    # 1 S01_1st_Col-0_Ppos /dirpath/NAIST_S01_1st_1_Ppos_S1_R1_001.fastq.gz /dirpath/NAIST_S01_1st_1_Ppos_S1_R2_001.fastq.gz
    # each line contains the following information
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1] == 'o'
            continue
        end
        
        # The clean reads are expected to the followin names
        # S01_1st_Col-0_Ppos_r1.clean.fastq.gz
        # Where SXX is the sample ID that should match with the ID field in the metadata table
        # extract the required information for the each sample
        flds = split(ln, "\t", limit=3)
        # match the pair of read files for each sample
        # id = flds[1]
        experiment = flds[2]
        # remove the sample id from th experiment
        id, experiment = split(experiment, "_", limit=2)
        # set the expected read names
        # Raw reads Paths have the following patter
        # NAIST_S01_1st_1_Ppos_S1_R1_001.fastq.gz
        # NAIST_S01_1st_1_Ppos_S1_R2_001.fastq.gz
        # Where S01 corresponds to the ID 1 in the metadata table
        readPrefix = joinpath(readsdir, "$(id)_$(experiment)_")
        # @show readPrefix
        # set the path names as they would be in the original data files    
        r1 = joinpath(outDir, "$(readPrefix)r1.clean.fastq")
        if !isfile(r1)
            # check if it is compressed
            r1 = "$(r1).gz"
            # set it to null if also the compressed version does not exist
            if !isfile(r1)
                r1 = "na"
            end
        end
        # check if the reads set exists
        r2 = joinpath(outDir, "$(readPrefix)r2.clean.fastq")
        if !isfile(r2)
            # check if it is compressed
            r2 = "$(r2).gz"
            # set it to null if also the compressed version does not exist
            if !isfile(r2)
                r2 = "na"
            end
        end

        # add the entry into the dictionary
        if !haskey(mergedSmplDict, experiment)
            mergedSmplDict[experiment] = Queue{Tuple{String, String, String}}()
        end
        enqueue!(mergedSmplDict[experiment], (id, basename(r1), basename(r2)))
        # break
    end

    tmpTpl::Tuple{String, String, String} = ("", "", "")
    # Write the information in the output file
    for (exp, info) in mergedSmplDict
        id = "$(first(info)[1])-$(last(info)[1][2:3])"
        # extract reads info until the queue is empty
        write(ofd, "$(id)\t$(exp)\t")
        while !isempty(info)
            tmpTpl = dequeue!(info)
            write(ofd, "$(tmpTpl[2])\t$(tmpTpl[3])")
            if length(info) == 0
                write(ofd, "\n")
            else
                write(ofd, "\t")
            end
        end

    end
    # close output and return the path to the table
    close(ofd)
end



##### MAIN #####

# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--tbl"])
outDir = abspath(args["--output-dir"])
readsDir = realpath(args["--reads-dir"])
dbsDir = realpath(args["--dbs"])
threads = parse(Int, args["--threads"])
memory = parse(Int, args["--memory"])
maxreads = parse(Int, args["--subset"]) # if 0 all reads will be used
minbcompl = parse(Int, args["--min-bin-comp"])
maxbcontami = parse(Int, args["--max-bin-contami"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@debug """metagenomic_analysis_saijo
tblPath: $tblPath
readsDir: $readsDir
    outDir: $outDir
    dbsDir: $dbsDir
    min bins completeness:\t$minbcompl
    max bins contamination:\t$maxbcontami
    threads:\t$threads
    memory:\t$memory
    reads to be used:\t$maxreads
    """

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# Get the directory with julia scripts
# srcDir = dirname(realpath(PROGRAM_FILE))
testDataDir = joinpath(dirname(@__DIR__), "test_data")
# modulePath = joinpath(srcDir, "modules/seq_tools.jl")

# @everywhere using systools
# @everywhere include("systools.jl")


# import the modules
# @everywhere include(joinpath(dirname(@__FILE__), "modules/mmseqs_tools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/seq_tools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/readAlignments.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/metagenomes.jl"))


# create output directory
systools.makedir(outDir)
outRoot = outDir
expSetsTbl = basename(rstrip(outDir, ['/']))
expSetsTbl = joinpath(outDir, "merged_smpl_names.$(expSetsTbl).tsv")
create_mergeable_sets(tblPath, readsDir, expSetsTbl, debug)

##### TO REMOVE ###########
# testSeq = joinpath(testDataDir, "fastq/bacteria.fa")
# testSeq = joinpath(testDataDir, "fasta/bacteria.fa.gz")
# testSeq = joinpath(testDataDir, "fastq/10k_S02_1st_2_Ppos_S2_R1_001.fastq.gz")

# refFasta = "/tmp/metawrap_data/athaliana_idx/athaliana.tair10.dna_rm.fa"
# refDb = "/tmp/metawrap_data/athaliana_idx/athaliana"
# r1 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos_r1.fastq"
# r2 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos_r2.fastq"
# tmpaln = joinpath(outDir, "bowtie_test.sam")
runKraken = true
classifyBins = true

# readAlignments.create_bowtie2_idx(refFasta, dirname(refFasta), "minchia", threads)
# readAlignments.map_reads_bowtie2(r1, r2, refDb, tmpaln, threads)

# extract unmapped reads
##############################

pathsToMerge = []
compress = false
# Load the file with the path and clean the reads
for ln in eachline(expSetsTbl)
    # skip hdr
    if ln[1:2] == "se"
        continue
    end

    # extract the info and perform the trimming
    tmpFlds = split(ln, "\t", limit=3)
    mergedNamePrefix = "$(tmpFlds[1])_$(tmpFlds[2])"
    outDir = joinpath(outRoot, mergedNamePrefix)
    # skip the analysis if the output directory is already present
    if isdir(outDir)
        println("\nAnalysis for samples $(mergedNamePrefix) was previous perfomed. Skipping...")
        continue
    end

    # create the required directories
    systools.makedir(outDir)
    logsDir = joinpath(outDir, "logs")
    systools.makedir(logsDir)
    
    println("\nMerging reads for $(mergedNamePrefix)...")
    # extract the read set names
    tmpFlds = split(tmpFlds[3], "\t", limit=6)
    tmpFlds = [joinpath(readsDir, x) for x in tmpFlds]
    # Process read sets for R1
    pathsToMerge = [tmpFlds[1], tmpFlds[3], tmpFlds[5]]
    r1 = joinpath(outDir, "$(mergedNamePrefix)_r1.fastq")
    seq_tools.merge_reads(pathsToMerge, r1, "fastq", maxreads, threads, compress)
    # Merge read sets for R2
    pathsToMerge = [tmpFlds[2], tmpFlds[4], tmpFlds[6]]
    r2 = joinpath(outDir, "$(mergedNamePrefix)_r2.fastq")
    seq_tools.merge_reads(pathsToMerge, r2, "fastq", maxreads, threads, compress)
    
    # Map and extract reads
    println("\nRemoving A. thaliana contamination for $(mergedNamePrefix)...")
    refDb = joinpath(dbsDir, "athaliana_idx/athaliana")
    outSam = joinpath(outDir, "$(mergedNamePrefix).sam")
    readAlignments.map_reads_bowtie2(r1, r2, refDb, outSam, threads)
    println("\nRemoving original merged sets...")
    rm(r1, force=false)
    rm(r2, force=false)
    # this should overwrite the concatenated datasets
    rcnt = readAlignments.extract_reads(outSam, outDir, r1, r2, "", ["-f4"], threads)
    rm(outSam, force=false)
    # move the log files
    for (root, dirs, files) in walkdir(outDir)
        if root == outDir
            for f in files
                # @show f
                if (f[1:3] == "std") || (f[1:4] == "log.")
                    mv(joinpath(root, f), joinpath(logsDir, f), force=true)
                end
            end
            break
        end
    end

    # Write number of input reads
    println("Input reads:\t$(rcnt)\n")

    # @show mergedNamePrefix
    mergedNamePrefix = string(rsplit(basename(r1), "_", limit=2)[1])
    metawOutDir = joinpath(outDir, mergedNamePrefix)
    skipAssembly = false

    #### TO REMOVE #####
    #=
    # r1 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos_r1.fastq"
    # r2 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos_r2.fastq"
    r1 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos/S01-03_1st_Col-0_Ppos/clean_reads/S01-03_1st_Col-0_Ppos_1.fastq"
    r2 = "/tmp/metawrap_data/output/S01-03_1st_Col-0_Ppos/S01-03_1st_Col-0_Ppos/clean_reads/S01-03_1st_Col-0_Ppos_2.fastq"
    mergedNamePrefix = "S01-03_1st_Col-0_Ppos"
    metawOutDir = joinpath(outDir, mergedNamePrefix)
    skipAssembly = true
    @show r1
    @show r2
    @show mergedNamePrefix
    @show metawOutDir
    =#
    ####################

    @info "##### Metawrap START #####" mergedNamePrefix
    analysisDirs = metagenomes.metawrap(r1, r2, metawOutDir, mergedNamePrefix, skipAssembly, runKraken, classifyBins, maxbcontami, minbcompl, threads, memory, true)
    @info "##### Metawrap END #####" mergedNamePrefix
    # Remove temporary files
    @info "##### Run clean START #####" mergedNamePrefix
    metagenomes.metawrap_cleanup(mergedNamePrefix, analysisDirs, minbcompl, maxbcontami, threads)
    @info "##### Run clean END #####" mergedNamePrefix

    # move all the results from metawrap to the main output directory
    
    # break
end

# count the reads in parallel
# extime = @elapsed seq_tools.parallel_count_reads(rPaths, threads, debug)
# @show extime
