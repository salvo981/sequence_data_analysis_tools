"""Generate PCoA of MDS for experiment with Phosphate and without phosphate."""
import os
import sys
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.manifold import MDS
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import pairwise_distances
from typing import Dict, List, Any, Tuple
# from pca import pca



def plot_mds2d(tblpath:str, outPath:str, plotTitle:str, fmt:str="pdf") -> str:
    """Generate PCoE of MDS"""
    # Load the dataframe
    df = pd.read_csv(tblpath, delimiter="\t")
    # Extract columns with numbers
    dataCols = df.columns[1:-2]
    M = df[dataCols].to_numpy()
    # Extract vectors with sample names 
    smplNames = df[df.columns[0]]
    genotypes = df[df.columns[-1]]

    # Compute the distances
    bcDm = pairwise_distances(M, metric='braycurtis')
    mds = MDS(n_components=2, dissimilarity="precomputed", random_state=0)
    mdsFitted = mds.fit_transform(bcDm)

    # Set plot paramaters
    msize = 125
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    plt.figure()
    plt.title(plotTitle)
    plt.xlabel("MDS 1")
    plt.ylabel("MDS 2")
    plt.grid()

    for i, val in enumerate(genotypes):
        x, y = mdsFitted[i]
        plt.scatter([x] ,[y], s=msize, c=colors[i],label=val)

    plt.legend()
    tmpOutPath = f"{outPath}.mds2d.braycurtis.{fmt}"
    plt.savefig(tmpOutPath, dpi=160)



def plot_pca2d(tblpath:str, outPath:str, plotTitle:str, fmt:str="pdf") -> str:
    """Generate PCoE of PCA"""
    # Load the dataframe
    df = pd.read_csv(tblpath, delimiter="\t")
    # Extract columns with numbers
    dataCols = df.columns[1:-2]
    M = df[dataCols].to_numpy()
    # Extract vectors with sample names 
    smplNames = df[df.columns[0]]
    genotypes = df[df.columns[-1]]

    # data scaling
    scaler = StandardScaler()
    scaler.fit(M)
    Mscaled = scaler.transform(M)

    # Create and fit the data to PCA model
    pcaModel = PCA(n_components=2, random_state=0)
    # perform projections of data into PCA
    pcaFit = pcaModel.fit_transform(Mscaled)
    #### DO NOT SCALE ####
    # pcaFit = pcaModel.fit_transform(M)
    #####################

    pcaRatios = pcaModel.explained_variance_ratio_
    # extract the values and round them
    pc1r = round(pcaRatios[0] * 100., ndigits=2)
    pc2r = round(pcaRatios[1] * 100., ndigits=2)

    # Set plot paramaters
    msize = 125
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    plt.figure()
    plt.title(plotTitle)
    plt.xlabel(f"PC 1 ({pc1r}%)")
    plt.ylabel(f"PC 2 ({pc2r}%)")
    plt.grid()

    # print(pcaFit)

    for i, val in enumerate(genotypes):
        x, y = pcaFit[i]
        plt.scatter([x] ,[y], s=msize, c=colors[i],label=val)

    plt.legend()
    # plt.show()
    tmpOutPath = f"{outPath}.pca2d.{fmt}"
    plt.savefig(tmpOutPath, dpi=160)



def plot_mds3d(tblpath:str, outPath:str, plotTitle:str, fmt:str="pdf") -> str:
    """Generate PCoE of MDS"""
    # Load the dataframe
    df = pd.read_csv(tblpath, delimiter="\t")
    # Extract columns with numbers
    dataCols = df.columns[1:-2]
    M = df[dataCols].to_numpy()
    # Extract vectors with sample names 
    smplNames = df[df.columns[0]]
    genotypes = df[df.columns[-1]]

    # Compute the distances
    bcDm = pairwise_distances(M, metric='braycurtis')
    mds = MDS(n_components=3, dissimilarity="precomputed", random_state=0)
    mdsFitted = mds.fit_transform(bcDm)

    # Set plot paramaters
    msize = 125
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    fig = plt.figure()
    # plt.title(plotTitle)

    ax = fig.gca(projection='3d')
    ax.set_xlabel("MDS 1")
    ax.set_ylabel("MDS 2")
    ax.set_zlabel("MDS 3")
    ax.set_title(plotTitle)
    plt.grid()

    for i, val in enumerate(genotypes):
        x, y, z = mdsFitted[i]
        ax.scatter([x] ,[y], [z], s=msize, c=colors[i],label=val)
        # plt.scatter([x] ,[y], s=msize, c=colors[i],label=val)

    plt.legend()
    tmpOutPath = f"{outPath}.mds3d.braycurtis.{fmt}"
    plt.savefig(tmpOutPath, dpi=160)



def plot_pca3d(tblpath:str, outPath:str, plotTitle:str, fmt:str="pdf") -> str:
    """Generate PCoE of PCA"""
    # Load the dataframe
    df = pd.read_csv(tblpath, delimiter="\t")
    # Extract columns with numbers
    dataCols = df.columns[1:-2]
    M = df[dataCols].to_numpy()
    # Extract vectors with sample names 
    smplNames = df[df.columns[0]]
    genotypes = df[df.columns[-1]]

    # data scaling
    scaler = StandardScaler()
    scaler.fit(M)
    Mscaled = scaler.transform(M)

    # Create and fit the data to PCA model
    pcaModel = PCA(n_components=3, random_state=0)
    # perform projections of data into PCA
    pcaFit = pcaModel.fit_transform(Mscaled)
    #### DO NOT SCALE ####
    # pcaFit = pcaModel.fit_transform(M)
    #####################

    pcaRatios = pcaModel.explained_variance_ratio_
    # extract the values and round them
    pc1r = round(pcaRatios[0] * 100., ndigits=2)
    pc2r = round(pcaRatios[1] * 100., ndigits=2)
    pc3r = round(pcaRatios[2] * 100., ndigits=2)

    # Set plot paramaters
    msize = 125
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    fig = plt.figure()
    # plt.title(plotTitle)

    ax = fig.gca(projection='3d')
    ax.set_xlabel(f"PC 1 ({pc1r}%)")
    ax.set_ylabel(f"PC 2 ({pc2r}%)")
    ax.set_zlabel(f"PC 3 ({pc3r}%)")
    ax.set_title(plotTitle)
    plt.grid()

    for i, val in enumerate(genotypes):
        x, y, z = pcaFit[i]
        ax.scatter([x] ,[y], [z], s=msize, c=colors[i],label=val)
        # plt.scatter([x] ,[y], s=msize, c=colors[i],label=val)

    plt.legend()
    tmpOutPath = f"{outPath}.pca3d.{fmt}"
    plt.savefig(tmpOutPath, dpi=160)



def plot_pca_pca_package(tblpath:str, outPath:str, plotTitle:str) -> str:
    """Generate PCoE of PCA"""

    # Example explained in
    # https://pypi.org/project/pca/

    # Load the dataframe
    df = pd.read_csv(tblpath, delimiter="\t")
    # Extract columns with numbers
    dataCols = df.columns[1:-2]
    M = df[dataCols].to_numpy()
    # Extract vectors with sample names 
    smplNames = df[df.columns[0]]
    genotypes = df[df.columns[-1]]


    # data scaling
    scaler = StandardScaler()
    scaler.fit(M)
    Mscaled = scaler.transform(M)

    print(genotypes)
    print(type(genotypes))
    print(list(genotypes))
    print(type(list(genotypes)))

    # Create the dataframe for the analysis
    modDf = pd.DataFrame(data=Mscaled, columns=df.columns[1:-2], index=list(genotypes))

    # initiliaze and reduce the data
    pcaModel = pca(n_components=0.95)
    pcaModel = pca(n_components=3)

    # plt.figure()
    # Fit transform
    pcaFit = pcaModel.fit_transform(Mscaled)

    # generate the plot
    pcaModel.scatter3d()
    # plt.legend()
    plt.show()

    print(outPath)
    plt.savefig(outPath)


    '''
    mdsFitted = mds.fit_transform(bcDm)

    # Set plot paramaters
    msize = 125
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    plt.figure()
    plt.title(plotTitle)
    plt.xlabel("Principal component 1")
    plt.ylabel("Principal component 2")
    plt.grid()

    for i, val in enumerate(genotypes):
        x, y = mdsFitted[i]
        plt.scatter([x] ,[y], s=msize, c=colors[i],label=val)

    plt.legend()
    # plt.show()
    plt.savefig(outPath, dpi=160)
    '''



def main():
    # Set the path to table file
    indir:str = "/home/salvocos/Desktop/saijo_pca_test/"
    fname:str = "saijo_pneg_samples.kdecoder.out.mod.tsv"
    tblpath:str = os.path.join(indir, fname)
    imgFmt:str = "png"
    # imfFmt:str = "pdf"

    # Set some plotting parameters
    plt.rcParams['figure.figsize'] = [12, 12]
    plt.rc("font", size=14)


    # Generate plots for Pneg
    fname:str = "saijo_pneg_samples.kdecoder.out.mod.tsv"
    tblpath:str = os.path.join(indir, fname)
    oname:str = "saijo_pneg_samples"
    outPath:str = os.path.join(indir, oname)
    tmpTitle:str = "MDS (Bray-curtis) for Pneg samples"
    # MDS PLots 2d
    plot_mds2d(tblpath, outPath, tmpTitle, imgFmt)
    # MDS PLots 3d
    plot_mds3d(tblpath, outPath, tmpTitle, imgFmt)
    # PCA Plots 2d
    tmpTitle = "PCA for Pneg samples"
    plot_pca2d(tblpath, outPath, tmpTitle, imgFmt)
    # PCA Plots 3d
    plot_pca3d(tblpath, outPath, tmpTitle, imgFmt)

    # Generate plots for Pneg
    fname = "saijo_ppos_samples.kdecoder.out.mod.tsv"
    tblpath = os.path.join(indir, fname)
    oname = "saijo_ppos_samples"
    outPath:str = os.path.join(indir, oname)
    tmpTitle:str = "MDS (Bray-curtis) for Ppos samples"
    # MDS PLots 2d
    plot_mds2d(tblpath, outPath, tmpTitle, imgFmt)
    # MDS PLots 3d
    plot_mds3d(tblpath, outPath, tmpTitle, imgFmt)
    # PCA Plots 2d
    tmpTitle = "PCA for Ppos samples"
    plot_pca2d(tblpath, outPath, tmpTitle, imgFmt)
    # PCA Plots 3d
    plot_pca3d(tblpath, outPath, tmpTitle, imgFmt)



if __name__ == "__main__":
    main()
