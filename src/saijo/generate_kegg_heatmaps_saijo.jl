#=
Process kofam_scan output and KEGG function heatmaps using KEGG-decoder
=#

using Printf:@printf,@sprintf
using DocOpt
using Logging
using DataStructures

# Command line interface
doc = """
Usage: generate_kegg_heatmaps_saijo.jl (-t <in_tbl>) (-o <out_dir>) [-d]

Options:
    -t <metadata>, --tbl                        Table with metadata regarding the samples and bins
    -o <out_dir>, --output-root                 Output directory in which the runs are stored [default: .]
    -d, --debug                                 Debug mode.
"""




"""Read table with sample names.
    Prepare dictionary with directories used in the annotation.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    experiment::String = ""
    tmpdir::String = ""
    # process the metatable
    # Each line contain lines as follow
    # set_name	experimental_condition	s1r1	s1r2	s2r1	s2r2	s3r1	s3r2
    # S01-03	1st_Col-0_Ppos	S01_1st_Col-0_Ppos_r1.fastq.gz	S01_1st_Col-0_Ppos_r2.fastq.gz	S02_1st_Col-0_Ppos_r1.fastq.gz	S02_1st_Col-0_Ppos_r2.fastq.gz	S03_1st_Col-0_Ppos_r1.fastq.gz	S03_1st_Col-0_Ppos_r2.fastq.gz
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1] == 's'
            continue
        end
        
        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=3)
        experiment = "$(flds[1])_$(flds[2])"
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, experiment)
        if !isdir(tmpDir)
            @warn "The run $(experiment) is missing" tmpDir
            continue
        else
            outDict[experiment] = Dict{String, String}("binsdir"=>"bin_refinement", "annotdir"=>joinpath(outRoot, "$(experiment)/kofamscan_annotation"), "preddir"=>joinpath(outRoot, "$(experiment)/protein_prediction"), "binstaxa"=>joinpath(outRoot, "$(experiment)/final_bins_taxonomy"))
        end

        tmpDir = joinpath(outRoot, "$(experiment)/bin_refinement")
        # detect the directory with refined bins
        if !isdir(tmpDir)
            @error "Bins refinement missing for $(experiment)" tmpDir
            exit(-2)
        else
            for el in readdir(tmpDir)
                if isdir(joinpath(tmpDir, el))
                    if startswith(el, r"metawrap_") && endswith(el, r"_bins")
                        # found the directory
                        outDict[experiment]["binsdir"] = joinpath(outRoot, "$(experiment)/bin_refinement/$(el)")
                        break
                    end
                end
            end
        end

        # break
    end

    return outDict
end



"""
    Concatenate KEGG-Decoder input files and generate heatmap with multiple
"""
function generate_heatmap_for_multiple_samples(smplPathsDict::OrderedDict{String, Dict{String, String}}, outdir::String, outPrefix::String)::Nothing
    @debug "generate_heatmap_for_multiple_samples :: START" smplPathsDict outdir outPrefix
    # define temp vars
    kdInputFinal::String = workDir = annotDir = tmpPath = ""
    tmpKdIn::String = tmpKeggId = tmpGeneId = kdtbl = kdsvg = kdhtml = ""
    systools.makedir(outdir)
    # create tmp dir and final KEGGDecoder input
    workDir = mktempdir(outdir, prefix="kd_merging_work_", cleanup=false)
    kdInputFinal = joinpath(workDir, "$(outPrefix).kdecoder.input.taxa.tsv")

    # Create output file
    ofd::IOStream = open(kdInputFinal, "w")
    # concatenate the kegg-decoder input files
    for smpl in keys(smplPathsDict)
        println("Processing KEGG-decoder input for sample $(smpl)")
        annotDir = smplPathsDict[smpl]["annotdir"]
        tmpPath = joinpath(annotDir, "$(smpl).bins.kdecoder.input.taxa.tsv.gz")
        # @show isfile(tmpPath)
        # @show tmpPath
        if isfile(tmpPath)
            # check if it an archive
            if systools.is_archive(tmpPath)[1]
                # decompress into the output directory
                # set the output name removing the extensions
                tmpKdIn = joinpath(workDir, rsplit(basename(tmpPath), ".", limit=2)[1])
                systools.pigz_decompress(tmpPath, tmpKdIn, keep=true, force=true, errLog=true, threads=2)
            else
                # simply copy it it to the work directory
                tmpKdIn = joinpath(workDir, basename(tmpPath))
                cp(tmpPath, tmpKdIn, force=true)
            end

            # Write the lines into the merged KD input giving
            # putting only the sample name in the first column
            for ln in eachline(tmpKdIn)
                tmpGeneId, tmpKeggId = split(ln, "\t", limit=2)
                smpl = replace(smpl, "_"=>"-")
                write(ofd, "$(smpl)-$(tmpGeneId)\t$(tmpKeggId)\n")
            end
        end
    end
    # close output
    close(ofd)

    # Generate heatmaps
    kdtbl = joinpath(outdir, "$(outPrefix).kdecoder.out.tsv")
    kdhtml = joinpath(outdir, "$(outPrefix).kdecoder.out.html")
    kdsvg = joinpath(outdir, "$(outPrefix).kdecoder.out.svg")

    # KEGG-decoder -i input.tsv -o out.list.tsv -v static
    global tmpCmd = `KEGG-decoder -i $(kdInputFinal) -o $(kdtbl) -v static`
    @debug "KEGG-Decoder CMD" tmpCmd
    run(pipeline(tmpCmd), wait=true)
    # Generate the static HTML plot of there are more than 2 samples
    if length(smplPathsDict) > 1
        global tmpCmd = `KEGG-decoder -i $(kdInputFinal) -o $(kdtbl) -v interactive`
        @debug "KEGG-Decoder CMD" tmpCmd
        run(pipeline(tmpCmd), wait=true)
    end

    # remove tmp directory
    rm(workDir, recursive=true, force=true)

    return nothing
end



"""
    Map KEGGDecored input file to have the bin taxonomy instead of bins
"""
function mapbins2taxa_for_keggdecoder(keggDecoderInputBins::String, outPath::String, mappingFile::String)::Nothing
    @debug "mapbins2taxa_for_keggdecoder :: START" keggDecoderInputBins outPath mappingFile

    # Lines in the original KEGGDecoder input file look like
    # bin.1_141_18581_length_39535_cov_69.9637_1	K06217
    # bin.1_141_18581_length_39535_cov_69.9637_2	K07042
    # bin.1_141_18581_length_39535_cov_69.9637_3	K07090
    # KEGGDecoder cuts the gene ID at the first underscore '_'
    # Hence in the heat map it would be bin.1

    # Lines in the mapping file look as follows
    # bin.1.fa	Bacteria;Proteobacteria;Betaproteobacteria;Burkholderiales;Comamonadaceae;Simplicispira;Simplicispira suum
    # bin.2.fa	Bacteria;Proteobacteria;Betaproteobacteria;Burkholderiales;Oxalobacteraceae;Massilia;Massilia putida
    # Each taxonomy line can have a different depth

    # Load the mapping onto a dictionary
    bin2taxaDict::OrderedDict{String, String} = OrderedDict{String, String}()
    tmpFlds::Array{String, 1} = String[]
    tmpTaxa::String = tmpBin = ""

    for ln in eachline(mappingFile)
        tmpFlds = split(ln, "\t", limit=2)
        tmpBin = rsplit(tmpFlds[1], ".", limit=2)[1]
        if tmpFlds[2] == "Bacteria"
            @warn "This bin has no bacterial taxonomic resolution" ln
            continue
        end
        #tmpTaxa = split(tmpFlds[2], ";", limit=2)[2]
        tmpFlds = split(tmpFlds[2], ";")
        # If the are at least 4 levels, then show the order
        if length(tmpFlds) >= 4
            tmpTaxa = join(tmpFlds[4:end], ".")
        else # 1<length<4
            # Will contain the Phylum and class
            tmpTaxa = join(tmpFlds[2:end], ".")
        end
        tmpTaxa = replace(tmpTaxa, " " => "-")
        bin2taxaDict[tmpBin] = tmpTaxa
        # if occursin("Massilia.Massilia", tmpTaxa)
        #     @show ln tmpTaxa keggDecoderInputBins
        #     exit(-7)
        # end
    end

    @debug "Mapping loaded" bin2taxaDict

    # Create output file
    ofd::IOStream = open(outPath, "w")
    # concatenate the kegg-decoder input files
    for ln in eachline(keggDecoderInputBins)
        tmpFlds = split(ln, "\t", limit=2)
        tmpBin = split(tmpFlds[1], "_", limit=2)[1]
        # @show tmpBin bin2taxaDict[tmpBin]
        if haskey(bin2taxaDict, tmpBin)
            write(ofd, "$(bin2taxaDict[tmpBin]).$(ln)\n")
        end
        # break
    end
    # close output
    close(ofd)

    return nothing
end




""" Store files generated from KEGG-decoder into the appropriate directories."""
function store_per_smpl_plots(workDir::String, annotDir::String, smplName::String; compress::Bool=true)::Nothing
    @debug "store_results_files :: START" workDir annotDir smplName compress

    # temp variables
    tmpOutPath::String = tmpInPath = ""
    # For each file in the work directory, if it starts
    # with the sample name than it should be kept
    for f in readdir(workDir)
        if startswith(f, "$(smplName).bins.kdecoder.")
            tmpInPath = joinpath(workDir, f)
            tmpOutPath = joinpath(annotDir, f)
            # Move or compress the file
            if compress
                systools.pigz_compress(tmpInPath, tmpOutPath, level=9, keep=true, force=true, useGzip=true, errLog=false, threads=2)
            else
                mv(tmpInPath, tmpOutPath, force=true)
            end
        end
    end

end


##### MAIN #####

# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--tbl"])
outRoot = abspath(args["--output-root"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@debug "generate_kegg_heatmaps_saijo" tblPath outRoot

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
# addprocs(threads)

# import the modules
include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
include(joinpath(dirname(@__FILE__), "modules/keggTools.jl"))

# create output directory
smplPathsDict = extract_run_names(tblPath, outRoot)
workDir = tmpDir = tmpPath = annotDir = kdtbl = bins2taxa = tmpKeggDecoderTaxaInput = kdsvg = kdhtml = ""
tmpCmd = ``
# will contain the files paths that will be used to generate heatmap with all samples
# kdInputFiles::Array{String, 1} = Array{String, 1}()

tmpRawKofam = smplKeggDecodeInput = tmpKeggDecOut = ""

#=
# More info on KEGG decoder at https://github.com/bjtully/BioData/tree/master/KEGGDecoder
for smpl in keys(smplPathsDict)
    global annotDir = smplPathsDict[smpl]["annotdir"]
    global bins2taxa = joinpath(smplPathsDict[smpl]["binstaxa"], "bin_taxonomy.tab")
    global tmpRawKofam = joinpath(annotDir, "$(smpl).bins.kofamscan.tsv.gz")
    global workDir = mktempdir(annotDir, prefix="kegg_decoder_work_", cleanup=false)
    global tmpPath = joinpath(workDir, rsplit(basename(tmpRawKofam), ".", limit=2)[1])
    # copy the mapping file to the work directory
    cp(bins2taxa, joinpath(workDir, basename(bins2taxa)), force=true)
    bins2taxa = joinpath(workDir, basename(bins2taxa))
    @debug "Generating KEGG heatmaps for sample $(smpl)" annotDir tmpRawKofam workDir tmpPath bins2taxa
    # unarchive the raw kofam results if required
    if systools.is_archive(tmpRawKofam)[1]
        # decompress into the work directory
        # set the output name removing the extensions
        systools.pigz_decompress(tmpRawKofam, tmpPath, keep=true, force=true, errLog=true, threads=2)
        # just copy the file
    else
        cp(tmpRawKofam, tmpPath, force=true)
    end

    # update the variables
    tmpRawKofam = tmpPath
    tmpPath = joinpath(workDir, "$(smpl).bins.kdecoder.input.tsv")
    global tmpKeggDecoderTaxaInput = joinpath(workDir, "$(smpl).bins.kdecoder.input.taxa.tsv")
    global bin2taxa = joinpath(annotDir, "$(smpl).bins.kdecoder.input.taxa.tsv")
    # Generate the input for KEGG-Decoder
    # replace '.k' with '_' in the FASTA headers
    keggTools.kofamout2keggdecoder(tmpRawKofam, tmpPath, smplSeparator=".k")
    mapbins2taxa_for_keggdecoder(tmpPath, tmpKeggDecoderTaxaInput, bins2taxa)
    
    # set paths for kegg-decoder output files
    kdtbl = joinpath(workDir, "$(smpl).bins.kdecoder.out.tsv")
    kdhtml = joinpath(workDir, "$(smpl).bins.kdecoder.out.html")
    kdsvg = joinpath(workDir, "$(smpl).bins.kdecoder.out.svg")
    # Call KEGG decoder to generate the HTML report
    # KEGG-decoder run example
    # KEGG-decoder -i input.tsv -o out.list.tsv -v static
    global tmpCmd = `KEGG-decoder -i $(tmpKeggDecoderTaxaInput) -o $(kdtbl) -v interactive`
    @debug "KEGG-Decoder CMD" tmpCmd
    run(pipeline(tmpCmd), wait=true)
    # Generate the static SVG plot
    global tmpCmd = `KEGG-decoder -i $(tmpKeggDecoderTaxaInput) -o $(kdtbl) -v static`
    @debug "KEGG-Decoder CMD" tmpCmd
    run(pipeline(tmpCmd), wait=true)
    # Store the generated files
    store_per_smpl_plots(workDir, annotDir, smpl, compress=true)
    # remove the work dir
    rm(workDir, force=true, recursive=true)
    # @debug "DEBUG :: exit"
    # exit(-5)
    # break
end
=#

# Generate all requested sample combinations
combinationDir = joinpath(outRoot, "sample_combinations")
systools.makedir(combinationDir)
# generate_heatmap_for_multiple_samples(smplPathsDict, combinationDir, "all_samples")

# use a dictionary for the combinations
# 1)
# 2nd_Col-0_Pneg vs 1st_Col-0_Ppos
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S01-03_1st_Col-0_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 2)
# 1st_Col-0_Ppos vs 1st_phr1_phl1_lpr1_lpr2_Ppos
s1 = "S01-03_1st_Col-0_Ppos"
s2 = "S07-09_1st_phr1_phl1_lpr1_lpr2_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_Col-0_Pneg vs 2nd_phr1_phl1_lpr1_lpr2_Pneg
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S25-27_2nd_phr1_phl1_lpr1_lpr2_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 3)
# 1st_Col-0_Ppos vs 1st_pepr1_pepr2_phr1_phl1_Ppos
s1 = "S01-03_1st_Col-0_Ppos"
s2 = "S10-12_1st_pepr1_pepr2_phr1_phl1_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_Col-0_Pneg vs 2nd_pepr1_pepr2_phr1_phl1_Pneg
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S28-30_2nd_pepr1_pepr2_phr1_phl1_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 4)
# 1st_Col-0_Ppos vs 1st_phr1_phl1_Ppos
s1 = "S01-03_1st_Col-0_Ppos"
s2 = "S04-06_1st_phr1_phl1_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_Col-0_Pneg vs 2nd_phr1_phl1_Pneg
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S22-24_2nd_phr1_phl1_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 5)
# 1st_Col-0_Ppos vs 1st_lpr1_lpr2_Ppos
s1 = "S01-03_1st_Col-0_Ppos"
s2 = "S16-18_1st_lpr1_lpr2_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_Col-0_Pneg vs 2nd_lpr1_lpr2_Pneg
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S34-36_2nd_lpr1_lpr2_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 6)
# 1st_phr1_phl1_Ppos vs 1st_pepr1_pepr2_phr1_phl1_Ppos
s1 = "S04-06_1st_phr1_phl1_Ppos"
s2 = "S10-12_1st_pepr1_pepr2_phr1_phl1_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_phr1_phl1_Pneg vs 2nd_pepr1_pepr2_phr1_phl1_Pneg
s1 = "S22-24_2nd_phr1_phl1_Pneg"
s2 = "S28-30_2nd_pepr1_pepr2_phr1_phl1_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 7)
# 1st_phr1_phl1_Ppos vs 1st_phr1_phl1_lpr1_lpr2_Ppos
s1 = "S04-06_1st_phr1_phl1_Ppos"
s2 = "S07-09_1st_phr1_phl1_lpr1_lpr2_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_phr1_phl1_Pneg vs 2nd_phr1_phl1_lpr1_lpr2_Pneg
s1 = "S22-24_2nd_phr1_phl1_Pneg"
s2 = "S25-27_2nd_phr1_phl1_lpr1_lpr2_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 8)
# 1st_lpr1_lpr2_Ppos vs 1st_phr1_phl1_lpr1_lpr2_Ppos
s1 = "S16-18_1st_lpr1_lpr2_Ppos"
s2 = "S07-09_1st_phr1_phl1_lpr1_lpr2_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_lpr1_lpr2_Pneg vs 2nd_phr1_phl1_lpr1_lpr2_Pneg
s1 = "S34-36_2nd_lpr1_lpr2_Pneg"
s2 = "S25-27_2nd_phr1_phl1_lpr1_lpr2_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 9)
# 1st_pepr1_pepr2_Ppos vs 1st_pepr1_pepr2_phr1_phl1_Ppos
s1 = "S13-15_1st_pepr1_pepr2_Ppos"
s2 = "S10-12_1st_pepr1_pepr2_phr1_phl1_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_pepr1_pepr2_Pneg vs 2nd_pepr1_pepr2_phr1_phl1_Pneg
s1 = "S31-33_2nd_pepr1_pepr2_Pneg"
s2 = "S28-30_2nd_pepr1_pepr2_phr1_phl1_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)


# 10)
# 1st_Col-0_Ppos vs 1st_pepr1_pepr2_Ppos
s1 = "S01-03_1st_Col-0_Ppos"
s2 = "S13-15_1st_pepr1_pepr2_Ppos"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)

# 2nd_Col-0_Pneg vs 2nd_pepr1_pepr2_Pneg
s1 = "S19-21_2nd_Col-0_Pneg"
s2 = "S31-33_2nd_pepr1_pepr2_Pneg"
combOutPrefix = "$(s1)_vs_$(s2)"
combDict = OrderedDict(s1=>smplPathsDict[s1], s2=>smplPathsDict[s2])
generate_heatmap_for_multiple_samples(combDict, combinationDir, combOutPrefix)
#=
=#