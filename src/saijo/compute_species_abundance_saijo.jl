#=
Compute relative species abundance Using MetaPhlAn3
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging

# Command line interface
doc = """
Usage: compute_species_abundance_saijo.jl (-r <reads_dir>) (-s <sample_names>) (-o <out_dir>) (-f <output_list>) [-d -m -t <cpus>]

Options:
    -r <reads_dir)>, --reads-dir <reads_dir>    Directory with raw reads
    -s <samples>, --samples                     Table with sample names (in the first column)
    -o <out_dir>, --output-dir                  Output directory [default: .]
    -f <name>, --output-files                   Name of the output file with each profile suresults for each sample
    -t <cpus>, --threads <cpus>                 Number of threads [default: 4]
    -m, --merged                                Process tables with merged read-sets
    -d, --debug                                 Debug mode.
"""



# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--samples"])
outDir = abspath(args["--output-dir"])
readsDir = realpath(args["--reads-dir"])
outListFile = args["--output-files"]
merged = args["--merged"]
threads = parse(Int, args["--threads"])
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)

@info tblPath readsDir outDir outListFile merged threads

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# import the modules
@everywhere include(joinpath(dirname(@__FILE__), "modules/metagenomes.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
# @everywhere include(joinpath(dirname(@__FILE__), "modules/preproc_reads.jl"))

# create output directory
systools.makedir(outDir)


# Open log file
ofd = open(outListFile, "w")
# Load the file with the path and clean the reads
for ln in eachline(tblPath)

    # skip the header if required
    if merged
        if ln[1:3] == "set"
            continue
        end
    end

    # extract the info and perform the trimming
    @show ln

    smplName::String = ""
    # Extract different columns depending if the sets are merged or not
    if !merged
        # The sample name must be in the first column
        smplName = split(ln, "\t", limit=2)[1]
    else
        # Lines of merged tables have the following formatting:
        # set_name	experimental_condition	s1r1	s1r2	s2r1	s2r2	s3r1	s3r2
        # S01-03	1st_Col-0_Ppos	S01_1st_Col-0_Ppos_r1.clean.fastq.gz	S01_1st_Col-0_Ppos_r2.clean.fastq.gz	S02_1st_Col-0_Ppos_r1.clean.fastq.gz	S02_1st_Col-0_Ppos_r2.clean.fastq.gz
        setName, expCondition, dummy = split(ln, "\t", limit=3)
        smplName = "$(setName)_$(expCondition)"
    end

    @show smplName
    if !merged
        r1 = joinpath(readsDir, "$(smplName)_r1.clean.fastq.gz")
        r2 = joinpath(readsDir, "$(smplName)_r2.clean.fastq.gz")
    else
        r1 = joinpath(readsDir, "$(smplName)_1.fastq.gz")
        r2 = joinpath(readsDir, "$(smplName)_2.fastq.gz")
    end
    
    println("\nProcessing sample $(smplName)...")

    tmpResults = metagenomes.metaphlan3(r1, r2, outDir, smplName, threads, debug)
    write(ofd, @sprintf("%s\n", tmpResults))
    # break
end

close(ofd)


# merge the results
mergedSpAbundTbl = joinpath(outDir, "sp_abundances_saijo.concatenated_sets.hclust2.input.tsv")
metagenomes.merge_metaphlan_species(outListFile, mergedSpAbundTbl, true, debug)