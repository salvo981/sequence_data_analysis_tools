#=
This file simply adds a prefix to kofam_scan output (generated using --format mapper)
to make it usable with KEGGDecoder
=#

using Printf:@printf,@sprintf
using DocOpt
using Logging
# using Distributed
# using DataStructures

# Command line interface
doc = """
Usage: convert_kofam_mapperout2keggdecoder.jl (-i <in_tbl>) (-o <out_tbl>)  (--in-format <kofam_format>) [--prefix <prefix_id> -d]

Options:
    -i <in_tbl>, --raw-kofam         Kofam scan output generated using (--format mapper)
    -o <out_tbl>, --out-tbl      Output file where the KEGGDecoder input is sotred [default: .]
    --prefix <prefix-id>         String to be added to first id in first colum of KEGGDecoder input [default: ]
    --in-format <kofam_format>   KofamScan file format (e.g., mapper, detail-tsv) [default: mapper]
    -d, --debug                  Debug mode.
"""



"""Convert raw kofamscan output to valid KeggDecoder input."""
function generate_keggdecoder_input(ksraw::String, outpath::String, ksformat::String="mapper", prefix::String="")::Nothing
    @debug "generate_keggdecoder_input :: START" ksraw outpath ksformat
    
    validFmts::Array{String, 1} = String["mapper", "detail-tsv"]
    tmpFlds::Array{String, 1} = String[]
    tmpProtId::String = tmpKeggId = ""

    if ksformat ∉ validFmts
        @error "Not accepted kofam_scan output format" ksformat
        exit(-5)
    end

    ofd::IOStream = open(outpath, "w")
    ifd::IOStream = open(ksraw, "r")
    if ksformat == "mapper"
        for ln in eachline(ksraw)
            tmpFlds = split(ln, "\t", limit=2)
            if length(tmpFlds) == 2
                if length(prefix) > 0
                    write(ofd, "$(prefix)_$(tmpFlds[1])\t$(tmpFlds[2])\n")
                else
                    write(ofd, "$(tmpFlds[1])\t$(tmpFlds[2])\n")
                end
            end
        end
    elseif ksformat == "detail-tsv"
        # Lines in the kofam scan output would look as follows
        # #	gene name	KO	thrshld	score	E-value	"KO definition"
        # #	---------	------	-------	------	---------	-------------
        # bin.1.k141_197600_length_650081_cov_25.0002_1	K02358	358.47	87.8	2e-24	"elongation factor Tu"
        # *	bin.1.k141_197600_length_650081_cov_25.0002_2	K02946	64.17	146.0	4.4e-42	"small subunit ribosomal protein S10"

        # skip the header lines
        readline(ifd, keep=true)
        readline(ifd, keep=true)
        # process the hits
        for ln in eachline(ksraw)
            if ln[1] == '*'
                tmpProtId, tmpKeggId = split(ln, "\t", limit=4)[2:3]
                # write the prefix if required
                if length(prefix) > 0
                    write(ofd, "$(prefix)_$(tmpProtId)\t$(tmpKeggId)\n")
                else
                    write(ofd, "$(tmpProtId)\t$(tmpKeggId)\n")
                end
            end
        end
    end
    close(ifd)
    close(ofd)

    return nothing
end


##### MAIN #####

# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--raw-kofam"])
outTbl = abspath(args["--out-tbl"])
prefix = String(args["--prefix"])
ksformat = args["--in-format"]
debug = args["--debug"]

# Create and activate the logger

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@debug "convert_kofam_mapperout2keggdecoder.jl" tblPath outTbl prefix ksformat

generate_keggdecoder_input(tblPath, outTbl, ksformat, prefix)

#=
# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)
=#


