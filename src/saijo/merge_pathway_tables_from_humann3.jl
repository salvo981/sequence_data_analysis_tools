#=
Compute merge pathway (abundance ot coverage) tables for Saijo metagenomic sets.
The pathway tables must be created by Humann3 using the script src/saijo/compute_pathway_coverages_using_humann3.jl
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
# using DataFrames
# using CSV
using OrderedCollections: OrderedDict
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
srcPath = abspath(dirname(@__FILE__))
modulesPath = joinpath(dirname(srcPath), "modules")
# @show srcPath, modulesPath

# add the path to LOAD_PATH
push!(LOAD_PATH, modulesPath)
# @show LOAD_PATH

# Load the module
using saijoAnalysis



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
    "version info, default values, " *
    "options with types, variable " *
    "number of arguments.",
    version = "Version 1.0", # version info
    add_version = true)      # audo-add version option
    
    @add_arg_table! s begin
    "tables-dir"
    nargs = 1
    arg_type = String
    help = "Directory containing the tables with pathway abundance/coverage computed using Humann3 (at least 1 per sample)."
    required = true

    "smpls"
    nargs = 1
    arg_type = String
    help = "A TSV table with sample names, which are part of the file names of read sets."
    required = true

    "--output-dir"
    nargs = '?'
    arg_type = String
    default = "."
    help = "Directory that will contain the output files."
    required = true

    "--search-mode"
    nargs = '?'
    arg_type = String
    default = "uniref50"
    help = "Use uniref50 or uniref90 database."
    required = true

    "--out-prefix"
    nargs = '?'
    arg_type = String
    default = "merged_pathway_tables"
    help = "Prefix to use for the output merged tables."
    required = false

    "--debug", "-d"
    action = :store_true   # this makes it a flag
    help = "Show debug information."
end

parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
return parsed_args
end



"""Map sample names tables files with pathway abundance and coverages.
"""
function load_tbls_paths(tablesDir::String, smplsTblPath::String, searchMode::String):: OrderedDict{String, Tuple{String, String}}
    @debug "load_tbls_paths :: START" tablesDir smplsTblPath searchMode
    
    tblsDict = OrderedDict{String, Tuple{String, String}}()
    smplName::String = ""
    abundancePath::String = ""
    coveragePath::String = ""

    i = 0
    # The tables with pathway coverages have following naming pattern
    # <sample-name>_<search-mode>_no_strata_pathcoverage.tsv
    # for example: S19_2nd_Col-0_Pneg.uniref50.with_strata_pathcoverage.tsv
    # The tables with pathway abundances have following naming pattern
    # for example: S19_2nd_Col-0_Pneg.uniref50.with_strata_pathabundance.tsv
    for ln in eachline(open(smplsTblPath, "r"), keep=false)
        # skip hdr
        if ln[1] == 'o'
            continue
        end

        # Extrac sample name
        smplName = split(ln, "\t", limit=3)[2]
        # @show smplName
        # The tables with pathway abundances have following naming pattern
        # <sample-name>_<search-mode>_no_strata_pathabundance.tsv
        # for example: S19_2nd_Col-0_Pneg.uniref50.no_strata_pathabundance.tsv
        abundancePath = joinpath(tablesDir, "$(smplName).$(searchMode).no_strata_pathabundance.tsv")
        # Make sure the read file exists
        if !isfile(abundancePath)
            @error "The table with pathway abundances for sample $(smplName) was not found" smplName abundancePath
            exit(-2)
        end
        # Check if the table with pathway coverages exists
        # The tables with pathway coverages have following naming pattern
        # <sample-name>_<search-mode>_no_strata_pathcoverage.tsv
        # for example: S19_2nd_Col-0_Pneg.uniref50.no_strata_pathcoverage.tsv
        coveragePath = joinpath(tablesDir, "$(smplName).$(searchMode).no_strata_pathcoverage.tsv")
        # Make sure the read file exists
        if !isfile(coveragePath)
            @error "The table with pathway coverages for sample $(smplName) was not found" smplName coveragePath
            exit(-2)
        end
        # add the paths to the dicionary
        if !haskey(tblsDict, smplName)
            tblsDict[smplName] = (abundancePath, coveragePath)
        else
            @error "Sample $(smplName) is repeated in sample names table." smplsTblPath
            exit(-6)
        end
    end

    @debug "Extracted paths for $(length(tblsDict)) samples"

    return tblsDict
end



"""Read and merge pathway coverage/abundance tables.
"""
function merge_tables(tblsDict::OrderedDict{String, Tuple{String, String}}, dataType::String, outPath::String):: Nothing
    @debug "merge_tables :: START" length(tblsDict) dataType outPath

    # make sure the search mode is valid
    validDataTypes = String["pathabundance", "pathcoverage"]
    if dataType ∉ validDataTypes
        @error "Invalid type of information provided!" dataType validDataTypes
        exit(-7)
    end

    # Tmp variables
    pathway::String = ""
    value::String = "" # can be coverage or abundance
    tblPath::String = ""
    pathwaySet::Set{String} = Set{String}()
    smplNames::Array{String, 1} = String[]
    pathwayList::Array{String, 1} = String[]
    tmpPathwayDict::Dict{String, String} = Dict{String, String}()
    # Associate the pathways and values (coverage|abundance) to each sample
    smpl2pathwayDict::Dict{String, Dict{String, String}} = Dict{String, Dict{String, String}}()
    # smplCnt::Int = 0

    dataTypeIdx::Int = 1 # Set the idx based on the data type
    if dataType == "pathcoverage"
        dataTypeIdx = 2
    end

    # The tables with pathway coverages have following naming pattern
    for (smpl, pathsTpl) in tblsDict
        # @show smpl
        # smplCnt += 1
        tblPath = pathsTpl[dataTypeIdx]
        # Empty the temporary dictionary
        empty!(tmpPathwayDict)

        # Each table file with pathway information have the following format
        # Pathway S19_2nd_Col-0_Pneg.uniref90.no_strata_Coverage
        # UNMAPPED 1.0000000000
        # UNINTEGRATED 1.0000000000
        # ILEUSYN-PWY: L-isoleucine biosynthesis I (from threonine) 1.0000000000
        # PWY-7111: pyruvate fermentation to isobutanol (engineered) 1.0000000000
        ifd::IOStream = open(tblPath, "r")
        # Skip the first 3 lines
        readline(ifd, keep=true)
        readline(ifd, keep=true)
        readline(ifd, keep=true)
        for ln in eachline(ifd, keep=false)
            # println(ln)
            pathway, value = split(ln, "\t", limit=2)
            # @show pathway value
            if pathway ∉ pathwaySet
                push!(pathwaySet, pathway)
            end
            # Add the entry to the temporary dictionary
            tmpPathwayDict[pathway] = value
        end
        close(ifd)

        # Add the pathway dictionary to main dictionary
        smpl2pathwayDict[smpl] = copy(tmpPathwayDict)
        # if smplCnt == 2
        #     break
        # end
    end

    # Create the output file and write the header
    smplNames = sort(collect(keys(smpl2pathwayDict)))
    pathwayList = sort(collect(pathwaySet))
    ofd::IOStream = open(outPath, "w")
    hdr::String = join(smplNames, "\t")
    hdr = "Pathway\t$(hdr)\n"
    write(ofd, hdr)

    @debug "Processing summary for pathway analysis tables" dataType length(smpl2pathwayDict) length(pathwaySet)
    empty!(tmpPathwayDict)

    # For each smpl add the missing pathways in the dictionary
    for smpl in smplNames
        # @show smpl
        # @show typeof(smpl2pathwayDict[smpl])
        # @show length(smpl2pathwayDict[smpl])
        tmpPathwayDict = smpl2pathwayDict[smpl]
        # @show length(tmpPathwayDict)
        # Add the pathway if missing from the dictionary
        for p in pathwayList
            if !haskey(tmpPathwayDict, p)
                tmpPathwayDict[p] = "0.0"
            end
        end
        # @show length(tmpPathwayDict)
        # break
    end

    # Iterate through pathway and smpl names and write lines to the output files
    for p in pathwayList
        # Write the pathway to output
        write(ofd, p)
        # Iterate though the sample names
        # and write the value for the pathway
        for smpl in smplNames
            write(ofd, "\t$(smpl2pathwayDict[smpl][p])")
        end
        write(ofd, "\n")
    end

    close(ofd)

    return nothing
end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

tblRoot = realpath(args["tables-dir"][1])
smplsTblPath = realpath(args["smpls"][1])
outDir = abspath(args["output-dir"])
outPrefix = args["out-prefix"]
searchMode = args["search-mode"]
tmpOutPath = ""
makedir(outDir)
# outTbl = abspath(args["output-tbl"])
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

# make sure the search mode is valid
validSearchModes = String["uniref50", "uniref90"]
if searchMode ∉ validSearchModes
    @error "Invalid search mode!" searchMode validSearchModes
    exit(-7)
end

@info "Merging of pathway abundances/coverage tables will be preformed with the following parameters:" tblRoot smplsTblPath outDir outPrefix searchMode debug

# Load read paths
tblsDict = load_tbls_paths(tblRoot, smplsTblPath, searchMode)
# @debug "Sample with table files" length(tblsDict)

# Merge tables with path coverages
dataType = "pathcoverage" # or "pathabundance"
tmpOutPath = joinpath(outDir, "$(outPrefix).$(searchMode).$(dataType).tsv")
merge_tables(tblsDict, dataType, tmpOutPath)

# Merge tables with path abundances
dataType = "pathabundance" # or "pathabundance"
tmpOutPath = joinpath(outDir, "$(outPrefix).$(searchMode).$(dataType).tsv")
merge_tables(tblsDict, dataType, tmpOutPath)