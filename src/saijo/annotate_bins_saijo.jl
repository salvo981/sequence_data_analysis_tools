#=
Annotate bins generate from downstream analysis
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging
using DataStructures

# Command line interface
doc = """
Usage: annotate_bins_saijo.jl (-i <in_tbl>) (-o <out_dir>) (--profiles <profiles>) (--ko-list <ko_file>) [-d -t <cpus> -m <mode> --out-format <fmt>]

Options:
    -i <metadata>, --tbl                        Table with metadata regarding the samples and bins
    -m <prodigal_mode>, --mode <prodigal_mode>  Set prodigal mode (single or meta) [default: single]
    -o <out_dir>, --output-root                 Output directory in which the runs are stored [default: .]
    --out-format <fmt>                          Valid output format for KofamScan [default: detail-tsv]
    --profiles <profiles>                       Directory with kofamscan HMM profiles [default: .]
    --ko-list <ko_file>                         Path to file with list of KO IDs [default: .]
    -t <cpus>, --threads <cpus>                 Number of threads [default: 4]
    -d, --debug                                 Debug mode.
"""



"""
    Concatenate bins into a single FASTA file
"""
function concatenate_bins(binsDir::String, outPath::String; threads::Int=4)::Nothing
    @debug "concatenate_bins :: START" binsDir outPath threads

    # define temp vars
    tmpPath::String = tmpWorkPath = ""
    outdir = dirname(outPath)
    systools.makedir(outdir)
    # Create output file
    ofd::IOStream = open(outPath, "w")
    # copy the bin files into the output directory
    for f in readdir(binsDir)
        tmpPath = joinpath(binsDir, f)
        if isfile(tmpPath)
            # check if it an archive
            if systools.is_archive(tmpPath)[1]
                # decompress into the output directory
                # set the output name removing the extensions
                tmpWorkPath = joinpath(outdir, rsplit(f, ".", limit=2)[1])
                systools.pigz_decompress(tmpPath, tmpWorkPath, keep=true, force=true, errLog=true, threads=threads)
            else
                # flush it into the output file
                tmpWorkPath = joinpath(outdir, f)
            end
            # copy the content of bin into the main output file
            write(ofd, read(tmpWorkPath))
            # @warn "only one BIN is being processed for testing" tmpWorkPath
            # break
        end
    end
    # close output
    close(ofd)
end



"""Read table with sample names.
    Prepare dictionary with directories used in the annotation.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    experiment::String = ""
    tmpdir::String = ""
    # process the metatable
    # Each line contain lines as follow
    # set_name	experimental_condition	s1r1	s1r2	s2r1	s2r2	s3r1	s3r2
    # S01-03	1st_Col-0_Ppos	S01_1st_Col-0_Ppos_r1.fastq.gz	S01_1st_Col-0_Ppos_r2.fastq.gz	S02_1st_Col-0_Ppos_r1.fastq.gz	S02_1st_Col-0_Ppos_r2.fastq.gz	S03_1st_Col-0_Ppos_r1.fastq.gz	S03_1st_Col-0_Ppos_r2.fastq.gz
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1] == 's'
            continue
        end
        
        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=3)
        experiment = "$(flds[1])_$(flds[2])"
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, experiment)
        if !isdir(tmpDir)
            @warn "The run $(experiment) is missing" tmpDir
            continue
        else
            outDict[experiment] = Dict{String, String}("binsdir"=>"bin_refinement", "annotdir"=>joinpath(outRoot, "$(experiment)/kofamscan_annotation"), "preddir"=>joinpath(outRoot, "$(experiment)/protein_prediction"))
        end

        tmpDir = joinpath(outRoot, "$(experiment)/bin_refinement")
        # detect the directory with refined bins
        if !isdir(tmpDir)
            @error "Bins refinement missing for $(experiment)" tmpDir
            exit(-2)
        else
            for el in readdir(tmpDir)
                if isdir(joinpath(tmpDir, el))
                    if startswith(el, r"metawrap_") && endswith(el, r"_bins")
                        # found the directory
                        outDict[experiment]["binsdir"] = joinpath(outRoot, "$(experiment)/bin_refinement/$(el)")
                        break
                    end
                end
            end
        end

        # break
    end

    return outDict
end


""" Store files generated from annotation pipeline into the appropriate directories."""
function store_annotation_files(workDir::String, protPredDir::String, annotDir::String, smplName::String; compress::Bool=true, threads::Int=4)::Nothing
    @debug "store_annotation_files :: START" workDir protPredDir annotDir smplName compress threads

    tmpOutPath::String = tmpInPath = ""
    # For each file in the work directory, if it starts
    # with the sample name than it should be kept
    for f in readdir(workDir)
        if startswith(f, "$(smplName).bins")
            tmpInPath = joinpath(workDir, f)
            # kofamscan file
            if occursin(r".kofamscan.", f)
                tmpOutPath = joinpath(annotDir, f)
            # predicted proteins
            elseif endswith(f, r".faa")
                tmpOutPath = joinpath(protPredDir, f)
            # concatenated bins
            elseif endswith(f, r".fna") && !startswith(f, r"tmp.concat.")  
                tmpOutPath = joinpath(protPredDir, f)
            else
                continue
            end

            # Move or compress the file
            if compress
                systools.pigz_compress(tmpInPath, tmpOutPath, level=9, keep=true, force=true, useGzip=true, errLog=false, threads=threads)
            else
                mv(tmpInPath, tmpOutPath, force=true)
            end
        end
    end

end


##### MAIN #####

# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--tbl"])
outRoot = abspath(args["--output-root"])
threads = parse(Int, args["--threads"])
pmode = args["--mode"]
outFmt = string(args["--out-format"])
profPath = abspath(args["--profiles"])
koList = abspath(args["--ko-list"])
debug = args["--debug"]

# Create and activate the logger

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@debug "annotate_bins_saijo" tblPath outRoot threads pmode

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# import the modules
# @everywhere include(joinpath(dirname(@__FILE__), "modules/mmseqs_tools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/seqAnnotation.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/seq_tools.jl"))

# create output directory
smplPathsDict = extract_run_names(tblPath, outRoot)
binsDir = annotDir = workDir = tmpDir = ""
masterBinsDna = masterBinProt = hdr2binMappingFile = tmpConcatBins = ""

for smpl in keys(smplPathsDict)
    println("Performing annotation for sample $(smpl)")
    annotDir = smplPathsDict[smpl]["annotdir"]
    protPredDir = smplPathsDict[smpl]["preddir"]
    # Create directories for protein prediction and annotation
    systools.makedir(annotDir)
    systools.makedir(protPredDir)
    binsDir = smplPathsDict[smpl]["binsdir"]
    hdr2binMappingFile = "$(binsDir).contigs"
    # Create temporary directory
    workDir = mktempdir(annotDir, prefix="kofam_work_", cleanup=false)
    # Concatenate the bins
    tmpConcatBins = joinpath(workDir, "tmp.concat.bins.fna")
    concatenate_bins(binsDir, tmpConcatBins, threads=threads)
    # concatenate_bins(binsDir, masterBinsDna, threads=threads)
    masterBinsDna = joinpath(workDir, "$(smpl).bins.fna")
    seq_tools.modify_fasta_hdr(tmpConcatBins, masterBinsDna, hdr2binMappingFile, extendLeft=true)
    # predict proteins using prodigal
    masterBinProt = seqAnnotation.prodigal(masterBinsDna, workDir, prefix="$(smpl).bins", mode=pmode, outLog=true, quiet=true)
    # perform kofamscan annotation
    seqAnnotation.kofamscan_annotate(masterBinProt, workDir, profiles=profPath, koList=koList, prefix="$(smpl).bins.kofamscan", fmt=outFmt, outLog=true, threads=threads)
    # store the generated anotation files
    store_annotation_files(workDir, protPredDir, annotDir, smpl, compress=true, threads=threads)
    # remove the work dir
    rm(workDir, force=true, recursive=true)
end