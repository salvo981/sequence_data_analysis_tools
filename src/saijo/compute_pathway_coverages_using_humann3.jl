# using Base: iscontiguous, Float64
#=
Compute pathway coverages for each sample in the Saijo metagenomic sets using Humann3
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
# using DataFrames
# using CSV
using OrderedCollections: OrderedDict
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
srcPath = abspath(dirname(@__FILE__))
modulesPath = joinpath(dirname(srcPath), "modules")
# @show srcPath, modulesPath

# add the path to LOAD_PATH
push!(LOAD_PATH, modulesPath)
# @show LOAD_PATH

# Load the module
using saijoAnalysis



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
    "version info, default values, " *
    "options with types, variable " *
    "number of arguments.",
    version = "Version 1.0", # version info
    add_version = true)      # audo-add version option
    
    @add_arg_table! s begin
    "reads-dir"
    nargs = 1
    arg_type = String
    help = "Directory containing the clean read-sets (2 for each sample)."
    required = true

    "smpls"
    nargs = 1
    arg_type = String
    help = "A TSV table with sample names, which are part of the file names of read sets."
    required = true

    "--output-dir"
    nargs = '?'
    arg_type = String
    default = "."
    help = "Directory that will contain the output files."
    required = true

    "--diamond-dir"
    nargs = '?'
    arg_type = String
    default = ""
    help = "Directory containing the binaries for DIAMOND."
    required = false

    "--bowtie-dir"
    nargs = '?'
    arg_type = String
    default = ""
    help = "Directory containing the binaries for Bowtie2."
    required = false

    "--uniref-dir"
    nargs = '?'
    arg_type = String
    default = ""
    help = "Directory containing uniref databases for DIAMOND."
    required = false

    "--search-mode"
    nargs = '?'
    arg_type = String
    default = "uniref50"
    help = "Use uniref50 or uniref90 database."
    required = false

    "--max-reads"
    nargs = '?'
    arg_type = Int
    default = 0
    help = "Number of reads to use for each sample (e.g., 1000 means 500 reads for each paired-end file)."
    required = false

    "--threads"
    nargs = '?'
    arg_type = Int
    default = 4
    help = "Number of threads."
    required = false

    "--debug", "-d"
    action = :store_true   # this makes it a flag
    help = "Show debug information."
end

parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
return parsed_args
end



"""Map sample name to read files.
"""
function load_read_paths(rawReadsRoot::String, smplsTblPath::String):: OrderedDict{String, Tuple{String, String}}
    @debug "load_read_paths :: START" rawReadsRoot smplsTblPath
    
    readsDict = OrderedDict{String, Tuple{String, String}}()
    smplName::String = ""
    tmpR1Path::String = ""
    tmpR2Path::String = ""

    i = 0
    # The table with sample names have the format
    # original_id sample r1 r2
    # 19 S19_2nd_Col-0_Pneg /nfs_share/salvocos/projects/pags/naist_saijo_shotgun_sequencing/original_data_from_usb_key/Sequence/NAIST_S19_2nd_13_Pneg_S19_R1_001.fastq.gz /nfs_share/salvocos/projects/pags/naist_saijo_shotgun_sequencing/original_data_from_usb_key/Sequence/NAIST_S19_2nd_13_Pneg_S19_R2_001.fastq.gz
    for ln in eachline(open(smplsTblPath, "r"), keep=false)
        # skip hdr
        if ln[1] == 'o'
            continue
        end

        # Extrac sample name
        smplName = split(ln, "\t", limit=3)[2]
        # @show smplName
        # Read set names have the following patter
        # smplName_r1.clean.fastq.gz
        tmpR1Path = joinpath(rawReadsRoot, "$(smplName)_r1.clean.fastq.gz")
        # Make sure the read file exists
        if !isfile(tmpR1Path)
            @error "The first read set for sample $(smplName) was not found" smplName tmpR1Path
            exit(-2)
        end
        # Check if the file for r2 exists
        tmpR2Path = joinpath(rawReadsRoot, "$(smplName)_r2.clean.fastq.gz")
        # Make sure the read file exists
        if !isfile(tmpR2Path)
            @error "The second read set for sample $(smplName) was not found" smplName tmpR2Path
            exit(-2)
        end
        # add the paths to the dicionary
        readsDict[smplName] = (tmpR1Path, tmpR2Path)
    end

    @debug "Extracted paths for $(length(readsDict)) samples"

    return readsDict
end



"""Process input reads and generate input for Humann3.
"""
function prepare_humann3_input(smplName::String, r1::String, r2::String, outDir::String; maxReads::Int):: String
    @debug "prepare_humann3_input :: START" smplName r1 r2 outDir maxReads

    # Tmp variables
    humannInput::String = joinpath(outDir, "$(smplName).fastq")
    tmpReadArchive::String = ""
    tmpReadSet::String = ""
    readsPerSet::Int = 0
    lnCntR1::Int = 0
    lnCntR2::Int = 0
    seqCntR1::Int = 0
    seqCntR2::Int = 0
    if maxReads > 0
        readsPerSet = Int(maxReads / 2)
    end

    # Open main output file
    ofd::IOStream = open(humannInput, "w")

    # Extract reads from first set
    tmpReadArchive = joinpath(outDir, basename(r1))
    cp(r1, tmpReadArchive, force=true)
    tmpReadSet = joinpath(outDir, replace(basename(r1), ".gz" => ""))
    pigz_decompress(tmpReadArchive, tmpReadSet, keep=true, force=true, errLog=false, threads=16)
    # Extract the reads
    ifd::IOStream = open(tmpReadSet, "r")
    # Write a limited number of reads
    if readsPerSet > 0
        # Write the read into the final input FASTQ file
        for ln in eachline(ifd, keep=true)
            lnCntR1 += 1
            write(ofd, ln)
            # increament the sequence count every 4 writes
            # and exit if the limit has been reached
            if (lnCntR1 % 4 == 0)
                if (seqCntR1 += 1) == readsPerSet break end
            end
        end
    else
        # Write the read into the final input FASTQ file
        for ln in eachline(ifd, keep=true)
            lnCntR1 += 1
            write(ofd, ln)
            # increament the sequence count every 4 writes
            if (lnCntR1 % 4 == 0) seqCntR1 += 1 end
        end
    end
    close(ifd)
    # Remove tempporary files
    rm(tmpReadArchive)
    rm(tmpReadSet)

    # Extract reads from second set
    tmpReadArchive = joinpath(outDir, basename(r2))
    cp(r2, tmpReadArchive, force=true)
    tmpReadSet = joinpath(outDir, replace(basename(r2), ".gz" => ""))
    pigz_decompress(tmpReadArchive, tmpReadSet, keep=true, force=true, errLog=false, threads=16)
    # Extract the reads
    ifd = open(tmpReadSet, "r")
    # Write a limited number of reads
    if readsPerSet > 0
        # Write the read into the final input FASTQ file
        for ln in eachline(ifd, keep=true)
            lnCntR2 += 1
            write(ofd, ln)
            # increament the sequence count every 4 writes
            # and exit if the limit has been reached
            if (lnCntR2 % 4 == 0)
                if (seqCntR2 += 1) == readsPerSet break end
            end
        end
    else
        # Write the read into the final input FASTQ file
        for ln in eachline(ifd, keep=true)
            lnCntR2 += 1
            write(ofd, ln)
            # increament the sequence count every 4 writes
            if (lnCntR2 % 4 == 0) seqCntR2 += 1 end
        end
    end
    close(ifd)
    # Remove tempporary files
    rm(tmpReadArchive)
    rm(tmpReadSet)

    close(ofd)

    @debug "Reads concatenation summary" lnCntR1 (lnCntR1/4) lnCntR2 (lnCntR2/4) seqCntR1 seqCntR2
    return humannInput

end



"""Run on the input Humann3.
"""
function run_humann3(humannInput::String, smplName::String, outDir::String; searchMode::String = "uniref50", dbPath::String = "", bowtieBinDir::String = "", diamondBinDir::String = "", noStratification::Bool = true, removeTempFiles::Bool = false, threads::Int = 4, tmpDir::String = ""):: String
    @debug "run_humann3 :: START" humannInput smplName outDir searchMode dbPath bowtieBinDir diamondBinDir noStratification removeTempFiles threads tmpDir

    # Tmp variables
    bname::String = basename(humannInput)
    fmt::String = ""
    if endswith(bname, ".fastq")
        fmt = "fastq"
    elseif endswith(bname, ".gz")
        fmt = "fastq.gz"
    elseif endswith(bname, ".bam")
        fmt = "bam"
    elseif endswith(bname, ".sam")
        fmt = "sam"
    end
    # bname = rsplit(bname, ".$(fmt)", limit=2)[1]
    outBname::String = "$(smplName).$(searchMode)"
    if noStratification
        outBname = "$(outBname).no_strata"
    else
        outBname = "$(outBname).with_strata"
    end
    # Example of humann3 execution
    # humann  --verbose --remove-temp-output --bypass-translated-search --remove-stratified-output --threads 32 --diamond /home/linuxbrew/.linuxbrew/bin/ --bowtie2 /home/linuxbrew/.linuxbrew/bin/ --protein-database  /home/salvocos/tmp/humann3_dbs/uniref_ec_filtered/ --search-mode uniref50 --input S19.6M.fastq --output test_6M_filtered_bypass_translated_no_strata &> log.test_6M_ec_uniref50_filtered_bypass_no_strata.txt

    humannParams::Array{String, 1} = String["--output-basename", outBname, "--input-format", fmt, "--threads", string(threads), "--bypass-translated-search", "--search-mode", searchMode]
    if removeTempFiles
        push!(humannParams, "--remove-temp-output")
    end
    if noStratification
        push!(humannParams, "--remove-stratified-output")
    end
    # Add paths to DB
    if length(dbPath) > 0
        push!(humannParams, "--protein-database")
        push!(humannParams, dbPath)
    end
    # Add paths to DIAMOND
    if length(diamondBinDir) > 0
        push!(humannParams, "--diamond")
        push!(humannParams, diamondBinDir)
    end
    # Add paths to Bowtie2
    if length(bowtieBinDir) > 0
        push!(humannParams, "--bowtie2")
        push!(humannParams, bowtieBinDir)
    end
        
    humannCmd::Cmd = `humann $(humannParams) --input $(humannInput) --output $(outDir)`
    # @show humannCmd
    print(humannCmd)
    
    # Create the buffers to store stderr and stdout
    # outBuffer::IOBuffer = IOBuffer()
    errBuffer::IOBuffer = IOBuffer()
    # procOut::Base.Process = run(pipeline(humannCmd, stdout=outBuffer, stderr=errBuffer), wait=true)
    procOut::Base.Process = run(pipeline(humannCmd, stderr=errBuffer), wait=true)
    # write the error if needed
    if procOut.exitcode != 0
        errContent::String = String(take!(errBuffer))
        @error "Something went wrong in the execution of humann3:" humannCmd
        print(errContent)
        exit(-5)
    end

    # If the a tmp directory was provided then it should be deleted
    @show tmpDir
    if length(tmpDir) > 0
        rm(tmpDir, force=true, recursive=true)
    end

    return outBname

end

#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

rawReadsRoot = realpath(args["reads-dir"][1])
smplsTblPath = realpath(args["smpls"][1])
outDir = abspath(args["output-dir"])
maxReads = args["max-reads"]
threads = args["threads"]
diamondBinDir = args["diamond-dir"]
searchMode = args["search-mode"]

# Check that the directory exists
if length(diamondBinDir) > 0
    diamondBinDir = abspath(diamondBinDir)
    if !isdir(diamondBinDir)
        @error "The directory with DIAMOND binaries is not valid." diamondBinDir
        exit(-2)
    end
end
bowtieBinDir = args["bowtie-dir"]
# Check that the directory exists
if length(bowtieBinDir) > 0
    bowtieBinDir = abspath(bowtieBinDir)
    if !isdir(bowtieBinDir)
        @error "The directory with Bowtie2 binaries is not valid." bowtieBinDir
        exit(-2)
    end
end
unirefDbDir = args["uniref-dir"]
# Check that the directory exists
if length(unirefDbDir) > 0
    unirefDbDir = abspath(unirefDbDir)
    if !isdir(unirefDbDir)
        @error "The directory containing uniref DBs is not valid." unirefDbDir
        exit(-2)
    end
end

# tmp variables
humannInFastq = ""

# make sure maxReads is an even number and is higer than 2
if maxReads > 0
    if maxReads % 2 != 0
        @error "The value for --max-reads ($(maxReads)) must be an even number." maxReads
        exit(-7)
    end
end
makedir(outDir)
# outTbl = abspath(args["output-tbl"])
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Computation of pathway abundances will be preformed with the following parameters:" rawReadsRoot smplsTblPath outDir maxReads diamondBinDir bowtieBinDir unirefDbDir  threads debug

# Load read paths
readsDict = load_read_paths(rawReadsRoot, smplsTblPath)

for (smpl, tpl) in readsDict
    # @show smpl tpl

    #######
    # Compute pathcoverages using uniref90
    global humannInFastq = prepare_humann3_input(smpl, tpl[1], tpl[2], outDir, maxReads=maxReads)    
    # Execute humann on the input (no stratification)
    outBname = run_humann3(humannInFastq, smpl, outDir, searchMode="uniref90", dbPath=unirefDbDir, bowtieBinDir=bowtieBinDir, diamondBinDir=diamondBinDir, noStratification=true, removeTempFiles=false, threads=threads, tmpDir="")
    # The tmp directory have the following pattern
    # outBname_humann_temp
    # The SAM alignment file path is
    # outBname_humann_temp/outBname_bowtie2_aligned.sam
    tmpDir = joinpath(outDir, "$(outBname)_humann_temp")
    if !isdir(tmpDir)
        @error "The tmp directory from the Humann3 run was not found" tmpReadArchive
        exit(-5)
    end
    samPath = joinpath(tmpDir, "$(outBname)_bowtie2_aligned.sam")
    if !isfile(samPath)
        @error "The SAM alignment file from Bowtie2 was not found" samPath
        exit(-5)
    end
    # Execute using the SAM file as input
    outBname = run_humann3(samPath, smpl, outDir, searchMode="uniref90", dbPath=unirefDbDir, bowtieBinDir=bowtieBinDir, diamondBinDir=diamondBinDir, noStratification=false, removeTempFiles=true, threads=threads, tmpDir=tmpDir)
    #######

    #######
    # Compute pathcoverages using uniref50
    # Execute humann on the input (no stratification)
    outBname = run_humann3(humannInFastq, smpl, outDir, searchMode="uniref50", dbPath=unirefDbDir, bowtieBinDir=bowtieBinDir, diamondBinDir=diamondBinDir, noStratification=true, removeTempFiles=false, threads=threads, tmpDir="")
    # The tmp directory have the following pattern
    # outBname_humann_temp
    # The SAM alignment file path is
    # outBname_humann_temp/outBname_bowtie2_aligned.sam
    tmpDir = joinpath(outDir, "$(outBname)_humann_temp")
    if !isdir(tmpDir)
        @error "The tmp directory from the Humann3 run was not found" tmpReadArchive
        exit(-5)
    end
    samPath = joinpath(tmpDir, "$(outBname)_bowtie2_aligned.sam")
    if !isfile(samPath)
        @error "The SAM alignment file from Bowtie2 was not found" samPath
        exit(-5)
    end
    # Execute using the SAM file as input
    outBname = run_humann3(samPath, smpl, outDir, searchMode="uniref50", dbPath=unirefDbDir, bowtieBinDir=bowtieBinDir, diamondBinDir=diamondBinDir, noStratification=false, removeTempFiles=true, threads=threads, tmpDir=tmpDir)
    #######

    # Remove the concatenated FASTQ file
    rm(humannInFastq, force=true)
    # break
end

