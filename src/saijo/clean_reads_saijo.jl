#=
Pre-process RAW reads from Saijo project
=#

using Printf:@printf,@sprintf
using DocOpt
using Distributed
using Logging

# Command line interface
doc = """
Usage: preprocess_saijo.jl (-i <in_tbl>) (-r <reads_dir>) (-o <out_dir>) [-d -t <cpus>]

Options:
    -r <reads_dir)>, --reads-dir <reads_dir>    Directory with raw reads
    -i <metadata>, --tbl                        Table with metadata regarding the raw reads
    -o <out_dir>, --output-dir                  Output directory [default: .]
    -t <cpus>, --threads <cpus>                 Number of threads [default: 4]
    -d, --debug                                 Debug mode.
"""

"""Maps original raw reads names to sample names containing the meta-data
   Outputs a table with the sample name mapping (only for the samples with existing raw reads)
"""
function map_raw_reads_names(metaTbl::String, readsdir::String, outdir::String, compressed::Bool=true, debug::Bool=false)::String
    if debug
        println("\nmap_raw_reads_names@$(@__MODULE__) :: START")
        @show metaTbl
        @show readsdir
        @show outdir
    end

    # open the outpuyt file
    outTbl::String = joinpath(outdir, "saijo_mapped_smpl_names.tsv")
    ofd::IOStream = open(outTbl, "w")
    write(ofd, "original_id\tsample\tr1\tr2\n")

    # tmp variables
    id::String = smplName = expCond = date1 = readPrefix = expectedPathR1 = expectedPathR2 = ""
    completeSmplName::String = ""
    flds::Array{String, 1} = []

    # read the metadata table and match the raw reads.
    # each line contains the following information
    # id: sample id
    # date: sampling date
    # experimental conditions: Phosphate addition/depletion +/-
    # sample_name: for example, Col-0, lpr1, etc.
    # the remaining information regard web-lab information (e.g., reagent concentrations)
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1] == 'i'
            continue
        end

        # The raw reads are expected to start with 'NAIST_SXX_*'
        # Where SXX is the sample ID that should match with the ID field in the metadata table
        # extract the required information for the each sample
        flds = split(ln, "\t", limit=6)
        # match the pair of read files for each sample
        id = flds[1]
        samplingDate = flds[2]
        # add a 0 in front of the ID if it is <10
        if length(id) == 1
            id = "0$(id)"
        end
        # Set the experiment to Ppos or Pneg if positive or negative
        expCond = flds[4]
        if expCond == "-"
            expCond = "Pneg"
        elseif expCond == "+"
            expCond = "Ppos"
        end
    
        # extract data info [1st or 2nd]
        date1 = split(flds[2], " (", limit=2)[2][1:3]
        # @show date1
        
        # set the expected read names
        # Raw reads Paths have the following patter
        # NAIST_S01_1st_1_Ppos_S1_R1_001.fastq.gz
        # NAIST_S01_1st_1_Ppos_S1_R2_001.fastq.gz
        # Where S01 corresponds to the ID 1 in the metadata table
        readPrefix = joinpath(readsdir, "NAIST_S$(id)_$(date1)_$(flds[3])_$(expCond)_S$(flds[1])_")
        # @show readPrefix
        # set the path names as they would be in the original data files    
        expectedPathR1 = "$(readPrefix)R1_001.fastq"
        expectedPathR2 = "$(readPrefix)R2_001.fastq"
        if compressed
            expectedPathR1 = "$(expectedPathR1).gz"
            expectedPathR2 = "$(expectedPathR2).gz"
        end

        # format the sample name
        smplName = replace(flds[5], " " => "_")
        completeSmplName = @sprintf("S%s_%s_%s_%s", id, date1, smplName, expCond)
        
        # Write the output line only if both
        # Read files exist
        if (isfile(expectedPathR1) && isfile(expectedPathR2))
            write(ofd, @sprintf("%s\t%s\t%s\t%s\n", flds[1], completeSmplName, expectedPathR1, expectedPathR2))
        else
            if !isfile(expectedPathR1)
                println("WARNING: read set R1 is missing\n$expectedPathR1\n")
            end
            if !isfile(expectedPathR2)
                println("WARNING: read set R2 is missing\n$expectedPathR2\n")
            end
        end
    end

    # close output and return the path to the table
    close(ofd)
    return outTbl
end



# Start the main

# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--tbl"])
outDir = abspath(args["--output-dir"])
readsDir = realpath(args["--reads-dir"])
threads = parse(Int, args["--threads"])
debug = args["--debug"]

# Create and activate the logger

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@debug """my msg
    tblPath: $tblPath
    readsDir: $readsDir
    outDir: $outDir
    threads:\t$threads
    """

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
addprocs(threads)

# Get the directory with julia scripts
# srcDir = dirname(realpath(PROGRAM_FILE))
testDataDir = joinpath(dirname(@__DIR__), "test_data")
# modulePath = joinpath(srcDir, "modules/seq_tools.jl")

# @everywhere using systools
# @everywhere include("systools.jl")


# import the modules
# @everywhere include(joinpath(dirname(@__FILE__), "modules/mmseqs_tools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
@everywhere include(joinpath(dirname(@__FILE__), "modules/preproc_reads.jl"))

# test reads trimming
# preproc_reads.clean_fastp(r1, r2, outDir, "prefix_banana", 28, threads, "test_trimming", true, debug)
##### END TESTING #####

# map metadata
#=
raw2smplTbl = map_raw_reads_names(tblPath, readsDir, outDir, debug)
# Load the file with the path and clean the reads
for ln in eachline(raw2smplTbl)
    # skip hdr
    if ln[1:3] == "ori"
        continue
    end
    # extract the info and perform the trimming
    tmpFlds = split(ln, "\t", limit=4)
    r1 = string(tmpFlds[3])
    r2 = string(tmpFlds[4])
    smplName = string(tmpFlds[2])
    println("\nProcessing sample $(smplName)...")
    preproc_reads.clean_fastp(r1, r2, outDir, smplName, 28, threads, smplName, true, debug)
    # break
end
=#

# count the reads in parallel
# extime = @elapsed seq_tools.parallel_count_reads(rPaths, threads, debug)
# @show extime
