#=
Filter a KEGGDecoder output table of unwanted pathways and generate a PCA
=#

using Printf:@printf,@sprintf
using DocOpt
using Logging
using DataStructures
# using Distributed
using DataFrames
using Statistics
using StatsBase
using Plots
using MultivariateStats
using MLBase
using CSV



doc = """
Usage: compute_pca_from_keggdecoder_table.jl.jl (-t <in_tbl>) (-r <main_output_dir>) (-o <output_dir>) [--output-prefix <op> -d]

Options:
    -t <metadata>, --tbl                   Table with metadata regarding the samples and bins
    -r <main_output_dir>, --output-root    Output directory in which the runs are stored [default: .]
    -o <output_dir>, --output-dir          Output directory in which the generated output will be stored [default: .]
    --output-prefix <op>                   Prefix for output files [default: compute_pca_from_keggdecoder_table]
    -d, --debug                            Debug mode.
"""



"""Read table with sample names.
    Prepare dictionary with directories used in the annotation.
"""
function extract_run_names(metaTbl::String, outRoot::String)::OrderedDict{String, Dict{String, String}}
    @debug "extract_run_names :: START" metaTbl outRoot

    # Dictionary reads info
    outDict::OrderedDict{String, Dict{String, String}} = OrderedDict{String, Dict{String, String}}()
    flds::Array{String, 1} = []
    experiment::String = ""
    tmpdir::String = ""
    # process the metatable
    # Each line contain lines as follow
    # set_name	experimental_condition	s1r1	s1r2	s2r1	s2r2	s3r1	s3r2
    # S01-03	1st_Col-0_Ppos	S01_1st_Col-0_Ppos_r1.fastq.gz	S01_1st_Col-0_Ppos_r2.fastq.gz	S02_1st_Col-0_Ppos_r1.fastq.gz	S02_1st_Col-0_Ppos_r2.fastq.gz	S03_1st_Col-0_Ppos_r1.fastq.gz	S03_1st_Col-0_Ppos_r2.fastq.gz
    for ln in eachline(open(metaTbl, "r"), keep=true)
        # skip hdr
        if ln[1] == 's'
            continue
        end
        
        # The run directory name is composed of the first 2 fields
        flds = split(ln, "\t", limit=3)
        experiment = "$(flds[1])_$(flds[2])"
        # make sure the run directory exists
        tmpDir = joinpath(outRoot, experiment)
        if !isdir(tmpDir)
            @warn "The run $(experiment) is missing" tmpDir
            continue
        else
            outDict[experiment] = Dict{String, String}("binsdir"=>"bin_refinement", "annotdir"=>joinpath(outRoot, "$(experiment)/kofamscan_annotation"), "preddir"=>joinpath(outRoot, "$(experiment)/protein_prediction"), "binstaxa"=>joinpath(outRoot, "$(experiment)/final_bins_taxonomy"))
        end

        tmpDir = joinpath(outRoot, "$(experiment)/bin_refinement")
        # detect the directory with refined bins
        if !isdir(tmpDir)
            @error "Bins refinement missing for $(experiment)" tmpDir
            exit(-2)
        else
            for el in readdir(tmpDir)
                if isdir(joinpath(tmpDir, el))
                    if startswith(el, r"metawrap_") && endswith(el, r"_bins")
                        # found the directory
                        outDict[experiment]["binsdir"] = joinpath(outRoot, "$(experiment)/bin_refinement/$(el)")
                        break
                    end
                end
            end
        end

        # break
    end

    return outDict
end



"""Generate a PCoA plot stating from the KEGGDecoder output"""
function generate_pcoa_all(kdtbl::String, outPrefix::String)
    @debug "generate_pcoa_all :: START" kdtbl outPrefix
    # Load DF
    df = DataFrame(CSV.File(kdtbl, delim="\t"))

    # Remove any missing values
    dropmissing!(df)
    colNames::Array{String, 1} = names(df)
    colsToDrop::Array{String, 1} = String[]
    tmpCol::String = ""
    tmpStd::Float64 = 0.
    samples::Int = size(df)[1]

    # filter the dataframe so that oit will not have NaN values
    # Columns with all 1 or all 0 will have NaN values after normalization
    for (idx, col) in enumerate(eachcol(df))
        tmpCol = colNames[idx]
        # @show tmpCol, idx
        # Skip the first, second last, and last columns
        if (idx == 1) || (idx == length(colNames) - 1) || (idx == length(colNames))
            continue
        # Process the column
        else
            tmpCol = colNames[idx]
            tmpStd = std(col)
            # Skip if they are all 0 or all 1
            if tmpStd == 0
                push!(colsToDrop, tmpCol)
            end
        end
    end

    @debug "Columns to be dropped to avoid NaN" length(colsToDrop) 

    # remove the columns
    df = select(df, Not(colsToDrop))
    # Update column names
    colNames = names(df)

    # Create mtx with numeric features
    M = Matrix(df[:, 2:length(colNames)-2])
    @show size(M), length(colNames)

    # Extract a list of labels (experimental condition)
    expCond = df[:, :exp_condition]
    # Generate label mapping
    expMap = labelmap(expCond)
    # Encode labels: associate an integer to each label (e.g., Pneg)
    uniqueExpIds = labelencode(expMap, expCond)
    
    # Extract a list of labels (mutants)
    genotype = df[:, :genotype]
    # Generate label mapping
    genoMap = labelmap(genotype)
    # Encode labels: associate an integer to each label (e.g., Col-0)
    uniqueGenotypeIds = labelencode(genoMap, genotype)
    
    # center and normalize the data
    Mnorm = M
    Mnorm = (Mnorm .- mean(Mnorm,dims = 1))./ std(Mnorm, dims=1)

    # Transpose the normalized data
    # since the PCA takes features - by - samples matrix
    MnormT = Mnorm'

    # First, we will fit the model via PCA.
    # maxoutdim is the output dimensions, we want it to be 2 in this case.
    pcaFitted = fit(PCA, MnormT, maxoutdim=2)
    @show pcaFitted
    ### THIS ONLY FOR UNDERSTANDING HOW PCA WORKS ###
    # Project the PCA data to 2D space
    # NOTE: this could have been done simply by using
    # pcaFitted.proj
    pcaProj = projection(pcaFitted)
    
    ################################################
    
    # Transform observations x into principal components
    # notice that Yte[:,1] is the same as P'*(data[1,:]-mean(p))
    Yte = MultivariateStats.transform(pcaFitted, MnormT)
    # In the case of Saijo, the final Yte should be 2 X 7 matrix
    # Where 2 is the reduction of the samples
    # And 7 is the number of KEGG Pathways
    
    # marker size
    msize::Int = 7
    myframe::Symbol = :box # [:box :semi :origin :zerolines :grid :none]
    myratio::Symbol = :equal # aspect_ratio {Symbol (:equal or :none) or Number}

    @show uniqueExpIds
    @show uniqueGenotypeIds
    
    #### 2D PCA plot ####
    Plots.scatter(Yte[1,expCond.=="Ppos"],Yte[2,expCond.=="Ppos"],color=1,label="Pops", dpi=160, legend=:bottomleft, markersize=msize, framestyle=myframe)
    Plots.xlabel!("pca component1")
    Plots.ylabel!("pca component2")
    Plots.scatter!(Yte[1,expCond.=="Pneg"],Yte[2,expCond.=="Pneg"],color=2,label="Pneg", markersize=msize)
    # Plots.scatter!(Yte[1,expCond.=="Europe"],Yte[2,expCond.=="Europe"],color=3,label="Europe")
    ############
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca2d.experimental_conditions.pdf")
    savefig(tmpOutPath)
    

    #### 2D PCA plot by genotype ####
    Plots.scatter(Yte[1,genotype.=="Col-0"],Yte[2,genotype.=="Col-0"],color=1,label="Col-0", dpi=160, legend=:bottomleft, markersize=msize, framestyle=myframe)
    Plots.xlabel!("pca component1")
    Plots.ylabel!("pca component2")
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1"],Yte[2,genotype.=="phr1-phl1"],color=2,label="phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1-lpr1-lpr2"],Yte[2,genotype.=="phr1-phl1-lpr1-lpr2"],color=3,label="phr1-phl1-lpr1-lpr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2-phr1-phl1"],Yte[2,genotype.=="pepr1-pepr2-phr1-phl1"],color=4,label="pepr1-pepr2-phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2"],Yte[2,genotype.=="pepr1-pepr2"],color=5,label="pepr1-pepr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="lpr1-lpr2"],Yte[2,genotype.=="lpr1-lpr2"],color=6 ,label="lpr1-lpr2", markersize=msize)


    ############
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca2d.genotype.pdf")
    savefig(tmpOutPath)

    # Now perform the 3d version
    pcaFitted3dims = fit(PCA, MnormT, maxoutdim=3)
    Yte3dims = MultivariateStats.transform(pcaFitted3dims, MnormT)
    scatter3d(Yte3dims[1,:], Yte3dims[2,:],Yte3dims[3,:], color=uniqueGenotypeIds, markershape=:auto, legend=false)
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca3d.pdf")
    savefig(tmpOutPath)

    # Plot it using Makie
    # scene = Makie.scatter(Yte3dims[1,:],Yte3dims[2,:],Yte3dims[3,:],color=uniqueExpIds)

end



"""Generate a PCoA plot stating from the KEGGDecoder output"""
function generate_pcoa_pneg(kdtbl::String, outPrefix::String)
    @debug "generate_pcoa_pneg :: START" kdtbl outPrefix
    # Load DF
    df = DataFrame(CSV.File(kdtbl, delim="\t"))

    # Remove any missing values
    dropmissing!(df)
    colNames::Array{String, 1} = names(df)
    colsToDrop::Array{String, 1} = String[]
    tmpCol::String = ""
    tmpStd::Float64 = 0.
    samples::Int = size(df)[1]

    # filter the dataframe so that oit will not have NaN values
    # Columns with all 1 or all 0 will have NaN values after normalization
    for (idx, col) in enumerate(eachcol(df))
        tmpCol = colNames[idx]
        # @show tmpCol, idx
        # Skip the first, second last, and last columns
        if (idx == 1) || (idx == length(colNames) - 1) || (idx == length(colNames))
            continue
        # Process the column
        else
            tmpCol = colNames[idx]
            tmpStd = std(col)
            # Skip if they are all 0 or all 1
            # if (tmpStd == 0) || (tmpStd == samples)
            if tmpStd == 0
                push!(colsToDrop, tmpCol)
            end
        end
    end

    @debug "Columns to be dropped to avoid NaN" length(colsToDrop) 

    # remove the columns
    df = select(df, Not(colsToDrop))
    # Update column names
    colNames = names(df)

    # Create mtx with numeric features
    M = Matrix(df[:, 2:length(colNames)-2])
    # @show colNames[1], colNames[end-1], colNames[end]

    @show size(M), length(colNames)
    # Extract a list of labels (experimental condition)
    expCond = df[:, :exp_condition]
    # Generate label mapping
    expMap = labelmap(expCond)
    # Encode labels: associate an integer to each label (e.g., Pneg)
    uniqueExpIds = labelencode(expMap, expCond)
    
    # Extract a list of labels (mutants)
    genotype = df[:, :genotype]
    # Generate label mapping
    genoMap = labelmap(genotype)
    # Encode labels: associate an integer to each label (e.g., Col-0)
    uniqueGenotypeIds = labelencode(genoMap, genotype)
    
    # center and normalize the data
    Mnorm = M
    Mnorm = (Mnorm .- mean(Mnorm,dims = 1))./ std(Mnorm, dims=1)
    # @show size(Mnorm)

    # Transpose the normalized data
    # since the PCA takes features - by - samples matrix
    MnormT = Mnorm'

    @which fit
    # First, we will fit the model via PCA.
    # maxoutdim is the output dimensions, we want it to be 2 in this case.
    pcaFitted = fit(PCA, MnormT, maxoutdim=2)
    @show pcaFitted
    ### THIS ONLY FOR UNDERSTANDING HOW PCA WORKS ###
    # Project the PCA data to 2D space
    # NOTE: this could have been done simply by using
    # pcaFitted.proj
    pcaProj = projection(pcaFitted)
    
    ################################################
    
    # Transform observations x into principal components
    # notice that Yte[:,1] is the same as P'*(data[1,:]-mean(p))
    Yte = MultivariateStats.transform(pcaFitted, MnormT)
    # In the case of Saijo, the final Yte should be 2 X 7 matrix
    # Where 2 is the reduction of the samples
    # And 7 is the number of KEGG Pathways

    # marker size
    msize::Int = 7
    myframe::Symbol = :box # [:box :semi :origin :zerolines :grid :none]
    myratio::Symbol = :equal # aspect_ratio {Symbol (:equal or :none) or Number}

    @show uniqueExpIds
    @show uniqueGenotypeIds

    #### 2D PCA plot by genotype ####
    Plots.scatter(Yte[1,genotype.=="Col-0"],Yte[2,genotype.=="Col-0"],color=1,label="Col-0", dpi=160, legend=:bottomright, markersize=msize, framestyle=myframe)
    Plots.xlabel!("pca component1")
    Plots.ylabel!("pca component2")
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1"],Yte[2,genotype.=="phr1-phl1"],color=2,label="phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1-lpr1-lpr2"],Yte[2,genotype.=="phr1-phl1-lpr1-lpr2"],color=3,label="phr1-phl1-lpr1-lpr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2-phr1-phl1"],Yte[2,genotype.=="pepr1-pepr2-phr1-phl1"],color=4,label="pepr1-pepr2-phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2"],Yte[2,genotype.=="pepr1-pepr2"],color=5,label="pepr1-pepr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="lpr1-lpr2"],Yte[2,genotype.=="lpr1-lpr2"],color=6 ,label="lpr1-lpr2", markersize=msize)

    ############
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca2d.genotype.pdf")
    savefig(tmpOutPath)

    # Now perform the 3d version
    pcaFitted3dims = fit(PCA, MnormT, maxoutdim=3)
    Yte3dims = MultivariateStats.transform(pcaFitted3dims, MnormT)
    scatter3d(Yte3dims[1,:], Yte3dims[2,:],Yte3dims[3,:], color=uniqueGenotypeIds, markershape=:auto, legend=false)
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca3d.pdf")
    savefig(tmpOutPath)

end




"""Generate a PCoA plot stating from the KEGGDecoder output"""
function generate_pcoa_ppos(kdtbl::String, outPrefix::String)
    @debug "generate_pcoa_ppos :: START" kdtbl outPrefix
    # Load DF
    df = DataFrame(CSV.File(kdtbl, delim="\t"))

    # Remove any missing values
    dropmissing!(df)
    colNames::Array{String, 1} = names(df)
    colsToDrop::Array{String, 1} = String[]
    tmpCol::String = ""
    tmpStd::Float64 = 0.
    samples::Int = size(df)[1]

    # filter the dataframe so that oit will not have NaN values
    # Columns with all 1 or all 0 will have NaN values after normalization
    for (idx, col) in enumerate(eachcol(df))
        tmpCol = colNames[idx]
        # @show tmpCol, idx
        # Skip the first, second last, and last columns
        if (idx == 1) || (idx == length(colNames) - 1) || (idx == length(colNames))
            continue
        # Process the column
        else
            tmpCol = colNames[idx]
            tmpStd = std(col)
            # Skip if they are all 0 or all 1
            # if (tmpStd == 0) || (tmpStd == samples)
            if tmpStd == 0
                push!(colsToDrop, tmpCol)
            end
        end
    end

    @debug "Columns to be dropped to avoid NaN" length(colsToDrop) 

    # remove the columns
    df = select(df, Not(colsToDrop))
    # Update column names
    colNames = names(df)

    # Create mtx with numeric features
    M = Matrix(df[:, 2:length(colNames)-2])
    # @show colNames[1], colNames[end-1], colNames[end]

    @show size(M), length(colNames)
    # Extract a list of labels (experimental condition)
    expCond = df[:, :exp_condition]
    # Generate label mapping
    expMap = labelmap(expCond)
    # Encode labels: associate an integer to each label (e.g., Pneg)
    uniqueExpIds = labelencode(expMap, expCond)
    
    # Extract a list of labels (mutants)
    genotype = df[:, :genotype]
    # Generate label mapping
    genoMap = labelmap(genotype)
    # Encode labels: associate an integer to each label (e.g., Col-0)
    uniqueGenotypeIds = labelencode(genoMap, genotype)
    
    # center and normalize the data
    Mnorm = M
    Mnorm = (Mnorm .- mean(Mnorm,dims = 1))./ std(Mnorm, dims=1)
    # @show size(Mnorm)

    # Transpose the normalized data
    # since the PCA takes features - by - samples matrix
    MnormT = Mnorm'

    # First, we will fit the model via PCA.
    # maxoutdim is the output dimensions, we want it to be 2 in this case.
    pcaFitted = fit(PCA, MnormT, maxoutdim=2)
    @show pcaFitted
    ### THIS ONLY FOR UNDERSTANDING HOW PCA WORKS ###
    # Project the PCA data to 2D space
    # NOTE: this could have been done simply by using
    # pcaFitted.proj
    pcaProj = projection(pcaFitted)
    
    ################################################
    
    # Transform observations x into principal components
    # notice that Yte[:,1] is the same as P'*(data[1,:]-mean(p))
    Yte = MultivariateStats.transform(pcaFitted, MnormT)
    # In the case of Saijo, the final Yte should be 2 X 7 matrix
    # Where 2 is the reduction of the samples
    # And 7 is the number of KEGG Pathways

    # marker size
    msize::Int = 7
    myframe::Symbol = :box # [:box :semi :origin :zerolines :grid :none]

    @show uniqueExpIds
    @show uniqueGenotypeIds

    #### 2D PCA plot by genotype ####
    Plots.scatter(Yte[1,genotype.=="Col-0"],Yte[2,genotype.=="Col-0"],color=1,label="Col-0", dpi=160, legend=:bottomright, markersize=msize, framestyle=myframe)
    Plots.xlabel!("pca component1")
    Plots.ylabel!("pca component2")
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1"],Yte[2,genotype.=="phr1-phl1"],color=2,label="phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="phr1-phl1-lpr1-lpr2"],Yte[2,genotype.=="phr1-phl1-lpr1-lpr2"],color=3,label="phr1-phl1-lpr1-lpr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2-phr1-phl1"],Yte[2,genotype.=="pepr1-pepr2-phr1-phl1"],color=4,label="pepr1-pepr2-phr1-phl1", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="pepr1-pepr2"],Yte[2,genotype.=="pepr1-pepr2"],color=5,label="pepr1-pepr2", markersize=msize)
    Plots.scatter!(Yte[1,genotype.=="lpr1-lpr2"],Yte[2,genotype.=="lpr1-lpr2"],color=6 ,label="lpr1-lpr2", markersize=msize)

    ############
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca2d.genotype.pdf")
    savefig(tmpOutPath)

    # Now perform the 3d version
    pcaFitted3dims = fit(PCA, MnormT, maxoutdim=3)
    Yte3dims = MultivariateStats.transform(pcaFitted3dims, MnormT)
    scatter3d(Yte3dims[1,:], Yte3dims[2,:],Yte3dims[3,:], color=uniqueGenotypeIds, markershape=:auto, legend=false)
    tmpOutPath = joinpath(dirname(kdtbl), "$(outPrefix).pca3d.pdf")
    savefig(tmpOutPath)

    # Plot it using Makie
    # scene = Makie.scatter(Yte3dims[1,:],Yte3dims[2,:],Yte3dims[3,:],color=uniqueExpIds)

end


##### MAIN #####
# obtain CLI arguments
args = docopt(doc)
tblPath = realpath(args["--tbl"])
outRoot = abspath(args["--output-root"])
outDir = abspath(args["--output-dir"])
outPrefix = args["--output-prefix"]
debug = args["--debug"]

# Create and activate the logger
logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "compute_pca_from_keggdecoder_table.jl" tblPath outRoot outDir outPrefix

# add Path to the modules directory
push!(LOAD_PATH, joinpath(dirname(@__FILE__), "modules"))
# @show LOAD_PATH
# deploy the workers
# addprocs(threads)

# import the modules
include(joinpath(dirname(@__FILE__), "modules/systools.jl"))
include(joinpath(dirname(@__FILE__), "modules/seqAnnotation.jl"))

# create output directory
smplPathsDict = extract_run_names(tblPath, outRoot)
systools.makedir(outDir)
workDir = tmpPath = tmpKfOutName = annotDir = kdtbl = kdsvg = kdhtml = ""
tmpSmplKeggDecoder = tmpKdInput = ""
# Create working directory in which the processing will happen
workDir = mktempdir(outDir, prefix="kd_input_", cleanup=false)
kdFinalInput = joinpath(workDir, "$(outPrefix).kdecoder.input.tsv")
tmpCmd = ``

sampleNames = String[]
#=
for (k, v) in smplPathsDict
    # Copy raw kfam input
    global tmpKfOutName = "$(k).bins.kofamscan.tsv"
    # check if the gz compressed file exists
    tmpKfOut = joinpath(v["annotdir"], "$(tmpKfOutName).gz")
    if isfile(tmpKfOut)
        # make sure it is an archive
        if systools.is_archive(tmpKfOut)[1]
            systools.pigz_decompress(tmpKfOut, joinpath(workDir, tmpKfOutName), keep=true, force=true, errLog=true, threads=4)
        end
    else # simply copy the file
        tmpKfOut = joinpath(v["annotdir"], tmpKfOutName)
        cp(tmpKfOut, joinpath(workDir, tmpKfOutName))
    end
    
    # update file path for kofamscan out
    tmpKfOut = joinpath(workDir, tmpKfOutName)
    # @show tmpKfOut
    # Generate the input file KEGGDecoder
    global tmpSmplKeggDecoder = replace(k, "_" => "-")
    push!(sampleNames, tmpSmplKeggDecoder)
    # @show tmpSmplKeggDecoder
    # global tmpKdInput = joinpath(workDir, "$(tmpSmplKeggDecoder).kdecoder.input.tsv")
    # @show tmpKdInput
    seqAnnotation.convert_kofamout2keggdecoder(tmpKfOut, kdFinalInput, ksformat="detail-tsv", prefix=tmpSmplKeggDecoder, appendMode=true)
end


sort!(sampleNames)
sortingFile = joinpath(outDir, "$(outPrefix).kdecoder.sample_order.txt")
ofdSort = open(sortingFile, "w")
for el in sampleNames
    write(ofdSort, "$(el)\n")
end
close(ofdSort)
=#

#=
# Generate KEGGDecoder Heatmap and table
# Generate heatmaps
kdtbl = joinpath(workDir, "$(outPrefix).kdecoder.out.tsv")
kdhtml = joinpath(workDir, "$(outPrefix).kdecoder.out.html")
kdsvg = joinpath(workDir, "$(outPrefix).kdecoder.out.svg")
# KEGG-decoder -i input.tsv -o out.list.tsv -v static
tmpCmd = `KEGG-decoder -i $(kdInputFinal) -o $(kdtbl) -v static`
@debug "KEGG-Decoder CMD" tmpCmd
run(pipeline(tmpCmd), wait=true)
# Check that the SVG file was generated
if !isfile(kdsvg)
    @error "The SVG heatmaps could not be generated" kdsvg kdtbl
    exit(-6)
end

# Generate the static HTML plot of there are more than 2 samples
global tmpCmd = `KEGG-decoder -i $(kdInputFinal) -o $(kdtbl) -v interactive`
@debug "KEGG-Decoder CMD" tmpCmd
run(pipeline(tmpCmd), wait=true)
# Check that the SVG file was generated
if !isfile(kdhtml)
    @error "The HTML heatmaps could not be generated" kdhtml kdtbl
    exit(-6)
end


# move the file with the KeggDecoder input
tmpPath = kdInputFinal
kdInputFinal = joinpath(outDir, basename(kdInputFinal))
cp(tmpPath, kdInputFinal)

# copy the TSV
tmpPath = kdtbl
kdtbl = joinpath(outDir, basename(kdtbl))
cp(tmpPath, kdtbl)

# copy the SVG
tmpPath = kdsvg
kdsvg = joinpath(outDir, basename(kdsvg))
cp(tmpPath, kdsvg)

# copy the SVG
tmpPath = kdhtml
kdhtml = joinpath(outDir, basename(kdhtml))
cp(tmpPath, kdhtml)

# remove tmp directory
rm(workDir, recursive=true, force=true)
=#

# Set the plotting backend
plotlyjs()
# gr()

# Generate PCoA
kdtbl = joinpath(outDir, "$(outPrefix).kdecoder.out.mod.tsv")
# # @show kdtbl, isfile(kdtbl)
# generate_pcoa_all(kdtbl, outPrefix)

# Plot PCoA PPos
outPrefix = "saijo_ppos_samples"
kdtbl = joinpath(outDir, "$(outPrefix).kdecoder.out.mod.tsv")
# @show kdtbl, isfile(kdtbl)
# generate_pcoa_ppos(kdtbl, outPrefix)

# Plot PCoA PNeg
outPrefix = "saijo_pneg_samples"
kdtbl = joinpath(outDir, "$(outPrefix).kdecoder.out.mod.tsv")
# @show kdtbl, isfile(kdtbl)
generate_pcoa_pneg(kdtbl, outPrefix)
