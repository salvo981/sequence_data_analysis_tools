"""
Methods for processing metagenomic reads.
"""

module metagenomes
using Logging: @warn, @debug, @info
using Printf: @printf, @sprintf
include("./systools.jl")
include("./seq_tools.jl")


struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("metagenoms", "metagenomes.jl", "Salvatore Cosentino", "GPLv3", 0.1, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end



"""Merge metaphlan result files and include only species level results"""
function merge_metaphlan_species(pathsList::String, outPath::String="", trimSmplName::Bool=true, debug::Bool=false)
    # trimSmplName: assumes the sample name is the part of each results file,
    # right on the left of the first '.' in the basename of the path
    # if true the that part of the filename will be sued as sample name
    if debug
        println("\nmerge_metaphlan_species@$(@__MODULE__) :: START")
        @show pathsList
        @show outPath
        @show trimSmplName
    end

    # check that the input exists
    if !isfile(pathsList)
        println("ERROR: the file $(pathsList) does not exist!")
        exit(-2)
    end
    
    # create the ouitput direcotry if required
    if !isdir(dirname(outPath))
        systools.makedir(dirname(outPath))
    end

    # Dictionary with the species abundances for each sample
    abundanceDict::Dict{String, Dict{String, Float64}} = Dict{String, Dict{String, Float64}}()
    # Other supporting structures
    species::Array{String, 1} = Array{String, 1}()
    smpls::Array{String, 1} = Array{String, 1}()
    flds::Array{String, 1} = Array{String, 1}()

    # tmp variables
    tmpSp::String = ""
    smplName::String = ""
    tmpAbundance::Float64 = 0.0
    fcnt::Int = 0

    # Read the file with result file paths
    for p in eachline(open(pathsList, "r"))
        # check that the result file exists
        if !isfile(p)
            println("ERROR: the result file $(p) does not exist!")
            exit(-2)
        else
            fcnt += 1
        end
        # @show basename(p)
        if trimSmplName
            smplName = split(basename(p), ".", limit=2)[1]
        else
            smplName = basename(p)
        end
        # add the sample to the list if required
        if !(smplName in smpls)
            push!(smpls, smplName)
            # create the entry in dictionary
            abundanceDict[smplName] = Dict{String, Float64}()
        else
            println("ERROR: the sample $(smplName) appers multiple times!")
            exit(-5)
        end
        # Extract info from the results file
        for ln in eachline(open(p, "r"))
            flds = split(ln, "|s__", limit=2)
            # @show length(flds)
            # skip the line if does not contain species information
            if length(flds) == 1
                continue
            else
                flds = split(flds[2], "\t", limit=4)
                tmpSp = flds[1]
                tmpAbundance = parse(Float64, flds[3])
                # add the species in the array if needed
                if !(tmpSp in species)
                    push!(species, tmpSp)
                end
                # create the entry in dictionary
                if !(haskey(abundanceDict[smplName], tmpSp))
                    abundanceDict[smplName][tmpSp] = tmpAbundance
                else
                    println("ERROR: the species $(tmpSp) appears multiple times in the same results file!\n$(p)")
                    exit(-5)
                end
            end
        end
        # break
    end

    # output some debug
    if debug
        @printf("\nParsed result files:\t%d\n", fcnt)
        @printf("Species found:\t%d\n", length(species))
    end

    # sort the species and sample names
    sort!(species)
    sort!(smpls)
    # tmp abundance array
    tmpAbdArray::Array{Float64, 1} = Array{Float64, 1}()
    # open the output file
    ofd::IOStream = open(outPath, "w")
    # write the header
    write(ofd, @sprintf("Species\t%s\n", join(smpls, "\t")))
    # print the results
    for sp in species
        write(ofd, @sprintf("%s\t", sp))
        # now write the abundance for each sample
        for tmpSmpl in smpls
            # println(tmpSmpl)
            # write the abundance if present
            if haskey(abundanceDict[tmpSmpl], sp)
                push!(tmpAbdArray, abundanceDict[tmpSmpl][sp])
                # write(ofd, @sprintf("%f\t", abundanceDict[tmpSmpl][sp]))
                # set the abundance to 0
            else
                push!(tmpAbdArray, 0)
            end
        end

        # write the abundance values
        if length(tmpAbdArray) != length(smpls)
            println("\nERROR: there are more elements than the avaliable species in the abundance line!")
            @show(length(tmpAbundance), length(species))
            exit(-5)
        else
            write(ofd, @sprintf("%s", join(tmpAbdArray, "\t")))
        end
        # close the line
        write(ofd, "\n")
        # empty the array with abundances
        empty!(tmpAbdArray)
    end
    close(ofd)
end



"""Use MetaPhlAn3 to compute relative species abundance"""
function metaphlan3(r1::String, r2::String="", outDir::String="", outName::String="methaphlan_abundance", threads::Int=4, debug::Bool=false)::String
    if debug
        println("\nmetaphlan3@$(@__MODULE__) :: START")
        @show r1
        @show r2
        @show outDir
        @show outName
        @show threads
    end

    # MetaPhlAn3 is available at
    # https://github.com/biobakery/MetaPhlAn/tree/3.0

    # prepare output paths
    mpaOutPath::String = joinpath(outDir, "$(outName).profiled.txt")
    bwtOutPath::String = joinpath(outDir, "$(outName).bowtie2.bz2")
    logPath::String = joinpath(outDir, "$(outName).metaphlan.log.txt")
    
    # check the the input exists
    if !isfile(r1)
        println("ERROR: the file $(r1) does not exist!")
        exit(-2)
    end
    # Set the run as paired or not
    isPaired::Bool = true
    if r2 == ""
        isPaired = false
    end
    
    if isPaired
        if !isfile(r2)
            println("ERROR: the file $(r2) does not exist!")
            exit(-2)
        end
    end

    # MetaPhlAn3 example
    # metaphlan sample1_r1.fastq,sample1_r2.fastq --bowtie2out sample1.bowtie2.bz2 --nproc 24 --input_type fastq -o sample1.profiled.txt
    
    # prepare the command
    tmpCmd::Cmd = ``
    if isPaired
        tmpCmd = `metaphlan $(r1),$(r2) --bowtie2out $(bwtOutPath) --nproc $(threads) --input_type fastq -o $(mpaOutPath)`
    else # SE run
        tmpCmd = `metaphlan $(r1) --bowtie2out $(bwtOutPath) --nproc $(threads) --input_type fastq -o $(mpaOutPath)`
    end

    # Open the log file and start the analysis
    lfd::IOStream = open(logPath, "w")
    write(lfd, @sprintf("MetaPhlAn3 Analysis :: START"))
    write(lfd, @sprintf("\n%s\n\nOutput:\n", string(tmpCmd)))
    close(lfd)
    # Reopen in append mode and run the analysis
    lfd = open(logPath, "a")
    procOut::Base.Process = run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went wrong during the MetaPhlAn3 preprocessing, check the log file\n$(logPath)")
        sys.exit(-5)
    end
    close(lfd)

    return mpaOutPath
end



"""Perform custom processing steps of the MetaWRAP pipeline.
"""
function metawrap(r1::String, r2::String="", outDir::String="", smplName::String="metawrap", skipAssembly::Bool=false, runKraken::Bool=false, classifyBins::Bool=false, maxContami::Int=10, minCompleteness::Int=70, threads::Int=4, memory::Int=200, moveInput::Bool=true)::Dict{String, String}
    
    @debug "metawrap :: START" r1 r2 outDir smplName skipAssembly runKraken classifyBins maxContami minCompleteness threads memory moveInput
    # MetaWRAP is available at
    # https://github.com/bxlab/metaWRAP
    
    # We will perform the following steps:
    # Assembly, kraken*, binning, bin_refinement, bins_classification
    
    outDirs::Dict{String, String} = Dict("assembly" => joinpath(outDir, "assembly"), "binning" => joinpath(outDir, "initial_binning"), "bin_refinement" => joinpath(outDir, "bin_refinement"))
    outDirs["clean_reads"] = joinpath(outDir, "clean_reads")
    if runKraken
        outDirs["kraken"] = joinpath(outDir, "kraken")
    end
    if classifyBins
        outDirs["classify_bins"] = joinpath(outDir, "final_bins_taxonomy")
    end
    
    # add other output directories
    outDirs["blobology"] = joinpath(outDir, "blobology")
    outDirs["bin_reassembly"] = joinpath(outDir, "bin_reassembly")

    # Create the output directories
    systools.makedir(outDir)
    for (k, v) in outDirs
        systools.makedir(v)
    end

    # temporary variables
    tmpPath::String = ""
    # Generate the log file for the metaWRAP execution
    lfd::IOStream = open(joinpath(outDir, "log.metawrap.$smplName.txt"), "w")
    # Write some run info
    write(lfd, "Metagenomic analysis on $(smplName) using metaWRAP.\n\n")
    write(lfd, "sample:\t$smplName\nr1: $r1\nr2: $r2\nthreads:\t$threads\nrunKraken:\t$runKraken\nclassifyBins:\t$classifyBins\n")
    
    # When moveInput is true, the original input reads are moved under the directory clean_reads
    # additionally the read files are renamed using the following pattern
    # sample_name_1.fastq and sample_name_2.fastq
    # This pattern for file naming is required by some of the Metawrap programs (eg binning)
    if moveInput
        write(lfd, "\nMoving input file into \n$(outDirs["clean_reads"])\nand renaming...\n")
        tmpPath = joinpath(outDirs["clean_reads"], "$(smplName)_1.fastq")
        if r1 != tmpPath
            mv(r1, tmpPath, force=true)
            r1 = tmpPath
        end
        tmpPath = joinpath(outDirs["clean_reads"], "$(smplName)_2.fastq")
        if r2 != tmpPath
            mv(r2, tmpPath, force=true)
            r2 = tmpPath
        end
    end
    
    flush(lfd)

    # Set temporary variables
    tmpCmd::Cmd = `echo whatever`
    tmpPath = joinpath(outDirs["assembly"], "final_assembly.fasta")
    ctgsPath::String = joinpath(outDirs["assembly"], "assembly.$smplName.fna")
    skipAllowed::Bool = false # set to true if the assembly can be skipped

    # Skip the assembly if possible
    if skipAssembly
        if isfile(ctgsPath)
            println("\nSkipping assembly, as conting are avaliable in $(ctgsPath)")
            skipAllowed = true
        elseif isfile(joinpath(ctgsPath, ".gz"))
            systools.pigz_decompress(joinpath(ctgsPath, ".gz"), ctgsPath, keep=true, force=true, errLog=true, threads=threads)
            skipAllowed = true
        else
            println("\nThe contigs assembly file do not exist hence the assembly is required")
        end
    end

    # Perform the assembly if required
    if !skipAllowed
        println("\nmetawrap assembly...")
        # Metagenomic Assembly (metaSpades)
        # Consider using MegaHits if this step is too slow
        # EXAMPLE: metawrap assembly -1 final_pure_reads_1.fastq -2 final_pure_reads_2.fastq -t 32 --metaspades -m 360 -o assembly/
        tmpCmd = `metawrap assembly -1 $(r1) -2 $(r2) -t $(threads) --megahit -m $(memory) -o $(outDirs["assembly"])`
        # Consider using --megahit for the assembly
        # tmpCmd = `metawrap assembly -1 $(r1) -2 $(r2) -t $(threads) --metaspades -m $(memory) -o $(outDirs["assembly"])`
        @show tmpCmd
        # execute command    
        run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
        if !isfile(tmpPath)
            @error "The assembly file could not be found! Something went wrong with the assembly." tmpPath
            exit(-6)
        else # rename the assembly file
            mv(tmpPath, ctgsPath, force=false)
        end

        # Count contigs in assembly
        # write(lfd, "\nAssembled contigs:\t$(seq_tools.count_reads(ctgsPath))\n")
    else
        @info "Assembly already generated, skipping assembly" ctgsPath
    end

    flush(lfd)

    if runKraken
        println("\nmetawrap kraken...")
        # Run Kraken if needed
        # EXAMPLE: metawrap kraken -o kraken/ -t 32 final_pure_reads_*fastq assembly/final_assembly.fasta
        tmpPath = joinpath(outDirs["clean_reads"], "$(smplName)_*fastq")
        tmpCmd = `metawrap kraken -o $(outDirs["kraken"]) -t $(threads) $(tmpPath) $(ctgsPath)`
        @show tmpCmd
        run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    end

    # Perform binning
    println("\nmetawrap binning...")
    # Consider removing CONCOCT binning
    # EXAMPLE: metawrap binning -o initial_binning/ -a assembly/final_assembly.fasta -t 32 --metabat2 --maxbin2 --concoct final_pure_reads_* -m 360
    tmpList::Array{String, 1} = ["--metabat2", "--maxbin2", "--concoct"]
    # tmpList::Array{String, 1} = ["--metabat2", "--maxbin2"]
    cleanReads::String = joinpath(outDirs["clean_reads"], "$(smplName)_*fastq")
    tmpCmd = `metawrap binning -o $(outDirs["binning"]) -a $(ctgsPath) -t $(threads) $tmpList -m $(memory) $(cleanReads)`
    @show tmpCmd
    run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)

    # Bin refinement
    println("\nmetawrap bin_refinement...")
    # EXAMPLE: metawrap bin_refinement -o bin_refinement/ -t 32 -A initial_binning/metabat2_bins/ -B initial_binning/maxbin2_bins/ -C initial_binning/concoct_bins/ -c 70 -x 5
    # tmpList = ["-A" ,joinpath(outDirs["binning"], "metabat2_bins"), "-B" ,joinpath(outDirs["binning"], "maxbin2_bins")]
    tmpList = ["-A" ,joinpath(outDirs["binning"], "metabat2_bins"), "-B" ,joinpath(outDirs["binning"], "maxbin2_bins"), "-C" ,joinpath(outDirs["binning"], "concoct_bins")]
    tmpCmd = `metawrap bin_refinement -o $(outDirs["bin_refinement"]) -t $(threads) $tmpList -m $(memory) -c $(minCompleteness) -x $(maxContami)`
    @show tmpCmd
    run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)

    println("\nmetawrap blobology...")
    # EXAMPLE: metawrap blobology -a ASSEMBLY/final_assembly.fasta -t 96 -o BLOBOLOGY --bins BIN_REFINEMENT/metawrap_bins CLEAN_READS/ERR*fastq
    tmpPath = joinpath(outDirs["bin_refinement"], "metawrap_$(minCompleteness)_$(maxContami)_bins")
    tmpCmd = `metawrap blobology -a $(ctgsPath) -t $(threads) -o $(outDirs["blobology"]) --bins $tmpPath $(cleanReads)`
    @show tmpCmd
    run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)

    if classifyBins
        println("\nmetawrap classify_bins...")
        # EXAMPLE: metawrap classify_bins -b <final_bins_dir> -o <output_dir> -t 40
        tmpCmd = `metawrap classify_bins -t $(threads) -b $(tmpPath) -o $(outDirs["classify_bins"])`
        @show tmpCmd
        run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    end

    #### REASSIBLY IS NOT REQUIRED and not working on Beyond ####
    #=
    # Reassemble Bins
    println("\nmetawrap reassemble_bins...")
    # EXAMPLE: metawrap reassemble_bins -o BIN_REASSEMBLY -1 CLEAN_READS/ALL_READS_1.fastq -2 CLEAN_READS/ALL_READS_2.fastq -t 96 -m 800 -c 50 -x 10 -b BIN_REFINEMENT/metawrap_bins
    tmpCmd = `metawrap reassemble_bins -o $(outDirs["bin_reassembly"]) -1 $(r1) -2 $(r2) -t $(threads) -m $(memory) -b $(tmpPath)`
    @show tmpCmd
    run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    =#

    close(lfd)

    return outDirs
end



"""Removed temporary files generated by Metawrap and archive some when required"""
function metawrap_cleanup(smplName::String, outDirs::Dict{String, String}, minCompl::Int, maxContami::Int, threads::Int=4)
    @debug "metawrap_cleanup :: START" smplName outDirs minCompl maxContami threads

    # tmp variables
    tmpDir::String = tmpPath = ""
    tmpList::Array{String, 1} = []
    binsDirs::Array{String, 1} = ["concoct_bins", "metabat2_bins", "maxbin2_bins", "metawrap_$(minCompl)_$(maxContami)_bins"]

    # Do a different thing depending on the output directory
    for (k, v) in outDirs
        tmpDir = v
        if k == "assembly"
            println("\nCleaning $(k) directory")
            tmpList = ["megahit", "metaspades"]
            for d in tmpList
                # Remove directory with temporary files
                tmpDir = joinpath(v, d)
                println("Removing $(tmpDir)")
                rm(tmpDir, force=true, recursive=true)
            end
            # compress the assembly file
            tmpPath = joinpath(v, "assembly.$smplName.fna")
            if isfile(tmpPath)
                systools.pigz_compress(tmpPath, tmpPath, level=9, keep=false, force=true, useGzip=true, errLog=false, threads=threads)
            end
        elseif k == "binning"
            println("\nCleaning $(k) directory")
            tmpDir = joinpath(v, "work_files")
            println("Removing $(tmpDir)")
            rm(tmpDir, force=true, recursive=true)
            # compress dirs if required
            for d in binsDirs
                tmpDir = joinpath(v, d)
                if isdir(tmpDir)
                    println("Removing $(tmpDir)")
                    rm(tmpDir, force=true, recursive=true)
                end
            end
        elseif k == "bin_refinement"
            println("\nCleaning $(k) directory")
            tmpDir = joinpath(v, "work_files")
            println("Removing $(tmpDir)")
            rm(tmpDir, force=true, recursive=true)
            # compress dirs if required
            for d in binsDirs
                tmpDir = joinpath(v, d)
                if isdir(tmpDir)
                    println("Compressing bins in $(tmpDir)") # condire creating a tar.gz archive
                    for p in readdir(tmpDir, join=true, sort=false)
                        if !systools.is_archive(p)[1]
                            systools.pigz_compress(p, p, level=11, keep=false, force=true, useGzip=true, errLog=false, threads=threads)
                        end
                    end
                end
            end
        elseif k == "kraken"
            println("\nCleaning $(k) directory")
            tmpPath = joinpath(v, "$(smplName).kraken")
            if isfile(tmpPath)
                println("Removing kraken temporary file:\n$(tmpPath)")
                rm(tmpPath, force=true, recursive=false)
            end
        elseif k == "clean_reads"
            println("\nCleaning $(k) directory")
            tmpPath = joinpath(v, "$(smplName)_1.fastq")
            if isfile(tmpPath)
                println("Compressing set of clean reads:\n$(tmpPath)")
                systools.pigz_compress(tmpPath, tmpPath, level=9, keep=false, force=true, useGzip=true, errLog=true, threads=threads)
            end
            tmpPath = joinpath(v, "$(smplName)_2.fastq")
            if isfile(tmpPath)
                println("Compressing set of clean reads:\n$(tmpPath)")
                systools.pigz_compress(tmpPath, tmpPath, level=9, keep=false, force=true, useGzip=true, errLog=true, threads=threads)
            end
        elseif k == "blobology"
            println("\nCleaning $(v) directory")
            # List the directory and keep only directories inside it
            for l in readdir(v, join=true)
                tmpPath = joinpath(v, l)
                if isfile(tmpPath)
                    rm(tmpPath)
                end
            end
        elseif k == "classify_bins"
            println("\nCleaning $(v) directory")
            tmpList = ["all_contigs.fa", "megablast_out.raw.tab", "megablast_out.pruned.tab", "megablast_out.tab", "mapping.tax", "binning.log", "predictions.gff3", "binned_predictions.txt"]
            # remove the files that are not required
            for n in tmpList
                tmpPath = joinpath(v, n)
                println("Removing $(tmpPath)")
                rm(tmpPath, force=true)
            end
        end
    end

end



end # module end
