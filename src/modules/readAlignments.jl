"""Utility methods aligning and extracting reads using other tools (eg, bowtie or samtools)."""

module readAlignments

include("./systools.jl")
using Logging: @warn, @debug, @info
using Printf: @printf, @sprintf
using DataStructures: SortedDict



"""
Information on the module.
"""
struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""
Represents a genomic interval.
"""
struct Genoint
    seq::String
    startpos::Int
    endpos::Int
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("Read alignments", "readAlignments.jl", "Salvatore Cosentino", "GPLv3", 0.2, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @info mod.name mod.source mod.author mod.license mod.version mod.maintainer mod.email
    end
    # return the module info struct
    return mod
end



"""Create genomic intervals"""
function create_genomic_intervals(chromotbl::String; window::Int=10000)::Array{Genoint, 1}
    @debug "create_genomic_intervals :: START" chromotbl window
    # The input table must have 2 columns
    # Sequence name (e.g., chromosome)
    # Sequence lenght (e.g. genome or chromosome size)
    # window is the size of the window defining the length of each interval

    genomeSizes::SortedDict = SortedDict{String, Int}()
    tmpFlds::Array{String, 1} = String[]
    # load the genome sizes
    for ln in eachline(chromotbl)
        tmpFlds = split(ln, "\t", limit=2)
        genomeSizes[tmpFlds[1]] = parse(Int, tmpFlds[2])
    end

    # Temporary Genomic interval
    prevpos::Int = -1
    genomicIntervals::Array{Genoint, 1} = Array{Genoint, 1}()
    # generate the ranges based on the window and genome size
    for (seq, gensize) in genomeSizes
        for (i, position) in enumerate(collect(range(1, stop=gensize, step=window)))
            # @show i, position
            if i ==2
                push!(genomicIntervals, Genoint(seq, 1, position - 1))
                # Create the genomic interval and add it to the Array
                # prevpos = position - 1
            elseif i > 2
                prevpos = genomicIntervals[end].endpos + 1
                push!(genomicIntervals, Genoint(seq, prevpos, position - 1))
            end
        end

        # add the kast genomic interval if required
        if gensize - genomicIntervals[end].endpos > 0
            push!(genomicIntervals, Genoint(seq, genomicIntervals[end].endpos + 1, gensize))
        end
    end

    return genomicIntervals

end



"""Create a indexed bowtie2 DB from a reference FASTA.
   Returns a string with the sequence format and a boolean saying if the file is compressed
"""
function create_bowtie2_idx(rpath::String, outdir::String; prefix::String="bowtie_ref_db", threads::Int=4)::String
    @debug "create_bowtie2_idx :: START" rpath outdir prefix threads
    prevdir::String = pwd()
    # create the output directory if required
    systools.makedir(outdir)
    # Bowtie2 indexing command example
    # bowtie2-build --threads 32 <reference.fa> <output_prefix>
    cd(outdir)
    tmpCmd::Cmd = `bowtie2-build --threads $(threads) $(rpath) $(prefix)`
    stdo::String = stde = ""
    stdo = joinpath(outdir, "stdout.bowtie2_build.$(prefix).txt")
    stde = joinpath(outdir, "stderr.bowtie2_build.$(prefix).txt")
    run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    # restore working directory
    cd(prevdir)
    # This is the path should be used in the -x parameter in bowtie2
    return joinpath(outdir, "$(prefix)") 
end


"""Given an input SAM/BAM alignment extract the reads using samtools fastq.
Returns the count of extracted reads.
"""
function extract_reads(inbam::String, outdir::String, o1::String, o2::String, outprefix::String, filter::Array{String,1}, threads::Int=8)::Int
    @debug "extract_reads :: START" inbam outdir o1 o2 outprefix filter threads

    # string representation of filter
    cleanFilter::String = join([replace(x, "-" => "") for x in filter], "")
    runName::String = rsplit(basename(inbam), ".", limit=2)[1]
    mapTmpBam::String = joinpath(outdir, "$(runName)_$(outprefix)_tmp.bam")
    mapTmpBamSort::String = joinpath(outdir, "$(runName)_$(outprefix)_tmp_sorted.bam")
    runLog::String = joinpath(outdir, "log.extract_reads.$(runName)_$(outprefix).txt")
    if length(outprefix) == 0
        mapTmpBam = joinpath(outdir, "$(runName)_tmp.bam")
        mapTmpBamSort = joinpath(outdir, "$(runName)_tmp_sorted.bam")
        runLog = joinpath(outdir, "log.extract_reads.$(runName).txt")
    end

    # open log file
    ofd = open(runLog, "w")
    # Mapping filtering
    # for more info on filtering go to the following webpage
    # https://broadinstitute.github.io/picard/explain-flags.html
    # -f 3 extract reads with both forwards and reverse mapping to the same contigs (properly mapped)
    # -f3 -F2048 excludes supplementary alignments as well
    # -F 12 extract all the mapping reads excluding those which have the mate unmapped
    # -L <ref.bed> extract only reads overlapping the provided BED file
    
    # Extract alignments
    compressionSettings::String = "-c 9"
    # if (o1[end-2:end] == ".gz") || o2[end-2:end] == ".gz"
    #     compressionSettings = "-c 9"
    # end
    tmpCmd::Cmd = `samtools fastq -@ $(threads) -1 $o1 -2 $o2 -0 /dev/null -s /dev/null -n $(filter) $(compressionSettings) $inbam`
    run(pipeline(tmpCmd, stdout=ofd, stderr=ofd), wait=true)
    extracted = countlines(o1)/2
    close(ofd)

    return extracted
end



"""Check if the input SAM/BAM is sorted."""
@inline function is_sorted_aln(inpath::String)::Tuple{Bool, String}
    @debug "is_sorted_aln :: START" inpath

    # Input format
    fmt::String = ""
    fmt = lowercase(rsplit(inpath, ".", limit=2)[2])
    # make sure the output file is eaither in BAM or SAM
    if fmt ∉ ["bam", "sam"]
        @error "The input file must be in either BAM or SAM format. Please add the appropriate extension to the input path." inpath fmt
        exit(-4)
    end

    return (false, "This function is not complete...we need a mode to read the BAM/SAM first line.")
end



"""Map read sets to a reference sequence DB using Bowtie2.
"""
function map_reads_bowtie2(r1::String, r2::String, refdb::String, outpath::String; threads::Int=4)
    # Set the run type (SE | PE)
    se::Bool = true
    # check if it is PE run
    if length(r2) > 0
        se = false
    end
    @debug "map_reads_bowtie2 :: START" r1 r2 refdb outpath threads se
    # create the output directory if required
    outdir::String = dirname(outpath)
    systools.makedir(outdir)
    # generate the Bowtie2 indexes if not present
    if !isfile("$(refdb).1.bt2") && !isfile("$(refdb).1.bt2l")
        @error "The file with the Bowtie2 idx could not be found. Please generate it before continuing." refdb
        exit(-6)
    end

    # Bowtie2 mapping example
    # bowtie2 --threads <t> -x <bt2-idx> -1 <r1> -2 <r2> -S <out.sam>
    tmpCmd::Cmd = `bowtie2 --threads $(threads) -x $(refdb) -1 $(r1) -S $(outpath)`
    if !se
        tmpCmd = `bowtie2 --threads $(threads) -x $(refdb) -1 $(r1) -2 $(r2) -S $(outpath)`
    end
    stdo::String = stde = ""
    stdo = joinpath(outdir, "stdout.bowtie2.$(basename(outpath)).txt")
    stde = joinpath(outdir, "stderr.bowtie2.$(basename(outpath)).txt")
    run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    # make sure the mapping finished succesfully
    if !isfile(outpath)
        @error "The file with alignments could not be found. Bowtie2 might have failed." outpath stdo stde
        exit(-6)
    end
end



"""Compute different stats from BAM file using different samtools programs samtools."""
function samtools_compute_stats(inpath::String, outdir::String; outprefix::String="samtools_stats", refpath::String="", plotstats::Bool=false, threads::Int=4)::Array{String, 1}
    @debug "samtools_compute_stats :: START" inpath outdir outprefix refpath plotstats threads
    # create the output directory if required
    systools.makedir(outdir)
    # Set to true if the reference is missing
    withref::Bool = true
    if !isfile(refpath)
        withref = false
        @warn "A valid path to the reference FASTA file was not provided. The GC content will not be computed." refpath
    end

    # Array with paths to the generated files
    outPaths::Array{String, 1} = String[]
    # Output format
    fmt::String = progname = ""
    fmt = lowercase(rsplit(inpath, ".", limit=2)[2])
    # Change the output prefix if needed
    if length(outprefix) == 0
        outprefix = rsplit(basename(inpath), ".", limit=2)[1]
    end
    # make sure the output file is eaither in BAM or SAM
    if fmt ∉ ["bam", "sam"]
        @error "The input file must be a sorted BAM/SAM alignment file. Please add the BAM extension to the output file." inpath fmt
        exit(-4)
    # Create the index if missing
    elseif (fmt == "bam") && (!isfile("$(inpath).bai"))
        samtools_index(inpath, threads=threads)
    end
    
    ### samtools flagstat ###
    # Example: samtools flagstat -@ 32 <input.bam>
    progname = "flagstat"
    tmpCmd::Cmd = `samtools $(progname) -@ $(threads) $(inpath)`
    stdo::String = stde = tmpout = plotsprefix = ""
    tmpout = joinpath(outdir, "samtools.$(progname).$(outprefix).txt")
    stde = joinpath(outdir, "stderr.samtools.$(progname).$(outprefix).txt")
    @debug "Running $(progname) on $(basename(inpath))..." tmpCmd stdo stde
    rout::Base.Process = run(pipeline(tmpCmd, stdout=tmpout, stderr=stde), wait=true)

    # Make sue nothing went wrong
    if rout.exitcode != 0
        @error "Something went wrong while running $(progname)." rout.exitcode rout.cmd tmpout stde
        exit(-5)
    end
    # add output path
    push!(outPaths, tmpout)

    ### samtools idxstat ###
    # Example: samtools idxstat -@ 32 <input.bam>
    progname = "idxstat"
    tmpCmd = `samtools $(progname) -@ $(threads) $(inpath)`
    stdo = stde = tmpout = ""
    tmpout = joinpath(outdir, "samtools.$(progname).$(outprefix).txt")
    stde = joinpath(outdir, "stderr.samtools.$(progname).$(outprefix).txt")
    @debug "Running $(progname) on $(basename(inpath))..." tmpCmd stdo stde
    rout = run(pipeline(tmpCmd, stdout=tmpout, stderr=stde), wait=true)

    # Make sue nothing went wrong
    if rout.exitcode != 0
        @error "Something went wrong while running $(progname)." rout.exitcode rout.cmd tmpout stde
        exit(-5)
    end
    # add output path
    push!(outPaths, tmpout)

    # Add extra parameters
    opts::Array{String, 1} = String[]
    if withref
        empty!(opts)
        push!(opts, "--ref-seq", refpath)
    end

    ### samtools stats ###
    #Example: samtools stats -@ 32 <input.bam>
    progname = "stats"
    tmpCmd = `samtools $(progname) -@ $(threads) $(opts) $(inpath)`
    stdo = stde = tmpout = ""
    tmpout = joinpath(outdir, "samtools.$(progname).$(outprefix).txt")
    stde = joinpath(outdir, "stderr.samtools.$(progname).$(outprefix).txt")
    @debug "Running $(progname) on $(basename(inpath))..." tmpCmd stdo stde
    rout = run(pipeline(tmpCmd, stdout=tmpout, stderr=stde), wait=true)

    # Make sue nothing went wrong
    if rout.exitcode != 0
        @error "Something went wrong while running $(progname)." rout.exitcode rout.cmd tmpout stde
        exit(-5)
    end
    # add output path
    push!(outPaths, tmpout)

    # Generate the plots if requested
    if plotstats
        # set the output paths for the plots
        plotsprefix = joinpath(outdir, "samtools_plots")
        # Create the directory for plots
        systools.makedir(plotsprefix)
        plotsprefix = joinpath(plotsprefix, "$(outprefix)")
        ### plot-bamstats ###
        #Example: plot-bamstats -p </path/to/outdir/prefix_out_name> <samtools.stats.out>
        stdo = stde = ""
        progname = "plot-bamstats"
        tmpCmd = `$(progname) -p $(plotsprefix) $(tmpout)`
        stdo = joinpath(dirname(plotsprefix), "stdout.$(progname).$(outprefix).txt")
        stde = joinpath(dirname(plotsprefix), "stderr.$(progname).$(outprefix).txt")
        @debug "Running $(progname) on $(basename(tmpout))..." tmpCmd stdo stde
        rout = run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    end
    
    # Make sue nothing went wrong
    if rout.exitcode != 0
        @error "Something went wrong while running $(progname)." rout.exitcode rout.cmd stdo stde
        exit(-5)
    end
    # Add path to plots directory
    push!(outPaths, dirname(plotsprefix))

    return outPaths
end



"""Compute coverage for BAM alignment using samtools coverage."""
function samtools_coverage(inpath::String, outdir::String, refpath::String=""; outprefix::String="samtools_coverage")::String
    @debug "samtools_coverage :: START" inpath outdir refpath outprefix
    # create the output directory if required
    systools.makedir(outdir)
    # Set to true if the reference is missing
    if !isfile(refpath)
        withref = false
        @error "A valid path to the reference FASTA file was not provided." refpath
    end

    # Output format
    fmt::String = progname = outpath = stde = ""
    fmt = lowercase(rsplit(inpath, ".", limit=2)[2])
    # Change the output prefix if needed
    if length(outprefix) == 0
        outprefix = rsplit(basename(inpath), ".", limit=2)[1]
    end
    # make sure the output file is eaither in BAM or SAM
    if fmt ∉ ["bam", "sam"]
        @error "The input file must be a sorted BAM/SAM alignment file. Please add the BAM extension to the output file." fmt inpath
        exit(-4)
    end

    ### samtools coverage ###
    # Example: samtools coverage --reference <reference.fasta> -o <output_coverage.tsv> <input.bam>
    progname = "coverage"
    outpath = joinpath(outdir, "samtools.$(progname).$(outprefix).tsv")
    tmpCmd::Cmd = `samtools $(progname) --reference $(refpath) -o $(outpath) $(inpath)`
    stde = joinpath(outdir, "stderr.samtools.$(progname).$(outprefix).txt")
    @debug "Computing coverage for $(basename(inpath))..." tmpCmd outpath stde
    rout::Base.Process = run(pipeline(tmpCmd, stderr=stde), wait=true)

    # Make sue nothing went wrong
    if rout.exitcode != 0
        @error "Something went wrong while running samtools $(progname)." rout.exitcode rout.cmd tmpout stde
        exit(-5)
    end

    return outpath
end



"""Compute coverage for BAM alignment using samtools coverage."""
function samtools_coverage_windowed(inpath::String, outdir::String, chromotbl::String, refpath::String=""; window::Int=10000, outprefix::String="samtools_coverage_windowed")::String
    @debug "samtools_coverage_windowed :: START" inpath outdir refpath chromotbl window outprefix
    # The input table must have 2 columns
    # Sequence name (e.g., chromosome)
    # Sequence lenght (e.g. genome or chromosome size)
    # window is the size of the window defining the length of each interval
    # create the output directory if required
    systools.makedir(outdir)
    # Set to true if the reference is missing
    if !isfile(refpath)
        withref = false
        @error "A valid path to the reference FASTA file was not provided." refpath
    end
    
    # Output format
    fmt::String = progname = outpath = stde = ""
    fmt = lowercase(rsplit(inpath, ".", limit=2)[2])
    # Change the output prefix if needed
    if length(outprefix) == 0
        outprefix = rsplit(basename(inpath), ".", limit=2)[1]
    end
    # make sure the output file is eaither in BAM or SAM
    if fmt ∉ ["bam", "sam"]
        @error "The input file must be a sorted BAM/SAM alignment file. Please add the BAM extension to the output file." fmt inpath
        exit(-4)
    end

    # create the genomic regions
    genomicIntervals = create_genomic_intervals(chromotbl, window=window)
    # generate a temp directory to store coverage files
    workDir::String = mktempdir(outdir, prefix="samtools_coverage_per_region_$(window)_", cleanup=true)
    # Example: samtools coverage  -H -r "Chromosome_name:1-10000" <input.bam> -o <out.coverage.tsv>
    progname = "coverage"
    outpath = joinpath(outdir, "samtools.$(progname).$(window).$(outprefix).tsv")
    # Create the output file and write the header
    ofd::IOStream = open(outpath, "w")
    write(ofd, "rname\tstartpos\tendpos\tnumreads\tcovbases\tcoverage\tmeandepth\tmeanbaseq\tmeanmapq\n")
    close(ofd)

    # tmp variables
    tmpOutPath::String =  tmpChrom = ""
    tmpCmd::Cmd = `echo hello`
    # The coverage will not be computed for each interval
    for gint in genomicIntervals
        if occursin("|", gint.seq)
            tmpChrom = rsplit(gint.seq, "|", limit=2)[end]
        else
            tmpChrom = gint.seq
        end
        tmpOutPath = joinpath(workDir, "tmp.$(tmpChrom).$(gint.startpos)-$(gint.endpos).tsv")
        tmpCmd = `samtools $(progname) --reference $(refpath) -H -r $(gint.seq):$(gint.startpos)-$(gint.endpos) $(inpath)`
        ### samtools coverage ###
        stde = joinpath(workDir, "stderr.$(basename(tmpOutPath)).txt")
        @debug "Computing coverage for $(basename(inpath))..." tmpCmd outpath stde
        rout::Base.Process = run(pipeline(tmpCmd, stderr=stde, stdout=outpath, append=true), wait=true)
        # rout::Base.Process = run(pipeline(tmpCmd, stderr=stde), wait=true)
        
        # Make sue nothing went wrong
        if rout.exitcode != 0
            @error "Something went wrong while running samtools $(progname)." rout.exitcode rout.cmd tmpOutPath stde
            exit(-5)
        end
        # break
    end

    return outpath
end





"""Index a sorted BAM alignment file."""
@inline function samtools_index(inpath::String; threads::Int=4)::Nothing
    @debug "samtools_index :: START" inpath threads

    # Output format
    fmt::String = outprefix = outdir = ""
    fmt = lowercase(rsplit(inpath, ".", limit=2)[2])
    outprefix = rsplit(basename(inpath), ".", limit=2)[1]
    outdir::String = dirname(inpath)
    # make sure the output file is eaither in BAM or SAM
    if fmt != "bam"
        @error "The input file must be in BAM format. Please add the BAM extension to the input file." inpath fmt
        exit(-4)
    end

    # Perfom the indexing
    # Example of samtools index
    # samtools index -@ 32 <input.bam>
    tmpCmd = `samtools index -@ $(threads) $(inpath)`
    stdo = joinpath(outdir, "stdout.samtools.index.$(outprefix).txt")
    stde = joinpath(outdir, "stderr.samtools.index.$(outprefix).txt")
    @debug "Indexing $(basename(inpath)) file..." tmpCmd stdo stde
    rout = run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    # Make sue nothing when wrong
    if rout.exitcode != 0
        @error "Something went wrong while indexing the input BAM. Maybe it is not sorted or sorted by name." rout.exitcode rout.cmd stdo stde
        exit(-5)
    end

    return nothing
end



"""Sort an input SAM or BAM alignment file using samtools."""
function samtools_sort(inpath::String, outpath::String; byname::Bool=false, indexbam::Bool=false, threads::Int=4)::Nothing
    @debug "samtools_sort :: START" inpath outpath byname indexbam threads
    # create the output directory if required
    outdir::String = dirname(outpath)
    systools.makedir(outdir)
    # Output format
    fmt::String = outprefix = ""
    fmt = lowercase(rsplit(outpath, ".", limit=2)[2])
    outprefix = rsplit(basename(outpath), ".", limit=2)[1]
    # make sure the output file is eaither in BAM or SAM
    if fmt ∉ ["bam", "sam"]
        @error "The output file must be in either BAM or SAM format. Please add the appropriate extension to the output path." outpath fmt
        exit(-4)
    end

    # Example of samtools sort
    # samtools sort [-n] -@ 32 -O <outfmt> -o <output.bam> <output_prefix>
    extraOptions::Array{String, 1} = String[]
    if byname
        push!(extraOptions, "-n")
    end
    tmpCmd::Cmd = `samtools sort $(extraOptions) -@ $(threads) -o $(outpath) $(inpath)`
    stdo::String = stde = ""
    stdo = joinpath(outdir, "stdout.samtools.sort.$(outprefix).txt")
    stde = joinpath(outdir, "stderr.samtools.sort.$(outprefix).txt")
    @debug "Sorting $(uppercase(fmt)) file..." tmpCmd stdo stde
    rout::Base.Process = run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)

    # Make sue nothing when wrong
    if rout.exitcode != 0
        @error "Something went wrong while sorting the input alignment file." rout.exitcode rout.cmd stdo stde
        exit(-5)
    end

    # Index the BAM file if required
    if indexbam
        # Skip if SAM
        if fmt == "sam"
            @warn "Samtools indexing can only be used with BAM alignment files." outpath
        elseif byname
            @warn "Samtools indexing can only be used with BAM files sorted by coordinates." outpath byname
        else
            # Perfom the indexing
            # Example of samtools index
            # samtools index -@ 32 <input.bam>
            tmpCmd = `samtools index -@ $(threads) $(outpath)`
            stdo = joinpath(outdir, "stdout.samtools.index.$(outprefix).txt")
            stde = joinpath(outdir, "stderr.samtools.index.$(outprefix).txt")
            @debug "Indexing $(basename(outpath)) file..." tmpCmd stdo stde
            rout = run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
            # Make sue nothing when wrong
            if rout.exitcode != 0
                @error "Something went wrong while indexing the input alignment file." rout.exitcode rout.cmd stdo stde
                exit(-5)
            end
        end
    end

    return nothing
end



end # module end
