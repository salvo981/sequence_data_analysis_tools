"""Utility methods for sequence annotation."""

module seqAnnotation

include("./systools.jl")
using Logging: @warn, @debug, @info
# using Distributed
# using DataStructures
using Printf: @printf, @sprintf



struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("Sequence annotation", "seqAnnotation.jl", "Salvatore Cosentino", "GPLv3", 0.1, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end



"""Convert raw kofamscan output to valid KeggDecoder input."""
function convert_kofamout2keggdecoder(ksraw::String, outpath::String; ksformat::String="mapper", prefix::String="", appendMode::Bool=false)::Nothing
    @debug "convert_kofamout2keggdecoder :: START" ksraw outpath ksformat appendMode
    
    validFmts::Array{String, 1} = String["mapper", "detail-tsv"]
    tmpFlds::Array{String, 1} = String[]
    tmpProtId::String = tmpKeggId = ""

    if ksformat ∉ validFmts
        @error "Not accepted kofam_scan output format" ksformat
        exit(-5)
    end

    # open the output file
    ofd::IOStream = open(outpath, "a")
    if !appendMode
        close(ofd)
        ofd = open(outpath, "w")
    end

    ifd::IOStream = open(ksraw, "r")
    if ksformat == "mapper"
        for ln in eachline(ksraw)
            tmpFlds = split(ln, "\t", limit=2)
            if length(tmpFlds) == 2
                if length(prefix) > 0
                    write(ofd, "$(prefix)_$(tmpFlds[1])\t$(tmpFlds[2])\n")
                else
                    write(ofd, "$(tmpFlds[1])\t$(tmpFlds[2])\n")
                end
            end
        end
    elseif ksformat == "detail-tsv"
        # Lines in the kofam scan output would look as follows
        # #	gene name	KO	thrshld	score	E-value	"KO definition"
        # #	---------	------	-------	------	---------	-------------
        # bin.1.k141_197600_length_650081_cov_25.0002_1	K02358	358.47	87.8	2e-24	"elongation factor Tu"
        # *	bin.1.k141_197600_length_650081_cov_25.0002_2	K02946	64.17	146.0	4.4e-42	"small subunit ribosomal protein S10"

        # skip the header lines
        readline(ifd, keep=true)
        readline(ifd, keep=true)
        # process the hits
        for ln in eachline(ksraw)
            if ln[1] == '*'
                tmpProtId, tmpKeggId = split(ln, "\t", limit=4)[2:3]
                # write the prefix if required
                if length(prefix) > 0
                    write(ofd, "$(prefix)_$(tmpProtId)\t$(tmpKeggId)\n")
                else
                    write(ofd, "$(tmpProtId)\t$(tmpKeggId)\n")
                end
            end
        end
    end
    close(ifd)
    close(ofd)

    return nothing
end





""" Annotate input proteins with KEGG information using KofamScan.
    ftp://ftp.genome.jp/pub/tools/kofam_scan/
"""
function kofamscan_annotate(inSeq::String, outdir::String; profiles::String, koList::String, prefix::String="kofamscan", fmt::String="detail", outLog::Bool=false, threads::Int=threads)::String
    @debug "kofamscan_annotate :: START" inSeq outdir profiles koList prefix fmt threads
    # create the output directory if required
    systools.makedir(outdir)
    # valid formats
    validFmts::Array{String, 1} = ["detail", "detail-tsv", "mapper", "mapper-oneline"]
    if fmt ∉ validFmts
        @warn "Invalid output format" fmt
        println("The output format will be set to the default one: 'detail'")
        fmt = "detail"
    end

    # check that profiles and KO list file are valid
    if !isfile(profiles)
        @error "The file or dir with HMM profiles is not valid" profiles
        exit(-2)
    end
    if !isfile(koList)
        @error "The file the list of KO IDs is not valid" koList
        exit(-2)
    end

    # set output paths    
    outPath::String = joinpath(outdir, "$(prefix).txt")
    # set to tsv format if not 'detail'
    if fmt != "detail"
        outPath = joinpath(outdir, "$(prefix).tsv")
    end

    # kofamscan example
    # exec_annotation <input.faa> -o <outfile.tsv> --format mapper --cpu <threads> -k <path/to/ko_list> -p <path/to/profiles/prokaryote.hal>
    tmpCmd::Cmd = `exec_annotation $(inSeq) -o $(outPath) --format $(fmt) --cpu $(threads) -k $(koList) -p $(profiles)`
    @debug "KofamScan CMD" tmpCmd
    stdo::String = stde = ""
    stdo = joinpath(outdir, "stdout.kofamscan.$(basename(outPath))")
    stde = joinpath(outdir, "stderr.kofamscan.$(basename(outPath))")
    if outLog
        run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    else
        run(pipeline(tmpCmd), wait=true)
    end
    # Return the path to the file annotations
    return outPath 
end



"""Predict proteins from input DNA sequences using Prodigal.
"""
function prodigal(inSeq::String, outdir::String; prefix::String="prodigal_prediction", mode::String="single", outLog::Bool=false, quiet::Bool=false)::String
    @debug "prodigal :: START" inSeq outdir prefix mode outLog quiet
    # create the output directory if required
    systools.makedir(outdir)
    # set output paths
    protPath::String = joinpath(outdir, "$(prefix).faa")
    outPath::String = joinpath(outdir, "$(prefix).gbk")
    options::Array{String, 1} = ["-p", mode]
    if quiet
        push!(options, "-q")
    end

    # prodigal example
    # prodigal -i <input.fna> -a <predicted_proteins.faa> -p single -o <main_output.gbk> -q
    tmpCmd::Cmd = `prodigal -i $(inSeq) -a $(protPath) -o $(outPath) $options`
    stdo::String = stde = ""
    stdo = joinpath(outdir, "stdout.prodigal.$(prefix).txt")
    stde = joinpath(outdir, "stderr.prodigal.$(prefix).txt")
    run(pipeline(tmpCmd, stdout=stdo, stderr=stde), wait=true)
    # Return the path to the file with proteins
    return joinpath(protPath) 
end



end # module end
