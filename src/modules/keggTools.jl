"""Utility methods for retrving infomation from KEGG"""

module keggTools

include("./systools.jl")
using Logging: @warn, @debug, @info
# using Distributed
using DataStructures: OrderedDict, Accumulator, counter, inc!
using Printf: @printf, @sprintf
using HTTP: get
using JSON: json, parsefile
# using StatsBase: mean
# using CodecZlib: GzipDecompressorStream



struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("KEGG tools", "keggTools.jl", "Salvatore Cosentino", "GPLv3", 0.1, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end



"""Given an output file from KofamScan count hits on metabolic pathways."""
function count_pathways(kofamfile::String, keggDir::String, jsonDir::String; ovrwrtKegg::Bool=false, ovrwrtJson::Bool=false)::Nothing
    @debug "count_pathways :: START" kofamfile keggDir jsonDir ovrwrtKegg ovrwrtJson

    if !isfile(kofamfile)
        @error "The file with kofam_scan results does not exist!" kofamfile
    end

    # Tmp variables
    flds::Array{String, 1} = Array{String, 1}()
    tmpInfo::Dict{String, Any} = Dict{String, Any}()
    tmpStr::String = currentK = currentDef = kjson = kraw = ""
    classCnt::Accumulator{String, Int} = counter(String)
    lnCnt::Int = uniqKegg = 0

    # Create the directories if required
    systools.makedir(keggDir)
    systools.makedir(jsonDir)

    # example of kofam scan output
    # #	gene name	KO	thrshld	score	E-value	"KO definition"
    # #	---------	------	-------	------	---------	-------------
    # *	k141_18581_length_39535_cov_69.9637_1	K06217	229.90	457.3	1.4e-137	"phosphate starvation-inducible protein PhoH and related proteins"
    # k141_18581_length_39535_cov_69.9637_1	K07175	256.87	192.1	3.4e-57	"PhoH-like ATPase"

    # Skip the header lines and parse only best hits
    for ln in eachline(kofamfile)
        if ln[1] == '*'
            flds = split(ln, "\t", limit=7)
            currentK = flds[3]
            currentDef = flds[7][2:end-1]
            println("$(currentK)\t$(currentDef)")
            # Download the KEGG entry if required
            kraw = joinpath(keggDir, "$(currentK).txt")
            if !isfile(kraw) || ovrwrtKegg
                kegg_rest_get(currentK, "ko", keggDir, overwrite=true)
            end
            # Convert the raw KEGG file to JSON if required
            kjson = joinpath(jsonDir, "$(currentK).json")
            if !isfile(kjson) || ovrwrtJson
                kegg_entry2json(kraw, kjson)
                uniqKegg += 1
            end

            # load the json file and perfom the count
            tmpInfo = parsefile(kjson)
            # increment the counter
            for c1 in keys(tmpInfo["brite"])
                inc!(classCnt, c1)
            end
        end
    end

    @show classCnt

    @info "Execution for input file $(basename(kofamfile))" kofamfile uniqKegg
    return nothing
end



"""Download KEGG entries in raw text and covert to JSON."""
function create_kegg_db(koList::String, keggDir::String, jsonDir::String; ovrwrtKegg::Bool=false, ovrwrtJson::Bool=false, waitTime::Float64=0.0)::Nothing
    @debug "create_kegg_db :: START" koList keggDir jsonDir ovrwrtKegg ovrwrtJson waitTime

    if !isfile(koList)
        @error "The file kofam_scan list of entries is missing!" koList
    end

    # Tmp variables
    # flds::Array{String, 1} = Array{String, 1}()
    # tmpInfo::Dict{String, Any} = Dict{String, Any}()
    tmpStr::String = currentK = kjson = kraw = ""
    lnCnt::Int = keggCnt = invalidCnt = jsonCnt = 0

    # Create the directories if required
    systools.makedir(keggDir)
    systools.makedir(jsonDir)

    # example of entries in the ko_list file from kofam koala
    # knum	threshold	score_type	profile_type	F-measure	nseq	nseq_used	alen	mlen	eff_nseq	re/pos	definition
    # K00001	383.33	domain	trim	0.321379	1543	1092	1368	398	12.48	0.590	alcohol dehydrogenase [EC:1.1.1.1]
    # The Kegg number is in the first column

    # Skip the header lines and parse only best hits
    for ln in eachline(koList)
        if ln[1] == 'k' # skip header
            continue
        else
            lnCnt += 1
            currentK = split(ln, "\t", limit=2)[1]
            # println("$(currentK)")
            if (length(currentK) != 6) || (currentK[1] != 'K')
                @warn "Not valid K number" currentK
                invalidCnt += 1
                continue
            end
            # Download the KEGG entry if required
            kraw = joinpath(keggDir, "$(currentK).txt")
            if !isfile(kraw) || ovrwrtKegg
                kegg_rest_get(string(currentK), "ko", keggDir, overwrite=true)
                keggCnt += 1
                if wait != 0
                    sleep(waitTime)
                end
            end
            # Convert the raw KEGG file to JSON if required
            kjson = joinpath(jsonDir, "$(currentK).json")
            if !isfile(kjson) || ovrwrtJson
                kegg_entry2json(kraw, kjson)
                jsonCnt += 1
            end
        end
    end

    @info "DB creation summary for $(basename(koList))" koList lnCnt invalidCnt keggCnt jsonCnt
    return nothing
end



"""Given a text file with a KEGG entry, extract and store the information into a JSON file.
"""
function kegg_entry2json(keggfile::String, outpath::String)::Nothing
    @debug "kegg_entry2json :: START" keggfile outpath

    if !isfile(keggfile)
        @error "The KEGG file does not exist!" keggfile
    end

    # tmp variables
    flds::Array{String, 1} = Array{String, 1}()
    briteLines::Array{String, 1} = Array{String, 1}()
    briteStopList::Array{String, 1} = ["Ba", "Ch", "DN", "En", "Pr", "Ri", "Se", "Tr"]
    push!(briteStopList, "Ce") # Skip "Cell adhesion molecules" and similar patterns
    push!(briteStopList, "Cy") # Skip "Cytokines" and similar patterns
    push!(briteStopList, "Ex") # Skip "Exosome" and similar patterns
    push!(briteStopList, "G ") # Skip "G protein-coupled" and similar patterns
    push!(briteStopList, "Gl") # Skip "Glycosaminoglycan" and similar patterns
    push!(briteStopList, "GT") # Skip "GTP-binding proteins" and similar patterns
    push!(briteStopList, "Li") # Skip "Lipopolysaccharide" and similar patterns
    push!(briteStopList, "Me") # Skip "Membrane trafficking" and similar patterns
    push!(briteStopList, "Mi") # Skip "Mitochondrial" and similar patterns
    push!(briteStopList, "No") # Skip "Non-coding RNAs" and similar patterns
    push!(briteStopList, "Pe") # Skip "Peptidases" and similar patterns
    push!(briteStopList, "Ph") # Skip "Photosynthesis proteins" and similar patterns
    push!(briteStopList, "Po") # Skip "Polyketide" and similar patterns
    push!(briteStopList, "Pr") # Skip "Proteasome proteins" and similar patterns
    push!(briteStopList, "Tw") # Skip "Two-components" and similar patterns
    push!(briteStopList, "Ub") # Skip "Ubiquitin proteins" and similar patterns
    push!(briteStopList, "Vi") # Skip "Viral proteins" and similar patterns

    tmpStr::String = currentFld = ec = ""

    # The dictionary should be organized as follows
    # OrderedDict{String,Any} with 5 entries:
    # "entry"      => "K00500"
    # "name"       => " the name"
    # "definition" => "The definition"
    # "pathway"    => OrderedDict("ko00360"=>"Phenylalanine metabolism","ko00120"=>"banana split")
    # "brite"      => OrderedDict{String,Any}("1"=>"Metabolism","2"=>OrderedDict("Amino acid metabolism"=>["leaf 1", "leaf 2"],"Metabolism of cofactors and vitamins"=>["Folate biosynthesis"]))
    
    # Where 'brite' is dictionary of lists,
    # And each list contains the final KEGG classes
    # For example:
    # kegg2json["brite"]["2"]
    # OrderedDict{String,Array{String,1}} with 2 entries:
    #   "Amino acid metabolism"                => ["leaf 1", "leaf 2"]
    #   "Metabolism of cofactors and vitamins" => ["Folate biosynthesis"]

    # Initalize the dictionary that will be converted to JSON
    kegg2json::OrderedDict{String, Any} = OrderedDict{String, Any}()
    # Add the main keys to the dictionary
    kegg2json["entry"] = ""
    kegg2json["name"] = ""
    kegg2json["definition"] = ""
    kegg2json["ec"] = ""
    kegg2json["pathway"] = OrderedDict{String, String}()
    kegg2json["disease"] = OrderedDict{String, String}()
    # kegg2json["brite"] = OrderedDict{String, Any}("1"=>"", "2"=>OrderedDict{String, Array{String, 1}}())
    kegg2json["brite"] = OrderedDict{String, OrderedDict{String, Array{String, 1}}}()

    # KEGG files have the following structure
    # ENTRY       K02874                      KO
    # NAME        RP-L14, MRPL14, rplN
    # DEFINITION  large subunit ribosomal protein L14
    # PATHWAY     ko03010  Ribosome
    # DISEASE     H00167  Phenylketonuria
    # BRITE       KEGG Orthology (KO) [BR:ko00001]
    #              09120 Genetic Information Processing
    #               09122 Translation
    #                03010 Ribosome
    #                 K02874  RP-L14, MRPL14, rplN; large subunit ribosomal protein L14
    #              09180 Brite Hierarchies
    #               09182 Protein families: genetic information processing
    #                03011 Ribosome
    #                 K02874  RP-L14, MRPL14, rplN; large subunit ribosomal protein L14
    #             Ribosome [BR:ko03011]
    #              Ribosomal proteins
    #               Mitochondria/ Chloroplast
    #                Large subunit
    #                 K02874  RP-L14, MRPL14, rplN; large subunit ribosomal protein L14
    #               Bacteria
    #                Large subunit
    #                 K02874  RP-L14, MRPL14, rplN; large subunit ribosomal protein L14
    #               Archaea
    #                Large subunit
    #                 K02874  RP-L14, MRPL14, rplN; large subunit ribosomal protein L14
    # DBLINKS     COG: COG0093
    #             GO: 0005762 0022625
    # GENES       HSA: 64928(MRPL14)
    #             PTR: 462726(MRPL14)
    #             PPS: 100977802(MRPL14)
    #             GGO: 101138826(MRPL14)
    # REFERENCE   PMID:22829778
    #   AUTHORS   Hauser R, Pech M, Kijek J, Yamamoto H, Titz B, Naeve F, Tovchigrechko A, Yamamoto K, Szaflarski W, Takeuchi N, Stellberger T, Diefenbacher ME, Nierhaus KH, Uetz P
    #   TITLE     RsfA (YbeB) proteins are conserved ribosomal silencing factors.
    #   JOURNAL   PLoS Genet 8:e1002815 (2012)
    #           DOI:10.1371/journal.pgen.1002815
    #   SEQUENCE  [hsa:64928] [eco:b3310]
    # ///

    # Start parsing the KEGG file
    for ln in eachline(keggfile)
        # extract Entry
        if ln[1] == 'E'
            currentFld = "entry"
            flds = split(ln, "       ", limit=3)
            kegg2json["entry"] = flds[2]
        # extract name
        elseif ln[1] == 'N'
            currentFld = "name"
            flds = split(ln, "        ", limit=2)
            kegg2json["name"] = flds[2]
        # extract definition
        elseif ln[1:2] == "DE"
            currentFld = "definition"
            flds = split(ln, "  ", limit=2)
            tmpStr = flds[2]
            # Extract the EC if present
            if tmpStr[end] == ']'
                # example: [EC:1.14.16.1]
                tmpStr, ec = rsplit(tmpStr, "[EC:", limit=2)
                tmpStr = rstrip(tmpStr)
                # ec = ec[1:end-1]
                kegg2json["ec"] = ec[1:end-1]
            end
            # set the definition
            kegg2json["definition"] = tmpStr
        # extract disease
        elseif ln[1:2] == "DI"
            currentFld = "disease"
            # DISEASE     H00167  Phenylketonuria
            flds = split(ln, "     ", limit=2)
            tmpStr = flds[2]
            # disease id and description
            flds = split(tmpStr, "  ", limit=2)
            # fill the dictionary
            kegg2json["disease"][flds[1]] = flds[2]
        # extract pathways
        elseif ln[1] == 'P'
            currentFld = "pathway"
            # extract the right part
            tmpStr = split(ln, "     ", limit=2)[2]
            # KO ID and pathway
            flds = split(tmpStr, "  ", limit=2)
            # fill the dictionary
            kegg2json["pathway"][flds[1]] = flds[2]
        # Catch BRITE first line
        elseif ln[1] == 'B'
            currentFld = "brite"
            push!(briteLines, ln)
        # handle field with multiple entries (e.g. pathway)
        elseif ln[1] == ' '
            if currentFld == "pathway"
                # KO ID and pathway
                flds = split(strip(ln, ' '), "  ", limit=2)
                # fill the dictionary
                kegg2json["pathway"][flds[1]] = flds[2]
            elseif currentFld == "brite"
                # Skip lines in which we have no interest (e.g. Enzymes, Ribosome)
                tmpStr = lstrip(ln, ' ')
                # if "PATHWAY" is missing then there is no Metabolism to extract
                if length(kegg2json["pathway"]) == 0
                    currentFld = "stop"
                # Skip enzymes, Ribosome, DNA Replication protein
                elseif tmpStr[1:2] ∈ briteStopList
                    currentFld = "stop"
                    break
                else
                    # store BRITE related line
                    push!(briteLines, ln)
                end
            end
        # any other filed would stop the reading
        elseif (ln[1:2] == "DB") || (ln[1:2] == "GE") || (ln[1] == 'R')
            break
        end
    end

    # Parse BRITE lines
    if length(briteLines) > 0
        # extract BRITE information
        kegg2json["brite"] = parse_brite_lines(briteLines)
    else
        # set the values to unclassified
        # no need to modifiy the filed "2" as it already an empty dict
        # kegg2json["brite"]["1"] = "Unclassified"
        kegg2json["brite"]["Unclassified"] = OrderedDict{String, Array{String, 1}}()
    end

    # dump into a JSON file
    ofd::IOStream = open(outpath, "w")
    write(ofd, json(kegg2json, 1))
    close(ofd)
    return nothing
end



"""Given a KEGG ID, retrieve the information using the REST API.
    https://www.kegg.jp/kegg/rest/keggapi.html
"""
function kegg_rest_get(keggid::String, trgtdb::String, outdir::String; overwrite::Bool=false)::Tuple{Int16, String}
    @debug "kegg_rest_get :: START" keggid trgtdb outdir overwrite

    # Set the valid DBs
    validDBs::Array{String, 1} = ["pathway", "brite", "module", "ko", "genome", "vg", "ag", "compound", "glycan", "reaction", "rclass", "enzyme", "network", "variant", "disease", "drug", "dgroup", "environ", "atc", "jtc", "ndc", "yj", "pubmed"]

    # make sure the target DB is valid
    if trgtdb ∉ validDBs
        @error "There is no target DB called $(trgtdb)"
        exit(-5)
    end

    # set the prefix for the get requist
    getPrefix::String = ""
    if trgtdb == "ko"
        getPrefix = "ko:"
    elseif trgtdb == "pathway"
        getPrefix = "path:"
    elseif trgtdb == "brite"
        getPrefix = "br:"
    end

    if getPrefix == ""
        @error "The prefix for the get REST request was not set" getPrefix
        exit(-7)
    end

    # requests use the 'get' function obtain the list of IDS
    # Example request: http://rest.kegg.jp/get/path:ko00360
    # create the output directory if required
    systools.makedir(outdir)
    # prepare the output file
    outPath::String = joinpath(outdir, "$(keggid).txt")

    if !overwrite
        if isfile(outPath)
            @warn "The file $(outPath) already exists, and will not be downloaded." outPath
            return (200, outPath)
        end
    end

    status::Int16 = 200
    # Perform the HTPP request
    url::String = "http://rest.kegg.jp/get/$(getPrefix)$(keggid)"
    try
        response = get(url)
        # ofd::IOStream = open(outPath, "w")
        write(outPath, String(response.body))
        status = response.status
    catch e
        status = response.status
        return (status, e)
    end

    return (status, outPath)

end



"""Given a KEGG ID, retrieve the related IDs in the target DB.
   This function performs REST through the KEGG API.
    https://www.kegg.jp/kegg/rest/keggapi.html
"""
function kegg_rest_link(srcid::String, trgtdb::String, outdir::String; overwrite::Bool=false)::Tuple{Int16, String}
    @debug "kegg_rest_link :: START" srcid trgtdb outdir overwrite

    # Set the valid DBs
    validDBs::Array{String, 1} = ["pathway", "brite", "module", "ko", "genome", "vg", "ag", "compound", "glycan", "reaction", "rclass", "enzyme", "network", "variant", "disease", "drug", "dgroup", "environ", "atc", "jtc", "ndc", "yj", "pubmed"]

    # make sure the target DB is valid
    if trgtdb ∉ validDBs
        @error "There is no target DB called $(trgtdb)"
        exit(-5)
    end

    # requests use the 'link' function obtain the list of IDS
    # Example request: http://rest.kegg.jp/link/brite/K00500
    # Request output: "ko:K00500\tbr:ko00001\nko:K00500\tbr:ko01000\n"
    # create the output directory if required
    systools.makedir(outdir)
    # prepare the output file
    outPath::String = joinpath(outdir, "$(srcid).$(trgtdb).txt")

    if !overwrite
        if isfile(outPath)
            @warn "The file $(outPath) already exists, and will not be downloaded." outPath
            return (200, outPath)
        end
    end

    status::Int16 = 200
    # Perform the HTPP request
    url::String = "http://rest.kegg.jp/link/$(trgtdb)/$(srcid)"
    try
        response = get(url)
        # ofd::IOStream = open(outPath, "w")
        write(outPath, String(response.body))
        status = response.status
    catch e
        status = response.status
        return (status, e)
    end

    return (status, outPath)

end



"""Exctract information from raw kofam_scan output and output a file formatted to use with KEGGDecoder."""
function kofamout2keggdecoder(inpath::String, outpath::String; smplSeparator::String="")::Nothing
    @debug "kofamout2keggdecoder :: START" inpath outpath
    # smplSepartor is a string that will be replaced with a single underscore '_'
    # Anything on the left of the first underscore is considered a sample/genome name
    # by KEGG decoder

    # More information about KEGG-decoder can be found at
    # https://github.com/bjtully/BioData/tree/master/KEGGDecoder

    # Example of kofam_scan output
    # # gene name                                        KO     thrshld  score   E-value KO definition
    # #------------------------------------------------- ------ ------- ------ --------- ---------------------
    # * bin.1.k141_18581_length_39535_cov_69.9637_1      K06217  229.90  457.3  2.6e-136 phosphate starvation-inducible protein PhoH and related proteins
    # bin.1.k141_18581_length_39535_cov_69.9637_1      K07175  256.87  192.1   6.4e-56 PhoH-like ATPase
    # bin.1.k141_18581_length_39535_cov_69.9637_1      K03581  244.17   38.5     2e-09 exodeoxyribonuclease V alpha subunit [EC:3.1.11.5]

    # temp variables
    flds::Array{String, 1} = Array{String, 1}()
    tmpStr::String = tmpid = ""
    queries::Dict{String, Int} = Dict()
    keggids::Dict{String, Int} = Dict()
    hcnt::Int = lcnt = 0
    # create the output dir if required
    systools.makedir(dirname(outpath))
    ofd::IOStream = open(outpath, "w")
    for ln in eachline(inpath)
        if ln[1] == '#'
            continue
        end
        # process the line
        lcnt += 1
        flds = split(ln, "\t", limit=4)
        tmpStr = flds[2]
        tmpid = flds[3]
        # println(flds)
        # Insert the gene into the dictionary
        if !haskey(queries, tmpStr)
            queries[tmpStr] = 0
        end
        # proces the best hit
        if flds[1] == "*"
            hcnt += 1
            # Insert KEGG id into the dictionary
            if !haskey(keggids, tmpid)
                keggids[tmpid] = 0
            end
            # Add the underscore to separate genome identifier
            if length(smplSeparator) > 0
                tmpStr = replace(tmpStr, smplSeparator => "_", count=1)
            end
            # write gene and kegg id into the output file
            write(ofd, "$(tmpStr)\t$(tmpid)\n")
        end
        # break
    end
    close(ofd)

    @debug "Kofam extraction summary" lcnt hcnt length(queries) length(keggids)
    # Start reading and parsing the input file
    return nothing
end



"""Parse lines containing BRITE levels."""
function parse_brite_lines(lines::Array{String, 1})::OrderedDict{String, OrderedDict{String, Array{String, 1}}}
    @debug "parse_brite_lines :: START" length(lines)

    # Dictionary that will store levels 2 and 3
    # levDict::OrderedDict{String, Array{String, 1}} = OrderedDict{String, Array{String, 1}}()
    levDict::OrderedDict{String, OrderedDict{String, Array{String, 1}}} = OrderedDict{String, OrderedDict{String, Array{String, 1}}}()

    # set the string of white spaces used
    # to identify the BRITE level
    lev4start::String = "                "
    lev3start::String = "               "
    lev2start::String = "              "
    lev1start::String = "             "
    tmpStr::String = l1 = tmpLev2 = ""
    # start parsing the line
    for ln in lines
        if ln[1] == 'B'
            continue
        # handle the case based on the number of starting spaces
        else
            @show ln
            # level 4, example: K00500  phhA, PAH; phenylalanine-4-hydroxylase
            if startswith(ln, lev4start)
                # skip
                continue
            # level 3, example: 00360 Phenylalanine metabolism
            elseif startswith(ln, lev3start)
                ln = lstrip(ln, ' ')
                tmpStr = split(ln, " ", limit=2)[2]
                # add the entry to the Array
                push!(levDict[l1][tmpLev2], tmpStr)
            # level 2, example: 09105 Amino acid metabolism
            elseif startswith(ln, lev2start)
                ln = lstrip(ln, ' ')
                tmpLev2 = split(ln, " ", limit=2)[2]
                # Add the entry to the Dict and initialize the list
                levDict[l1][tmpLev2] = Array{String, 1}()
            # level 1, example: 09100 Metabolism
            elseif startswith(ln, lev1start)
                ln = lstrip(ln, ' ')
                l1 = split(ln, " ", limit=2)[2]
                # initialize the dictionary
                levDict[l1] = OrderedDict{String, Array{String, 1}}()
            end
        end
    end

    # return (l1, levDict)
    return levDict
    # return OrderedDict{String, OrderedDict{String, Array{String, 1}}}()
end


end # module end
