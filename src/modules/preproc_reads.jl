"""
Methods for preprocessing raw reads.
"""

module preproc_reads
using Logging: @warn, @debug, @info
using Printf: @printf, @sprintf
include("./systools.jl")

struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("Preprocess reads", "preproc_reads.jl", "Salvatore Cosentino", "GPLv3", 0.1, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end



"""Use Fastp to filter and assess reads quality"""
function clean_fastp(r1::String, r2::String="", outDir::String=""; outPrefix::String="fastp_trimmed", avgQual::Int=28, threads::Int=4, repTitle::String="fastp trimming report", outHtml::Bool=true)::Tuple{String, String}
    @debug "clean_fastp :: START" r1 r2 outDir outPrefix avgQual threads repTitle outHtml

    # fastp can be found at
    # https://github.com/OpenGene/fastp

    o1::String = o2 = reportsDir = htmlPath = ""
    # create the directory with reports
    reportsDir = joinpath(outDir, "fastp_reports")
    systools.makedir(reportsDir)
    # Path to json report
    jsonPath = joinpath(reportsDir, "$(outPrefix).trimming.report.fastp.json")
    logPath::String = joinpath(outDir, "$(outPrefix).fastp.log.txt")
    # set the HTML report path if required
    if outHtml
        htmlPath = joinpath(reportsDir, "$(outPrefix).trimming.report.fastp.html")
    end
    # check the the input exists
    if !isfile(r1)
        println("ERROR: the file $(r1) does not exist!")
        exit(-2)
    end
    # Set the run as paired or not
    isPaired::Bool = true
    if r2 == ""
        isPaired = false
    end

    if isPaired
        if !isfile(r2)
            println("ERROR: the file $(r2) does not exist!")
            exit(-2)
        end
    end

    # example of xommand for paired-end read-sets
    # fastp -i r1.fastq -I r2.fastq -o o1.trim.fastq -O o2.trim.fastq --detect_adapter_for_pe --thread 8 --report_title my_report --html ./test_trimming_pe_report_avgqual28.html --average_qual 28

    # prepare the command
    tmpCmd::Cmd = ``
    if isPaired
        o1 = joinpath(outDir, "$(outPrefix)_r1.clean.fastq")
        o2 = joinpath(outDir, "$(outPrefix)_r2.clean.fastq")
        if outHtml
            tmpCmd = `fastp -i $(r1) -I $(r2) -o $(o1) -O $(o2) --thread $(threads) --report_title $(repTitle) --html $(htmlPath) --json $(jsonPath) --average_qual $(avgQual) --detect_adapter_for_pe`
        else
            tmpCmd = `fastp -i $(r1) -I $(r2) -o $(o1) -O $(o2) --thread $(threads) --report_title $(repTitle) --json $(jsonPath) --average_qual $(avgQual) --detect_adapter_for_pe`
        end
    else # SE run
        o1 = joinpath(outDir, "$(outPrefix).clean.fastq")
        if outHtml
            tmpCmd = `fastp -i $(r1) -o $(o1) --thread $(threads) --report_title $(repTitle) --html $(htmlPath) --json $(jsonPath) --average_qual $(avgQual)`
        else
            tmpCmd = `fastp -i $(r1) -o $(o1) --thread $(threads) --report_title $(repTitle) --json $(jsonPath) --average_qual $(avgQual)`
        end
    end

    lfd::IOStream = open(logPath, "w")
    procOut::Base.Process = run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went wrong during the the read preprocessing, check the log file\n$(logPath)")
    end
    close(lfd)
    # return the paths to the clean reads
    return (o1, o2)
end



""" Store files generated from fastp into the desired directory."""
function store_fastp_output(workDir::String, outDir::String, smplName::String; compress::Bool=true, threads::Int=4)::Nothing
    @debug "store_fastp_output :: START" workDir outDir smplName compress threads
    
    tmpOutPath::String = tmpInPath = ""
    # Make sure input and output directories differ
    if workDir == outDir
        @error "Input and output directories must be different." workDir outDir
        exit(-6)
    end

    # For each file in the work directory, if it starts
    # with the sample name than it should be kept
    for f in readdir(workDir)
        if startswith(f, "$(smplName)")
            tmpInPath = joinpath(workDir, f)
            # kofamscan file
            if occursin(r".clean.fastq", f)
                tmpOutPath = joinpath(outDir, f)
            # log file
            elseif endswith(f, r".fastp.log.txt")
                tmpOutPath = joinpath(outDir, f)
            else
                continue
            end
            # Move or compress the file
            if compress
                systools.pigz_compress(tmpInPath, tmpOutPath, level=9, keep=true, force=true, useGzip=true, errLog=false, threads=threads)
            else
                mv(tmpInPath, tmpOutPath, force=true)
            end
        elseif f == "fastp_reports"
            # Move the report files into the output directory
            for rep in readdir(joinpath(workDir, f))
                tmpInPath = joinpath(workDir, "$(f)/$(rep)")
                tmpOutPath = joinpath(outDir, rep)
                mv(tmpInPath, tmpOutPath, force=true)
            end
        end
    end

    return nothing
end



end # module end
