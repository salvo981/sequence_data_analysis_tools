"""Methods to interface with MMseqs2"""

module mmseqs_tools

using Distributed
using DataStructures
using DataFrames
using CSV
using Printf: @printf, @sprintf
using Base


struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("MMseqs2 tools", "mmseqs_tools.jl", "Salvatore Cosentino", "GPLv3", 0.2, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end


"""
Create MMseqs2 profiles database from MSA file from CATh FunFams
"""
function create_funfam_profiles(inSto::String, outDir::String, threads::Int=4, matchRatio::Float64=0.8, debug::Bool=false)::String
    if debug
        println("\ncreate_funfam_profiles :: START")
        @show inSto
        @show outDir
        @show threads
        @show matchRatio
    end

    # Now prepare the commands and create the profiles
    tmpDir::String = joinpath(outDir, "mmseqs_tmp_dir")
    # create the directory if required
    makedir(tmpDir)
    tmpDbPath::String = "$(inSto).mmseqs_msa"
    logPath::String = joinpath(outDir, "log.create_funfam_profiles.txt")
    lfd::IOStream = open(logPath, "w")

    ### Convert the MSA ###
    write(lfd, @sprintf("Stockholm MSA conversion\n"))

    tmpCmd::Cmd = `mmseqs convertmsa  $(inSto) $(tmpDbPath) --compressed 0 -v 3`
    write(lfd, @sprintf("%s\n\n", join(tmpCmd.exec, " ")))
    close(lfd)

    # reopen in append mode
    lfd = open(logPath, "a")
    procOut::Base.Process = run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went wrong during the conversion of the MSA, check the log file\n$(logPath)")
    end

    ### Create profiles ###
    profileDbPath::String = "$(inSto).profile_db_$(replace(string(matchRatio), "."=>""))"
    write(lfd, @sprintf("\nProfiles creation:\n"))
    tmpCmd = `mmseqs msa2profile $(tmpDbPath) $(profileDbPath) --match-mode 1 --match-ratio $(matchRatio) --threads $(threads) -v 3`
    write(lfd, @sprintf("%s\n\n", join(tmpCmd.exec, " ")))
    close(lfd)

    # reopen in append mode
    lfd = open(logPath, "a")
    procOut = run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went wrong during the creation of the MMseqs profiles, check the log file\n$(logPath)")
    end

    ### Index profile DB ###
    # NOTE: the -k value increase the index size, while increasing the sensitivity with (-s)
    # will have a smaller impact on the index size
    write(lfd, @sprintf("\nIndex profiles DB:\n"))
    tmpCmd = `mmseqs createindex $(profileDbPath) $(tmpDir) -k 5 -s 7 --threads $(threads) -v 3`
    write(lfd, @sprintf("%s\n\n", join(tmpCmd.exec, " ")))
    close(lfd)

    # reopen in append mode
    lfd = open(logPath, "a")
    procOut = run(pipeline(tmpCmd, stdout=lfd, stderr=lfd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went wrong during the indexing of the MMseqs profiles, check the log file\n$(logPath)")
    end
    close(lfd)

    return profileDbPath
end



function count_proteins(fpath::String)::Tuple{String,Int}
    cnt::Int = 0
    #@show fpath
    if !isfile(fpath)
        write(Base.stderr, "\nInput FASTA not found!")
    else
        open(fpath, "r") do ifd
            for ln in eachline(ifd)
                if ln[1] == '>'
                    cnt += 1
                end
            end
        end
    end
    #@show cnt
    # return the count
    (fpath, cnt)
end



"""Count sequences in parallel."""
function parallel_count_proteins(fPaths::Array{String, 1}, threads::Int=4, debug::Bool=false)::OrderedDict{String, Int}
    if debug
        println("\nparallel_count_proteins :: START")
        @show Sys.CPU_THREADS
        @show length(fPaths)
        @show threads
        @show workers()
    end

    # will contain the Future for each path
    fs = Dict{Int,Future}()
    @sync for (i, fastaPath) in enumerate(fPaths)
        @async fs[i] = @spawn count_proteins(fastaPath)
    end

    res = Dict{Int,Tuple{String, Int}}()
    @sync for fut in fs
        @async res[fut[1]] = fetch(fut[2])
    end

    # fill the output dictionary keep the input sort of the input array of paths
    # will contain only the basename and the number of sequences
    sortedCounts = OrderedDict{String, Int}()
    for (i, fastaPath) in enumerate(fPaths)
        tpl = res[i]
        sortedCounts[basename(tpl[1])] = tpl[2]
    end

    if debug
        @show length(sortedCounts)
        @show sortedCounts
    end

    sortedCounts
end



"""Count sequences in parallel."""
function parallel_count_proteins_pmap(fPaths::Array{String, 1}, outJdl::String, threads::Int=4, debug::Bool=false)::OrderedDict{String, Int}
    if debug
        println("\nparallel_count_proteins_pmap :: START")
        @show Sys.CPU_THREADS
        @show length(fPaths)
        @show outJdl
        @show threads
        @show workers()
    end

    # create a pool of Workers
    wp = WorkerPool(workers())
    # execute the computation
    c = pmap(count_proteins, wp, fPaths, distributed=true)

    @show typeof(c)
    @show c

    # fill the output dictionary keep the input sort of the input array of paths
    # will contain only the basename and the number of sequences
    sortedCounts = OrderedDict{String, Int}()
    for (i, tpl) in enumerate(c)
        sortedCounts[basename(tpl[1])] = tpl[2]
    end

    if debug
        @show length(sortedCounts)
        @show sortedCounts
    end
    # Store the dictionary in a JLD2 file
    FileIO.save(outJdl, sortedCounts)

    sortedCounts
end



"""Crete MMseqs DBs in parallel."""
function parallel_create_db(fPaths::Array{String, 1}, outDir::String, createIdx::Bool=true, threads::Int=4, debug::Bool=false)#::Dict{String, Int}
    if debug
        println("\nparallel_create_db :: START")
        @show length(fPaths)
        @show outDir
        @show createIdx
        @show threads
        @show workers()
    end

    # will contain the Future for each path
    fs = Dict{Int,Future}()
    @sync for (i, fastaPath) in enumerate(fPaths)
        @show nprocs()
        @show nworkers()
        # Output path
        dbPath::String = joinpath(outDir, basename(fastaPath))
        @async fs[i] = @spawn create_db(fastaPath, dbPath, createIdx, threads, false)
    end

end



"""Crete MMseqs DBs in parallel (pmap)."""
function parallel_create_db_pmap(fPaths::Array{String, 1}, outDir::String, createIdx::Bool=true, threads::Int=4, debug::Bool=false)::Array{String, 1}
    if debug
        println("\nparallel_create_db_pmap :: START")
        @show length(fPaths)
        @show outDir
        @show createIdx
        @show threads
    end

    # create a pool of Workers
    wp = WorkerPool(workers())

    # create the arrays with the parameters for each run
    dbPathsParams::Array{String, 1} = []
    makeIdxParams::Array{Bool, 1} = []
    threadsParams::Array{Int, 1} = []
    debugParams::Array{Bool, 1} = []

    for p in fPaths
        push!(dbPathsParams, joinpath(outDir, basename(p)))
        push!(makeIdxParams, createIdx)
        push!(threadsParams, 1)
        push!(debugParams, false)
    end

    # perform the computations
    extime = @elapsed c = pmap((a1,a2,a3,a4,a5)->create_db(a1,a2,a3,a4,a5), wp, fPaths, dbPathsParams, makeIdxParams, threadsParams, debugParams, distributed=true)
    # NOTE: the parameters could have been passed as a collection (Array) of tuples
    # pmap((args)->f(args...),{{"a",2},{"b",1},{"c",3}})
    @show typeof(extime)
    @show extime

    # return paths to the created MMseqs databases
    dbPathsParams
end



"""Search profiles in parallel."""
function parallel_profile_search(qDbPaths::Array{String, 1}, profileDB::String, outDir::String, threads::Int=4, debug::Bool=false)::Array{Tuple{String, Float64, Float64, String},1}
    if debug
        println("\nparallel_profile_search :: START")
        @show length(qDbPaths)
        @show profileDB
        @show outDir
        @show threads
    end

    # create a pool of Workers
    wp = WorkerPool(workers())

    # create the arrays with the parameters for each run
    profilePathParams::Array{String, 1} = []
    outDirParams::Array{String, 1} = []
    threadsParams::Array{Int, 1} = []
    debugParams::Array{Bool, 1} = []

    for db in qDbPaths
        push!(profilePathParams, profileDB)
        push!(outDirParams, outDir)
        push!(threadsParams, 17)
        push!(debugParams, false)
    end

    # perform the computations and store the total execution time in extime
    c = pmap((a1,a2,a3,a4,a5)->profile_search(a1,a2,a3,a4,a5), wp, qDbPaths, profilePathParams, outDirParams, threadsParams, debugParams, distributed=true)
    # NOTE: the parameters could have been passed as a collection (Array) of tuples
    # pmap((args)->f(args...),{{"a",2},{"b",1},{"c",3}})

    for res in c
        @show typeof(res)
        @show res
    end
    @show typeof(c)
    # return the results
    return c
end



"""Search profiles in parallel."""
function parallel_profile_search_and_stats(qDbPaths::Array{String, 1}, profileDB::String, outDir::String, seqLenJld::String, resFileName::String, threads::Int=4, debug::Bool=false)
    if debug
        println("\nparallel_profile_search_and_stats :: START")
        @show length(qDbPaths)
        @show profileDB
        @show outDir
        @show resFileName
        @show seqLenJld
        @show threads
    end

    # fill the output dictionary keep the input sort of the input array of paths
    # will contain only the basename and the number of sequences
    seqCntDict = OrderedDict{String, Int}()
    for tpl in load(seqLenJld)
        seqCntDict[tpl[1]] = tpl[2]
    end

    @show typeof(seqCntDict)

    # create a pool of Workers
    wp = WorkerPool(workers())

    # create the arrays with the parameters for each run
    profilePathParams::Array{String, 1} = []
    outDirParams::Array{String, 1} = []
    seqLenParams::Array{Int, 1} = []
    outFdParams::Array{IOStream, 1} = []
    threadsParams::Array{Int, 1} = []
    debugParams::Array{Bool, 1} = []

    # set the path and open the ooutput file
    resFilePath::String = joinpath(outDir, resFileName)
    @show resFilePath
    ofd::IOStream = open(resFilePath, "w")

    for db in qDbPaths
        push!(profilePathParams, profileDB)
        push!(outDirParams, outDir)
        push!(seqLenParams, seqCntDict[basename(db)])
        push!(outFdParams, ofd)
        push!(threadsParams, 1)
        push!(debugParams, debug)
    end
    # perform the computations and store the total execution time in extime
    c = pmap((a1,a2,a3,a4,a5,a6)->profile_search_and_stats(a1,a2,a3,a4,a5,a6), wp, qDbPaths, profilePathParams, outDirParams, seqLenParams, threadsParams, debugParams, distributed=true)
    # NOTE: the parameters could have been passed as a collection (Array) of tuples
    # pmap((args)->f(args...),{{"a",2},{"b",1},{"c",3}})

    for res in c
        write(ofd, "$(join(res, "\t"))\n")
    end
    @show typeof(c)
    close(ofd)

end



"""Create MMseqs database"""
function  create_db(inFasta::String, outPath::String, createIdx::Bool=true, threads::Int=4, debug::Bool=false)
    if debug
        println("\ncreate_db :: START")
        @show inFasta
        @show outPath
        @show createIdx
        @show threads
    end
    # prepare the system call to create the DB
    tmpCmd::Cmd = `mmseqs createdb $(inFasta) $(outPath)`
    outDbDir::String = dirname(outPath)
    runLog::String = joinpath(outDbDir, "log.create_db.$(basename(inFasta)).txt")
    ofd = open(runLog, "w")
    # execute the command
    run(pipeline(tmpCmd, stdin=ofd, stdout=ofd), wait=true)
    write(ofd, "\n#####\nDONE => mmseqs createdb\n#####\n")
    close(ofd)

    # create the index if required
    if createIdx
        # reopen in append mode
        ofd = open(runLog, "a")
        write(ofd, "\n")
        tmpDbDir::String = joinpath(outDbDir, "tmp_$(basename(inFasta))")
        tmpCmd = `mmseqs createindex $(outPath) $(tmpDbDir) --threads $(threads) -v 3`
        run(pipeline(tmpCmd, stdin=ofd, stdout=ofd), wait=true)
        write(ofd, "\n#####\nDONE => mmseqs createindex\n#####\n")
        # close the log file
        close(ofd)
    end

    @show outPath

end



"""Extract simple stats about the profile search."""
function extract_search_stats(resPath::String, inSeqCnt::Int, debug::Bool=false)::Tuple{Int, Int, Int, Float64}
    if debug
        println("extract_search_stats :: START")
        @show resPath
        @show inSeqCnt
    end

    # load the dataframe
    df::DataFrame = CSV.read(resPath, copycols=true, header=false)
    # sort by first columns
    totHits::Int = size(df)[1]
    sort!(df, (:Column1, :Column2, :Column3), rev=(false, false, true))
    # obtain index for duplicates
    dupIdx::Array{Int, 1} = []
    dupCnt::Int = 0
    # create array of row indexes with duplicates
    println("Counting duplicates...")
    for (i, isDup) in enumerate(nonunique(df, [:Column1, :Column2]))
        if isDup # if it is a duplicate
            dupCnt += 1
            push!(dupIdx, i)
        end
    end

    # remove multiple hits for same query-profile hits
    unique!(df, [:Column1, :Column2])
    uniqHits::Int = size(df)[1]
    # count number of queries with at least one hit
    qWithHits::Int = length(unique(df[:Column1]))
    # percentage of input query with at least one hit
    qHitPct::Float64 = round((qWithHits / inSeqCnt) * 100., digits=2)

    if debug
        @show totHits
        @show dupCnt
        @show uniqHits
        @show qWithHits
        @show qHitPct
    end
    # return the stats
    (totHits, uniqHits, qWithHits, qHitPct)
end



"""Perform search on profiles using MMseqs2"""
function profile_search(queryDB::String, profileDB::String, outDir::String, threads::Int=4, debug::Bool=false)::Tuple{String, Float64, Float64, String}
    if debug
        println("profile_search :: START")
        @show queryDB
        @show profileDB
        @show outDir
        @show threads
    end

    # prepare the system call perform the search
    runName::String = "$(basename(queryDB))_$(basename(profileDB))"
    #outName::String = "$(basename(queryDB))_profiles"
    tmpDir::String = joinpath(outDir, "tmp_$(runName)")
    rawout::String = joinpath(outDir, "$(runName).raw")
    #tmpCmd::Cmd = `mmseqs search $(query) $(target) $(rawout) $(tmpDir) --threads $(threads) -s $(sensitivity) --alt-ali $(altAlign) -v 3`
    # sorted output
    tmpCmd::Cmd = `mmseqs search $(queryDB) $(profileDB) $(rawout) $(tmpDir) --threads $(threads) -k 6 -s 7.5 --alt-ali 10 -v 0 --sort-results 1 --db-load-mode 2`

    # NOTE!
    # sorting only sorts based on query ID and not on the target

    # filter the output by alignments score and/or coverage

    # output a DB instead of a text file, and filter it in advance
    # keep only first N hits

    runLog::String = joinpath(outDir, "$(runName)_prof_search_log.txt")
    ofd = open(runLog, "w")
    # execute the command
    extimeSearch::Float64 = @elapsed run(pipeline(tmpCmd, ofd), wait=true)
    write(ofd, "\n#####\nDONE => mmseqs search\n#####\n")
    close(ofd)
    
    # convert to BLAST output
    blastout::String = joinpath(outDir, "$(runName).blast")
    tmpCmd = `mmseqs convertalis $(queryDB) $(profileDB) $(rawout) $(blastout) --threads $(threads) --format-mode 2 -v 0`
    #tmpCmd = `mmseqs convertalis $(queryDB) $(profileDB) $(rawout) $(blastout) --threads $(threads) --format-output 'query,target,pident,alnlen,mismatch,gapopen,qstart,qend,tstart,tend,bits,qlen,tlen,qcov,tcov' -v 0`
    
    ofd = open(runLog, "a")
    write(ofd, "\n")
    # execute the command
    extimeConv::Float64 = @elapsed  run(pipeline(tmpCmd, ofd), wait=true)
    totexTime::Float64 = extimeConv + extimeSearch
    write(ofd, "\n#####\nDONE => mmseqs convertalis\n#####\n")
    close(ofd)
    
    # write some info on the executions times
    @show extimeSearch
    @show extimeConv
    @show totexTime
    # return input proteome name, execution times, and path to blast file
    (runName, extimeSearch, extimeConv, blastout)
end



"""Perform search with stats"""
function profile_search_and_stats(queryDB::String, profileDB::String, outDir::String, inSeqCnt::Int, threads::Int=4, debug::Bool=false)::Tuple{String, Float64, Float64, Float64, Int, Int, Int, Int, Float64}
    if debug
        println("\nprofile_search_and_stats :: START")
    end
    # first perform the profile search
    searchTpl::Tuple{String, Float64, Float64, String} = profile_search(queryDB, profileDB, outDir, threads, debug)
    # extract some variables
    blastPath::String = searchTpl[4]
    runName::String = searchTpl[1]
    extimeSearch::Float64 = searchTpl[2]
    extimeConv::Float64 = searchTpl[3]
    extimeTot::Float64 = extimeSearch + extimeConv
    # now extract the stats
    statsTpl::Tuple{Int, Int, Int, Float64} = extract_search_stats(blastPath, inSeqCnt, debug)
    # prepare the final output containing the following
    # run name, execution time (search, convertalis, and total),
    # input proteins, unique hits, query with hits, %query with hits, total hits
    # (totHits, uniqHits, qWithHits, qHitPct)

    if debug
        @show runName
        @show extimeSearch
        @show extimeConv
        @show extimeTot
        @show inSeqCnt
        @show statsTpl[2]
        @show statsTpl[1]
        @show statsTpl[3]
        @show statsTpl[4]
    end

    # return the tuple with the required information
    (runName, extimeSearch, extimeConv, extimeTot, inSeqCnt, statsTpl[1], statsTpl[2], statsTpl[3], statsTpl[4])
end



"""Perform batch search with mmseqs and convert with convertalis"""
function search(query::String, target::String, outDir::String, sensitivity::Float64=6.0, altAlign::Int=10, threads::Int=4, debug::Bool=false)
    if debug
        println("search :: START")
        @show query
        @show target
        @show outDir
        @show sensitivity
        @show altAlign
        @show threads
    end

    # prepare the system call to create the DB
    pairName::String = "$(basename(query))-$(basename(target))"
    tmpDir::String = joinpath(outDir, "tmp_$(pairName)")
    rawout::String = joinpath(outDir, "$(pairName).raw")
    #tmpCmd::Cmd = `mmseqs search $(query) $(target) $(rawout) $(tmpDir) --threads $(threads) -s $(sensitivity) --alt-ali $(altAlign) -v 3`
    # sorted output
    tmpCmd::Cmd = `mmseqs search $(query) $(target) $(rawout) $(tmpDir) --threads $(threads) -s $(sensitivity) --alt-ali $(altAlign) -v 3 --sort-results 1`

    # NOTE!
    # sorting only sorts based on query ID and not on the target

    # filter the output by alignments score and/or coverage

    # output a DB instead of a text file, and filter it in advance
    # keep only first N hits


    runLog::String = joinpath(outDir, "$(pairName)_search_log.txt")
    ofd = open(runLog, "w")
    # execute the command
    run(pipeline(tmpCmd, ofd), wait=true)
    write(ofd, "\n#####\nDONE => mmseqs search\n#####\n")
    close(ofd)

    # convert to BLAST output
    blastout::String = joinpath(outDir, "$(pairName).blast_sorted")
    #tmpCmd = `mmseqs convertalis $(query) $(target) $(rawout) $(blastout) --threads $(threads) --format-mode 2 -v 0`

    tmpCmd = `mmseqs convertalis $(query) $(target) $(rawout) $(blastout) --threads $(threads) --format-output 'query,target,pident,alnlen,mismatch,gapopen,qstart,qend,tstart,tend,bits,qlen,tlen,qcov,tcov' -v 0`

    ofd = open(runLog, "a")
    write(ofd, "\n")
    # execute the command
    run(pipeline(tmpCmd, ofd), wait=true)
    write(ofd, "\n#####\nDONE => mmseqs convertalis\n#####\n")
    close(ofd)
end



function makedir(dpath::String, debug::Bool=false)
    try
        mkdir(dpath, mode=0o751)
    catch e
        if !isdir(dirname(outDir))
            println("\nINFO: intermediate directories are missing and will be created.")
            mkpath(dpath, mode=0o751)
        else
            if debug
                println("\nThe directory $(dpath)\nalready exists.")
            end
        end
    end
end


end # module end
