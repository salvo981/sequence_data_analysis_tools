'''
This module contains different functions to use NCBI Entrez tools.
It allows to search entrez and download genomes and sequences
'''
import os
import sys
from Bio import Entrez


__module_name__ = 'Entrez Tools'
__source__ = 'entrez_tools.py'
__author__ = 'Salvatore Cosentino'
#__copyright__ = ''
__license__ = 'GPL'
__version__ = '0.3'
__maintainer__ = 'Cosentino Salvatore'
__email__ = 'salvo981@gmail.com'



_EMAIL = None
_DBLIST = None

def info():
    '''
    This module contains different functions to use NCBI Entrez tools.
    It allows to search entrez and download genomes and sequences
    '''
    print('MODULE NAME:\t%s'%__module_name__)
    print('SOURCE FILE NAME:\t%s'%__source__)
    print('MODULE VERSION:\t%s'%__version__)
    print('LICENSE:\t%s'%__license__)
    print('AUTHOR:\t%s'%__author__)
    print('_EMAIL:\t%s'%__email__)



def dwnld_gbk(gi, dataBase, outDir=os.getcwd(), outName=None, outLog=False, debug=False):
    '''
    Downloads a genbank file from NCBI.
    input: a valid genbank id and a valid database name
    '''
    if debug:
        print('GI:\t%s'%str(gi))
        print('DB:\t%s'%str(dataBase))
        print('OUTPUT DIR:\t%s'%str(outDir))
        print('OUTPUT NAME:\t%s'%str(outName))
        print('OUTPUT LOG FILE:\t%s'%str(outLog))
    if outDir != os.getcwd():
        import sys_tools as systools
        if outDir == None:
            outDir = os.getcwd()
        systools.makedir(outDir)
    gi = gi.strip().lower()
    #set the output name
    if outName == None:
        outName = '%s.gbk'%gi
    outPath = '%s%s'%(outDir, outName)
    #lets write the log file if requested
    logFile = lfd = None
    if outLog:
        logFile = '%s.log'%outName.replace('.gbk', '')
        logFile = '%s%s'%(outDir, logFile)
        if debug:
            print('LOG FILE PATH:\t%s'%logFile)
        lfd = open(logFile, 'w')
        lfd.write('GI:\t%s'%gi)
        lfd.write('\nDB:\t%s'%dataBase)
        lfd.write('\nOUTPUT DIR:\t%s'%outDir)
        lfd.write('\nOUTPUT NAME:\t%s'%outName)
        lfd.write('\nOUTPUT LOG FILE:\t%s'%outLog)
        lfd.write('\nLOG FILE PATH:\t%s'%logFile)
    #let's now retrieve the file
    Entrez.email = _EMAIL
    handle = Entrez.efetch(db=dataBase, id=gi, rettype="gbwithparts", retmode="text")
    record = handle.read()
    handle.close()
    #generate the output file
    ofd = open(outPath, 'w')
    ofd.write(record)
    ofd.close()
    #check that the file has been downloaded properly
    size = -2
    try:
        size = os.stat(outPath).st_size
    except OSError:
        sys.stderr.write('\nWARNING: the file %s was probably not downoaded...'%outPath)
        size = -1
    #close the log file if needed
    outMsg = None
    if lfd:
        if size == -1:
            outMsg = '\nWARNING: the file %s was probably not downloaded...'%outPath
            lfd.write(outMsg)
        elif size == 0:
            outMsg = '\nWARNING: the file size for %s is 0, there is maybe no gbk for %s'%(os.path.basename(outPath), gi)
            lfd.write(outMsg)
        elif size == -2:
            outMsg = '\nERROR: something went wrong downloading  the gbk for %s'%(gi)
            lfd.write(outMsg)
            sys.exit(-5)
        else:
            outMsg = '\nFILE SIZE:\t%.2fKb\n'%(float(size)/1000.)
            lfd.write(outMsg)
        lfd.close()
    return(outPath, size, outMsg)



def dwnld_seq_by_gi(gi, dataBase, return_type='gbwithparts', outDir=os.getcwd(), outName=None, outLog=False, debug=False):
    '''
    Downloads a fasta or genbank file from NCBI.
    input: a valid genbank id and a valid database name
    return_type: the output type, which could genbank or fasta sequences
    '''
    if debug:
        print('GI:\t%s'%str(gi))
        print('DB:\t%s'%str(dataBase))
        print('RETTYPE:\t%s'%str(return_type))
        print('OUTPUT DIR:\t%s'%str(outDir))
        print('OUTPUT NAME:\t%s'%str(outName))
        print('OUTPUT LOG FILE:\t%s'%str(outLog))
    if outDir != os.getcwd():
        import sys_tools as systools
        if outDir == None:
            outDir = os.getcwd()
        systools.makedir(outDir)
    gi = gi.strip().lower()
    #valid return types
    validRettypeList = ['fasta', 'fasta_cds_aa', 'fasta_cds_na', 'gb', 'gbwithparts']
    return_type = return_type.lower().strip()
    #set the output format
    outFmt = None
    if return_type.startswith('gb'):
        outFmt = 'gbk'
    elif return_type == 'fasta':
        outFmt = 'fa'
    elif return_type.endswith('_na'): #fasta nucleotides
        outFmt = 'fna'
    elif return_type.endswith('_aa'): #fasta proteins
        outFmt = 'faa'
    if not return_type in validRettypeList:
        sys.stderr.write('\nERROR: return type %s is not valid, please use one of the following:\n%s'%(return_type, ', '.join(validRettypeList)))
        sys.exit(-5)
    #set the output name
    if outName == None:
        outName = '%s.%s'%(gi, outFmt)
    outPath = '%s%s'%(outDir, outName)
    #lets write the log file if requested
    logFile = lfd = None
    if outLog:
        if outName == None:
            logFile = '%s.log'%outName.replace('.%s'%outFmt, '')
        else:
            logFile = '%s.log'%outName
        logFile = '%s%s'%(outDir, logFile)
        if debug:
            print('LOG FILE PATH:\t%s'%logFile)
        lfd = open(logFile, 'w')
        lfd.write('GI:\t%s'%gi)
        lfd.write('\nDB:\t%s'%dataBase)
        lfd.write('\nRETTYPE:\t%s'%return_type)
        lfd.write('\nOUTPUT DIR:\t%s'%outDir)
        lfd.write('\nOUTPUT NAME:\t%s'%outName)
        lfd.write('\nOUTPUT LOG FILE:\t%s'%outLog)
        lfd.write('\nLOG FILE PATH:\t%s'%logFile)
    #let's now retrieve the file
    Entrez.email = _EMAIL
    handle = Entrez.efetch(db=dataBase, id=gi, rettype=return_type, retmode="text")
    record = handle.read()
    handle.close()
    #generate the output file
    ofd = open(outPath, 'w')
    ofd.write(record)
    ofd.close()
    #check that the file has been downloaded properly
    size = -2
    try:
        size = os.stat(outPath).st_size
    except OSError:
        sys.stderr.write('\nWARNING: the file %s was probably not downoaded...'%outPath)
        size = -1
    #close the log file if needed
    outMsg = None
    if lfd:
        if size == -1:
            outMsg = '\nWARNING: the file %s was probably not downloaded...'%outPath
            lfd.write(outMsg)
        elif size == 0:
            outMsg = '\nWARNING: the file size for %s is 0, there is maybe no sequence/genbank for %s'%(os.path.basename(outPath), gi)
            lfd.write(outMsg)
        elif size == -2:
            outMsg = '\nERROR: something went wrong downloading the sequence/genbank for %s'%(gi)
            lfd.write(outMsg)
            sys.exit(-5)
        else:
            outMsg = '\nFILE SIZE:\t%.2fKb\n'%(float(size)/1000.)
            lfd.write(outMsg)
        lfd.close()
    return(outPath, size, outMsg)



def get_db_list(debug=False):
    '''Will return a list with the available databases.'''
    global _DBLIST
    if _EMAIL == None:
        sys.stderr.write('\nERROR: you must run the init function providing a valid email address')
        sys.exit(-6)
    dbs = None
    if _DBLIST == None:
        Entrez.email = _EMAIL
        handle = Entrez.einfo()
        dbs = Entrez.read(handle)
        handle.close()
        dbs = dbs['DbList']
        _DBLIST = dbs
    else:
        dbs = _DBLIST
    if debug:
        print('\nAvailable DBs:\n%s'%str(dbs))
    return dbs



def get_db_info(dataBase=None, debug=False):
    '''
    Retrieves a dictionary with info about a given Entrez database.
    input: a valid database name
    '''
    if debug:
        print('DB:\t%s'%str(dataBase))
    if _EMAIL == None:
        sys.stderr.write('\nERROR: you must run the init function providing a valid email address')
        sys.exit(-6)
    if _DBLIST == None:
        get_db_list(debug)
    #check that the requested database is in the list
    dataBase = dataBase.strip().lower()
    if not dataBase in _DBLIST:
        sys.stderr.write('\nWARNING: %s is not a valid database'%(dataBase))
        sys.stderr.write('\nPlease use one of the following valid databases\n%s'%(str(_DBLIST)))
        return None
    #otherwise retrieve the info
    Entrez.email = _EMAIL
    handle = Entrez.einfo(db=dataBase)
    dbInfo = Entrez.read(handle)
    handle.close()
    return dbInfo



def get_email():
    '''Returns the email address.'''
    return _EMAIL



def get_org_gis(accession, debug=False):
    '''
    This function will perform a search in NCBI nucleotide database.
    accession: a valid ncbi accession
    returns a list with the retrieved Genbank Ids (GIs)
    '''
    if debug:
        print('ACCESSION:\t%s'%str(accession))
    db = 'nucleotide'
    accession = accession.strip().lower()
    resDict = search(db, accession, debug)
    outList = resDict['IdList']
    if debug:
        print('Entries found for \'%s\':\n%s'%(accession, str(outList)))
    return outList



def init(email=None, debug=False):
    '''Will set the email address and database list.'''
    global _EMAIL
    global _DBLIST
    if debug:
        print('EMAIL:\t%s'%str(email))
    _EMAIL = email.strip().lower()
    if _DBLIST == None:
        _DBLIST = get_db_list(False)



def search(dataBase=None, searchTerm=False, debug=False):
    '''
    This function will perform a search in NCBI using Entrez
    dataBase: name of the database to search
    searchTerm: a string to search in the database
    returns a dictionary with the search result
    '''
    if debug:
        print('DB:\t%s'%str(dataBase))
        print('QUERY:\t%s'%str(searchTerm))
    if _EMAIL == None:
        sys.stderr.write('\nERROR: you must run the init function providing a valid email address')
        sys.exit(-6)
    if _DBLIST == None:
        get_db_list(debug)
    Entrez.email = _EMAIL
    handle = Entrez.esearch(db=dataBase, term=searchTerm)
    result = Entrez.read(handle)
    handle.close()
    return result



def test_init(debug=True):
    '''Test the setting of email and databases name.'''
    email = 'salvocos@gen-info.osaka-u.ac.jp'
    init(email, debug)



def test_dwnld_gbk(debug=True):
    '''Tests the download of genbank files.'''
    gid = '537637238'
    db = 'nucleotide'
    outTestDir = '/user/gen-info/salvocos/tmp/entrez_test/gbk/'
    #automatic naming
    dwnld_gbk(gid, db, outTestDir, None, True, debug)



def test_dwnld_seq_by_gi(debug=True):
    '''Tests the download of genbank files.'''
    gid = '15640032'
    db = 'nucleotide'
    outTestDir = '/user/gen-info/salvocos/tmp/entrez_test/'
    #automatic gbk
    #rtype = 'fasta'
    #rtype = 'gbwithparts'
    #rtype = 'fasta_cds_na'
    rtype = 'fasta_cds_aa'
    #tpl = dwnld_seq_by_gi(gid, db, rtype, outTestDir, outName=None, outLog=True, debug=debug)
    #custom output name
    tpl = dwnld_seq_by_gi(gid, db, rtype, outTestDir, outName='minchia_test', outLog=True, debug=debug)
    #if debug:
        #print(tpl)



def test_get_db_info(debug=True):
    '''Test the function to get info for a db.'''
    infoDict = get_db_info('nuccore', debug = debug) #valid db
    #print(infoDict)
    if debug:
        for key, val in infoDict.items():
            print('%s\n%s'%(key, val))



def test_get_org_gis(debug=True):
    '''Test the function retrieve GIs about a given organism.'''
    acc = 'PRJNA32795'
    giList = get_org_gis(acc, debug)
    if debug:
        print('%d GIs for \'%s\':'%(len(giList), acc))
        for gi in giList:
            print('%s'%(gi))



def test_search(debug=True):
    '''Test the function executing esearch.'''
    db = 'nucleotide'
    query = 'PRJNA32795'
    searchDict = search(db, query, debug)
    if debug:
        for key, val in searchDict.items():
            print('%s\n%s'%(key, val))
    #search pubmed
    #db = 'pubmed'
    #query = 'human'
    #searchDict = search(db, query, debug)
    #if debug:
        #for key, val in searchDict.items():
            #print('%s\n%s'%(key, val))
