'''Module that contain different file conversion steps.'''

import os
from Bio import SeqIO
from Bio.SeqIO import FastaIO
from . import sys_tools as systools

# functions
def fastq_to_fasta(path, debug=False):
    """Convert a fastq file in fasta."""
    #check if the input file exists
    if not os.path.isfile(path):
        sys.stderr.write('\nERROR: {:s} is not a valid file.'.format(path))
        sys.exit(-2)
    # create output path
    outPath = path.replace('.fastq', '.fa')
    # convert the file using Biopython
    with open(path, "rU") as input_handle, open(outPath, "w") as output_handle:
        sequences = SeqIO.parse(input_handle, "fastq")
        # to write single-line sequences
        fasta_out = FastaIO.FastaWriter(output_handle, wrap=None)
        fasta_out.write_file(sequences)
    #if debug:
        #print('{:d} FASTA records written in {:s}.'.format(count, outPath))
