'''
This module is used to trimm adapters and perform the quality filtering on raw reads,
It supports bbduk, trimmomatic and cutadapt.
'''
import sys
import os
import subprocess
from collections import OrderedDict

#variables
__module_name__ = 'Reads Cleaner'
__source__ = 'reads_cleaner.py'
__author__ = 'Salvatore Cosentino'
#__copyright__ = ''
__license__ = 'GPL'
__version__ = '0.8'
__maintainer__ = 'Cosentino Salvatore'
__email__ = 'salvo981@gmail.com'


#main directories for the program to run
#root path for files
srcDir = os.path.dirname(os.path.abspath(__file__))
srcDir += '/'
resource_root = '%sadapters/'%srcDir #directory in which the adapters are stored

adaptersDict = OrderedDict()
adaptersDict['NexteraPE-PE'] = '%sNexteraPE-PE.fa'%resource_root
adaptersDict['TruSeq3-PE'] = '%sTruSeq3-PE.fa'%resource_root
#adaptersDict['TruSeq3-SE'] = '%sTruSeq3-SE.fa'%resource_root
adaptersDict['TruSeq3-PE-2'] = '%sTruSeq3-PE-2.fa'%resource_root
adaptersDict['TruSeq2-PE'] = '%sTruSeq2-PE.fa'%resource_root
#adaptersDict['TruSeq2-SE'] = '%sTruSeq2-SE.fa'%resource_root
#adaptersDict['NexteraSE-iMet'] = '%sNexteraSE-iMet.fa'%resource_root
#adaptersDict['NexteraPE-iMet'] = '%sNexteraPE-iMet.fa'%resource_root



#information about the module
def info():
    '''This module is used to trimm adapters and perform the quality filtering on raw reads.'''
    print('MODULE NAME:\t%s'%__module_name__)
    print('SOURCE FILE NAME:\t%s'%__source__)
    print('MODULE VERSION:\t%s'%__version__)
    print('LICENSE:\t%s'%__license__)
    print('AUTHOR:\t%s'%__author__)
    print('EMAIL:\t%s'%__email__)


def getResourceRoot():
    """returns the to the resource directory."""
    return resource_root



def getRoot():
    """returns the to the root directory."""
    return root



def setResourceRoot(path):
    """set the path to of the resource directory."""
    global resource_root
    if os.path.isdir(path):
        resource_root = path
        if resource_root[-1] != '/':
            resource_root += '/'
        #update the dictionary with the adaptors
        updateAdapterPaths(path, debug=False)
    else:
        sys.stderr.write('ERROR: %s is not a valid directory, please check metapatfinder config file'%path)
        sys.exit(-2)
    return resource_root



def setRoot(path):
    """set the path to of the root directory."""
    global root
    if os.path.isdir(path):
        root = path
        if root[-1] != '/':
            root += '/'
        #update all the affected paths
        updatePaths(root)
    else:
        sys.stderr.write('ERROR: %s is not a valid directory, please check metapatfinder config file'%path)
        sys.exit(-2)
    return root



def updatePaths(rootPath):
    global resource_root
    #directory with the adapters directory
    resource_root = '%sadapters/'%rootPath
    if not os.path.isdir(resource_root):
        sys.stderr.write('ERROR: %s is not a valid directory, please check metapatfinder config file.\n'%resource_root)
        sys.exit(-2)



def updateToolsPaths(path, debug=False):
    '''update the binary paths to the tools.'''
    global toolsPaths
    if debug:
        print('\nupdateToolsPaths START:')
        print('New directory:\n%s'%path)
    for program in toolsPaths:
        if program == 'trimmomatic':
            toolsPaths[program] = '%s%s'%(path, 'trimmomatic-0.32.jar')
        else:
            toolsPaths[program] = '%s%s'%(path, program)
        if not os.path.isfile(toolsPaths[program]):
            sys.stderr.write('ERROR: the path for %s program is not valid\n%s\nplease check metapatfinder config file.\n'%(program, toolsPaths[program]))
            sys.exit(-2)
    if debug:
        print('Paths to the trimming programs correctly updated.')



def updateAdapterPaths(path, debug=False):
    '''update the paths to adapters.'''
    global adaptersDict
    if debug:
        print('\nupdateAdapterPaths START:')
        print('New directory:\n%s'%path)
    for adapter in adaptersDict:
        adaptersDict[adapter] = '%s%s.fa'%(path, adapter)
        if not os.path.isfile(adaptersDict[adapter]):
            sys.stderr.write('ERROR: the path for %s adapter file is not valid\n%s\nplease check metapatfinder config file.\n'%(adapter, adaptersDict[adapter]))
            sys.exit(-2)
    if debug:
        print('Paths to the adapter sequence paths correctly updated.')



def trimmomaticQuality(in1, in2, out1=None, out2=None, outDir=os.getcwd(), logFile=None, adapterSeqFile=None, minlen=20, minAvgQual=25, zipOut=True, threads=4, layout='se', debug=False):
    '''
    Trim quality filtering for PE and SE reads using Trimmomatic.

    Outputs a Tuple containing the paths to the trimmed reads, stderr and stdout
    adaptersSeqs must contain the path to a fasta file containing the adapter sequences
    '''
    #let's first check the file paths
    if debug:
        print('trimmomaticQuality START:')
        print('INPUT 1 ::\t%s'%in1)
        print('INPUT 2 ::\t%s'%in2)
        print('OUTPUT 1 ::\t%s'%out1)
        print('OUTPUT 2 ::\t%s'%out2)
        print('OUT DIR ::\t%s'%outDir)
        print('ADAPTER FILE ::\t%s'%str(adapterSeqFile))
        print('MIN SEQ LEN ::\t%s'%str(minlen))
        print('MIN AVG SEQ QUAL ::\t%s'%str(minAvgQual))
        print('COMPRESS OUTPUT ::\t%s'%str(zipOut))
        print('THREADS ::\t%s'%str(threads))
        print('LAYOUT ::\t%s'%str(layout))
    #MINLEN=10 -> (ml) Reads shorter than this after trimming will be discarded.
    #AVGQUAL=0 -> (maq) Reads with average quality (after trimming) below this will be discarded.
    if adapterSeqFile != None:
        if not os.path.isfile(adapterSeqFile):
            sys.stderr.write('ERROR: the file %s containing the adapter sequences does not exist'%adapterSeqFile)
            sys.exit(-2)
    #check that file list is in a valid format
    if not os.path.isfile(in1):
        sys.stderr.write('ERROR: the input file %s does not exist'%in1)
        sys.exit(-2)
    if layout == 'pe':
        if in2 == None or not os.path.isfile(in2):
            sys.stderr.write('ERROR: the input file %s does not exist, you must provide 2 reads files for PE mode.\n'%in2)
            sys.exit(-2)
    if not os.path.isdir(outDir):
        sys.stderr.write('ERROR: the output directory %s does not exist'%outDir)
        sys.exit(-2)
    #set adapter sequences
    '''
    if adapterSeqFile == None: #use default adapters
        if layout == 'pe':
            adapterSeqFile = adaptersDict['NexteraPE-PE']
        #elif layout == 'se':
            #adapterSeqFile = adaptersDict['NexteraSE-iMet']
    '''
    #let's set the output files names
    trim1 = trimUNP1 = None
    if out1 != None:
        trim1 = out1
        trimUNP1 = 'unpaired.%s'%out1
    else:
        flds = os.path.basename(in1).split('.')
        trim1 = '%s_trimmo_trf.fastq'%('.'.join(flds[:-1]))
        trimUNP1 = '%s_trimmo_unpaired_trf.fastq'%('.'.join(flds[:-1]))
    #2nd output file
    trim2 = None
    if out2 != None:
        trim2 = out2
        trimUNP2 = 'unpaired.%s'%out2
    else:
        if layout == 'pe':
            flds = os.path.basename(in2).split('.')
            trim2 = '.'.join(flds[:-1]) + '_trimmo_trf.fastq'
            trimUNP2 = '.'.join(flds[:-1]) + '_trimmo_unpaired_trf.fastq'
    #log file
    logOut = None
    if logFile != None:
        logOut = logFile
    else:
        logOut = '%strimmomatic.log'%outDir
    #minimum length after trimming
    if minlen <= 0:
        minlen = 5
    #minimum avg read quality before trimming
    if minAvgQual <= 0:
        minAvgQual = 20
    #TRIMMOMATIC EXAMPLE
    #java -jar /imetgpfs/tools/Trimmomatic-0.32/trimmomatic-0.32.jar
    #PE
    #-threads 16
    #-phred33
    #-trimlog <path_to_log_file>
    # <path_to_input_reads_1>
    # <path_to_input_reads_2>
    # <path_to_trimmed_file_1>
    # <path_to_trimmed_unpaired_file_1>
    # <path_to_trimmed_file_2>
    # <path_to_trimmed_unpaired_file_2>
    # ILLUMINACLIP:<path_to_adapters_file>:2:30:10
    # LEADING:3 TRAILING:3
    # MAXINFO:60:0.7
    # MINLEN:10
    # AVGQUAL:20
    #cmd parameters
    cmdParams = ''
    if threads <= 1:
        threads = 4
    cmdParams = cmdParams + ' %s'%layout.upper().strip()
    cmdParams = cmdParams + ' -threads %d'%threads
    cmdParams = cmdParams + ' -phred33 '
    #cmdParams = cmdParams + ' -trimlog %s'%logOut
    #Parameters for the finla part of the command
    cmdParamsEnd = ''
    if adapterSeqFile != None:
        cmdParamsEnd = cmdParamsEnd + ' ILLUMINACLIP:%s:2:30:10'%adapterSeqFile
    cmdParamsEnd = cmdParamsEnd + ' LEADING:3 TRAILING:3'
    ######## USE MAX INFO  ########
    #cmdParamsEnd = cmdParamsEnd + ' MAXINFO:60:0.7'
    ###############################
    ######## USE SLIDING WINDOW  ########
    #SLIDINGWINDOW:<windowSize>:<requiredQuality>
    cmdParamsEnd = cmdParamsEnd + ' SLIDINGWINDOW:4:15'
    ###############################
    cmdParamsEnd = cmdParamsEnd + ' MINLEN:%s'%minlen
    cmdParamsEnd = cmdParamsEnd + ' AVGQUAL:%s'%minAvgQual
    #string defining input and output files paths
    fPathsStr = None
    if layout == 'pe':
        fPathsStr = ' %s %s %s %s %s %s '%(in1, in2, outDir+trim1, outDir+trimUNP1, outDir+trim2, outDir+trimUNP2)
    elif layout == 'se':
        fPathsStr = ' %s %s '%(in1, outDir+trim1)
    #let's write write command
    trimmoCmd = 'trimmomatic %s %s %s'%(cmdParams, fPathsStr, cmdParamsEnd)
    if debug:
        print('TRIMMOMATIC CMD:\n%s'%trimmoCmd)
    #sys.exit('DEBUG :: trimmomaticQuality')
    #EXECUTE TRIMMOMATIC
    process = subprocess.Popen(trimmoCmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_val, stderr_val = process.communicate() #get stdout and stderr
    process.wait()
    if debug:
        print('STDOUT TRIMMOMATIC:\n%s'%repr(stdout_val))
        print('STDERR TRIMMOMATIC:\n%s'%repr(stderr_val))
    if logFile != False:
        #open and write the log file
        fd = open(logOut, 'w')
        fd.write(stdout_val.decode('utf-8'))
        fd.write('\n')
        fd.write(stderr_val.decode('utf-8'))
        fd.close()
    #return the output tuple
    if layout == 'pe':
        return(outDir + trim1, outDir + trim2, stdout_val, stderr_val)
    else:
        return(outDir + trim1, None, stdout_val, stderr_val)



def trimmomaticAdapterAndQuality(in1, in2, out1=None, out2=None, outDir=os.getcwd(), logFile=None, adapterSeqFile=None, minlen=36, minAvgQual=25, zipOut=True, threads=4, layout='se', debug=False):
    '''
    Trim adaptor sequences and quality filtering for PE and SE reads using Trimmomatic program
    Outputs a Tuple containing the paths to the trimmed reads, stderr and stdout
    adaptersSeqs must contain the path to a fasta file containing the adapter sequences
    '''
    #let's first check the file paths
    if debug:
        print('trimmomaticFilter START:')
        print('INPUT 1 ::\t%s'%in1)
        print('INPUT 2 ::\t%s'%in2)
        print('OUTPUT 1 ::\t%s'%out1)
        print('OUTPUT 2 ::\t%s'%out2)
        print('OUT DIR ::\t%s'%outDir)
        print('STATS FILE ::\t%s'%str(logFile))
        print('ADAPTER SEQS ::\t%s'%repr(adapterSeqFile))
        print('MIN SEQ LEN ::\t%s'%str(minlen))
        print('MIN AVG SEQ QUAL ::\t%s'%str(minAvgQual))
        print('COMPRESS OUTPUT ::\t%s'%str(zipOut))
        print('THREADS ::\t%s'%str(threads))
        print('LAYOUT ::\t%s'%str(layout))
    #MINLEN=10 -> (ml) Reads shorter than this after trimming will be discarded.
    #AVGQUAL=0 -> (maq) Reads with average quality (after trimming) below this will be discarded.
    #check that file list is in a valid format
    if adapterSeqFile != None:
        if not os.path.isfile(adapterSeqFile):
            sys.stderr.write('ERROR: the file %s containing the adapter sequences does not exist'%adapterSeqFile)
            sys.exit(-2)
    if not os.path.isfile(in1):
        sys.stderr.write('ERROR: the input file %s does not exist'%in1)
        sys.exit(-2)
    if layout == 'pe':
        if in2 == None or not os.path.isfile(in2):
            sys.stderr.write('ERROR: the input file %s does not exist, you must provide 2 reads files for PE mode.\n'%in2)
            sys.exit(-2)
    if not os.path.isdir(outDir):
        sys.stderr.write('ERROR: the output directory %s does not exist'%outDir)
        sys.exit(-2)
    #set adapter sequences
    if adapterSeqFile == None: #use default adapters
        if layout == 'pe':
            adapterSeqFile = adaptersDict['NexteraPE-iMet']
        elif layout == 'se':
            adapterSeqFile = adaptersDict['NexteraSE-iMet']
    #let's set the output files names
    trim1 = trimUNP1 = None
    if out1 != None:
        trim1 = out1
        trimUNP1 = out1 + '.unpaired'
    else:
        flds = os.path.basename(in1).split('.')
        trim1 = '.'.join(flds[:-1]) + '_trimmo_trf.fastq'
        trimUNP1 = '.'.join(flds[:-1]) + '_trimmo_unpaired_trf.fastq'
    #2nd output file
    trim2 = None
    if out2 != None:
        trim2 = out2
        trimUNP2 = out2 + '.unpaired'
    else:
        if layout == 'pe':
            flds = os.path.basename(in2).split('.')
            trim2 = '.'.join(flds[:-1]) + '_trimmo_trf.fastq'
            trimUNP2 = '.'.join(flds[:-1]) + '_trimmo_unpaired_trf.fastq'
    #log file
    logOut = None
    if logFile != None:
        logOut = logFile
    else:
        logOut = outDir + 'trimmomatic_trf.log'
    #minimum length after trimming
    if minlen <= 0:
        minlen = 5
    #minimum avg read quality before trimming
    if minAvgQual <= 0:
        minAvgQual = 20
    #TRIMMOMATIC EXAMPLE
    #java -jar /imetgpfs/tools/Trimmomatic-0.32/trimmomatic-0.32.jar
    #PE
    #-threads 16
    #-phred33
    #-trimlog <path_to_log_file>
    # <path_to_input_reads_1>
    # <path_to_input_reads_2>
    # <path_to_trimmed_file_1>
    # <path_to_trimmed_unpaired_file_1>
    # <path_to_trimmed_file_2>
    # <path_to_trimmed_unpaired_file_2>
    # ILLUMINACLIP:<path_to_adapters_file>:2:30:10
    # LEADING:3 TRAILING:3
    # MAXINFO:60:0.7
    # MINLEN:10
    # AVGQUAL:20
    #cmd parameters
    cmdParams = ''
    if threads <= 1:
        threads = 4
    cmdParams = cmdParams + ' %s'%layout.upper().strip()
    cmdParams = cmdParams + ' -threads %d'%threads
    cmdParams = cmdParams + ' -phred33 '
    #cmdParams = cmdParams + ' -trimlog %s'%logOut
    #Parameters for the finla part of the command
    cmdParamsEnd = ''
    cmdParamsEnd = cmdParamsEnd + ' ILLUMINACLIP:%s:2:30:10'%adapterSeqFile
    cmdParamsEnd = cmdParamsEnd + ' LEADING:3 TRAILING:3'
    ######## USE MAX INFO  ########
    #cmdParamsEnd = cmdParamsEnd + ' MAXINFO:60:0.7'
    ###############################
    ######## USE SLIDING WINDOW  ########
    #SLIDINGWINDOW:<windowSize>:<requiredQuality>
    cmdParamsEnd = cmdParamsEnd + ' SLIDINGWINDOW:4:15'
    ###############################
    cmdParamsEnd = cmdParamsEnd + ' MINLEN:%s'%minlen
    cmdParamsEnd = cmdParamsEnd + ' AVGQUAL:%s'%minAvgQual
    #string defining input and output files paths
    fPathsStr = None
    if layout == 'pe':
        fPathsStr = ' %s %s %s %s %s %s '%(in1, in2, outDir+trim1, outDir+trimUNP1, outDir+trim2, outDir+trimUNP2)
    elif layout == 'se':
        fPathsStr = ' %s %s '%(in1, outDir+trim1)
    #let's write write command
    trimmoCmd = toolsPaths['trimmomatic'] + cmdParams + fPathsStr + cmdParamsEnd
    trimmoCmd = 'java -jar %s'%trimmoCmd
    if debug:
        print('TRIMMOMATIC CMD:\n%s'%trimmoCmd)
    #EXECUTE TRIMMOMATIC
    process = subprocess.Popen(trimmoCmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_val, stderr_val = process.communicate() #get stdout and stderr
    process.wait()
    if debug:
        print('STDOUT TRIMMOMATIC:\n%s'%repr(stdout_val))
        print('STDERR TRIMMOMATIC:\n%s'%repr(stderr_val))
    if logFile != False:
        #open and write the log file
        fd = open(logOut, 'w')
        fd.write(stdout_val)
        fd.write('\n')
        fd.write(stderr_val)
        fd.close()
    #return the output tuple
    if layout == 'pe':
        return(outDir + trim1, outDir + trim2, stdout_val, stderr_val)
    else:
        return(outDir + trim1, None, stdout_val, stderr_val)



def test_update_paths(debug=False):
    #check the current path
    print(getRoot())
    print(getResourceRoot())
    #print the tools paths
    for program in toolsPaths:
        print(program)
        print(toolsPaths[program])
    #set the new root path
    print('\nCHANGE PATHS:')
    newRoot = setRoot('/user/gen-info/salvocos/projects/metapatfinder/Trimmomatic-0.32/')
    print(newRoot)
    print(getResourceRoot())
    updateToolsPaths(getRoot(), debug=debug)
    print('\nUPDATED TRIMMING TOOLS PATHS:')
    for program in toolsPaths:
        print(program)
        print(toolsPaths[program])
    #update the paths to the databases
    updateAdapterPaths(getResourceRoot())
    print('\nUPDATED ADAPTOR PATHS:')
    for adapter in adaptersDict:
        print(adapter)
        print(adaptersDict[adapter])



def test_trimmomaticFilter(debug=False):
    '''Test the trimming of raw reads using trimmomatic.'''
    outDir = '/user/gen-info/salvocos/tmp/test_reads_cleaner/'
    r1 = '%sinput/CW4919_B_L002_R1.fastq'%outDir
    r2 = '%sinput/CW4919_B_L002_R2.fastq'%outDir
    #set the program paths
    newRoot = setRoot('/user/gen-info/salvocos/projects/metapatfinder/Trimmomatic-0.32/')
    updateToolsPaths(getRoot(), debug=debug)
    updateAdapterPaths(getResourceRoot())
    #PE NEXTERA iMet
    tr1, tr2, stdOut, stdErr = trimmomaticFilter(r1, r2, out1=None, out2=None, outDir=outDir, logFile=None, adapterSeqFile=None, minlen=25, minAvgQual=24, zipOut=False, threads=32, layout='pe', debug=debug)
    #SE NEXTERA iMet
    #tr1, tr2, stdOut, stdErr = trimmomaticFilter(r1, None, out1=None, out2=None, outDir=outDir, logFile=None, adapterSeqFile=None, minlen=25, minAvgQual=24, zipOut=False, threads=32, layout='se', debug=debug)
    if debug:
        print('TRIMMING OUTPUT:')
        print(tr1)
        print(tr2)
        print(stdOut)
        print(stdErr)
