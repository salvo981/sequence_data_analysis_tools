'''This module is used to execute and analyse FastQC for raw data quality analysis.'''
import os
import sys
import sys_tools as systools


__module_name__ = 'FastaQC Handler'
__source__ = 'fastqc_handler.py'
__author__ = 'Salvatore Cosentino'
#__copyright__ = ''
__license__ = 'GPL'
__version__ = '0.8'
__maintainer__ = 'Cosentino Salvatore'
__email__ = 'salvo981@gmail.com'


#root path for files
pySrcDir = os.path.dirname(os.path.abspath(__file__))
pySrcDir += '/'
metapatfinder_root, d1, d2 = pySrcDir.rsplit('/', 2)
fastqcPath = '%s/FastQC/fastqc'%metapatfinder_root



def info():
    '''This module is used to execute and analyse FastQC for raw data quality analysis.'''
    print('MODULE NAME:\t%s'%__module_name__)
    print('SOURCE FILE NAME:\t%s'%__source__)
    print('MODULE VERSION:\t%s'%__version__)
    print('LICENSE:\t%s'%__license__)
    print('AUTHOR:\t%s'%__author__)
    print('EMAIL:\t%s'%__email__)



def basicStats2Dat(inputList, outDir=os.getcwd(), outName=None, noOutFile=False, debug=False):
    '''Takes as input a list with one or more report files created by FastQC and outputs a plotable tab separated file containing the basic statistics for each file.'''
    from collections import OrderedDict
    #let's first check the file paths
    if debug:
        print('basicStats2Dat START:')
        print('INPUT TYPE ::\t%s'%str(type(inputList)))
        print('OUTPUT DIR ::\t%s'%outDir)
        print('OUTPUT FILE NAME ::\t%s'%outName)
        print('INPUT LIST LENGTH ::\t%s'%str(len(inputList)))
    systools.makedir(outDir)
    filesToParse = []
    outDict = OrderedDict()
    #check the files exist and that they are valid text files
    for path in inputList:
        fileFmt = systools.checkFormat(path, None, debug)
        if fileFmt != 'text/plain':
            sys.stderr.write('WARNING: the file %s is not in text format and will hence be skipped'%os.path.basename(path))
        else:
            filesToParse.append(path)
    #open the output file
    outFile = None
    if outName == None:
        outFile = '%sfastqc_basic_stats.dat'%outDir
    else:
        outFile = '%s%s'%(outDir, outName)
    if debug:
        print('OUTPUT FILE PATH ::\t%s'%outFile)
    ofd = open(outFile, 'w')
    #write the file header
    ofd.write('filename\tencoding\tseq_cnt\tpoor_qual\tseq_len\tgc_pct\n')
    #lets now starts extracting the stats
    for path in filesToParse:
        outDict[path] = []
        #now let's open the report file and parse it
        fd = open(path)
        valDict = OrderedDict() #will contain the info that will compose the output lines
        for ln in fd:
            ln = ln.strip()
            flds = ln.split('\t')
            if ln[:2] == '##': #start for the report file
                continue
            elif flds[0] == 'Filename':
                valDict['filename'] = flds[-1]
                outDict[path].append('Filename\t%s'%flds[-1])
            elif flds[0] == 'File type':
                outDict[path].append('File type\t%s'%flds[-1])
            elif flds[0] == 'Encoding':
                valDict['encoding'] = flds[-1]
                outDict[path].append('Encoding\t%s'%flds[-1])
            elif flds[0] == 'Total Sequences':
                valDict['seq_cnt'] = flds[-1]
                outDict[path].append('Total Sequences\t%s'%flds[-1])
            elif flds[0] == 'Sequences flagged as poor quality':
                valDict['poor_qual'] = flds[-1]
                outDict[path].append('Sequences flagged as poor quality\t%s'%flds[-1])
            elif flds[0] == 'Sequence length':
                valDict['seq_len'] = flds[-1]
                outDict[path].append('Sequence length\t%s'%flds[-1])
            elif flds[0] == '%GC':
                valDict['gc_pct'] = flds[-1]
                outDict[path].append('%%GC\t%s'%flds[-1])
                break
        fd.close()
        #build the string
        outStr = '\t'.join(valDict.values())
        if len(outStr) > 1:
            ofd.write('%s\n'%outStr)
        if debug:
            print(outStr)
        #let's now
        del valDict
    ofd.close() #close output
    #check the content of the output dictionary
    if debug:
        sys.stdout.write('\n######\nOutput Dictionary:')
        for k in outDict:
            sys.stdout.write('\n%s'%k)
            for el in outDict[k]:
                sys.stdout.write('\n%s'%el)
        sys.stdout.write('\n######\n')
    #remove the output file if not needed
    if noOutFile:
        os.remove(outFile)
        outFile = None
    return (outFile, outDict)



def getFastqcPath():
    '''returns the path to fastqc executable.'''
    return fastqcPath



def setFastqcPath(path):
    '''set the path to the executable.'''
    global fastqcPath
    if os.path.isfile(path):
        fastqcPath = path
    else:
        sys.stderr.write('ERROR: %s is not a valid file, please check metapatfinder config file'%path)
        sys.exit(-2)
    return fastqcPath



def runFastQC(inputReads, outDir=os.getcwd(), zipOut=True, threads=1, quiteMode=False, inFormat=None, debug=False):
    '''Execute a system call of fastqc on the input raw reads.'''
    # fastqc *.gz  --format fastq -t 8 --dir .
    import subprocess as sbprc
    #first of all check if the file exists
    if not os.path.isfile(inputReads):
        sys.stderr.write('ERROR: the input file %s does not exist '%inputReads)
        sys.exit(-2)
    #let's create the output directory if not existent and if possible
    systools.makedir(outDir)
    if debug:
        print('inputReads ::\t%s'%inputReads)
        print('outDir ::\t%s'%outDir)
        print('zipOut ::\t%s'%str(zipOut))
        print('threads ::\t%s'%str(threads))
        print('quiteMode ::\t%s'%str(quiteMode))
        print('inFormat ::\t%s'%inFormat)
    #now we prepare the command to run
    cmdParams = ' --casava --dir %s '%outDir #set the directory for the temporary files
    if zipOut:
        cmdParams = cmdParams + ' --noextract '
    if quiteMode:
        cmdParams = cmdParams + ' --quiet '
    #specify which test to run using a configuration file
    fastqcRoot = os.path.dirname(fastqcPath)
    #fastqLimitFile = '%s/Configuration/iMet_limits.txt'%fastqcRoot
    #print(fastqLimitFile)
    #sys.exit('debug::runFastQC')
    #cmdParams = cmdParams + ' --limit %s '%fastqLimitFile
    #format, if specified
    if inFormat != None:
        cmdParams = cmdParams + ' --format %s '%inFormat
    if threads <= 1:
        threads = 1
    cmdParams = cmdParams + ' --threads %s '%str(threads)
    fastqcCmd = 'fastqc %s --outdir %s %s'%(inputReads, outDir, cmdParams)
    if debug:
        print('FASTQC CMD:\n%s'%fastqcCmd)
    process = sbprc.Popen(fastqcCmd, shell=True, stdout=sbprc.PIPE, stderr=sbprc.PIPE)
    stdout_value, stderr_value = process.communicate() #get stdout and stderr
    process.wait()
    #match the output name
    outArchivePath = outDir
    archiveName = os.path.basename(inputReads) #will contain the output name
    #remove '.gz' if present in the input reads
    if archiveName[-3:] == '.gz':
        archiveName = archiveName[:-3]
    if '.fastq' in archiveName:
        archiveName = archiveName.replace('.fastq', '_fastqc.zip')
        outArchivePath = outArchivePath + archiveName
    if debug:
        print('OUTPUT ARCHIVE ::\t%s'%outArchivePath)
        print('STDOUT ::\n', repr(stdout_value))
        print('STDERR :: \n', repr(stderr_value))
    return(outArchivePath, stdout_value, stderr_value)



def runFastQCMulti(inputReads, outDir=os.getcwd(), zipOut=True, threads=1, quiteMode=False, inFormat=None, debug=False):
    '''
    Reads a list of fastqc report files and creates a data files for each each report.
    inputReads :: a list with file paths
    outDir :: a single directory or a dictionary with a path for the specific report.
    inFormat :: either None, or a dictionary with the file name as key and the format as values
    '''
    #let's first check the file paths
    if debug:
        print('runFastQCMulti START:')
        print('INPUT TYPE ::\t%s'%str(type(inputReads)))
        print('INPUT LIST LENGTH ::\t%s'%str(len(inputReads)))
        print('INPUT FILES ::\t%s'%str(inputReads))
        print('INPUT FORMAT ::\t%s'%str(inFormat))
    #check that file list is in a valid format
    #it must be a dictionary with the names or labels as keys and file paths
    inListLen = len(inputReads)
    if inListLen <= 0:
        sys.stderr.write('ERROR: the input files list must contain at least one file path')
        sys.exit(-1)
    outTplList = [] #contains the information from the runFastQC execution
    #let's run fastqc for each of the files in the list
    for path in inputReads:
        outTplList.append(runFastQC(path, outDir, zipOut, threads, quiteMode, debug=True))
    return outTplList



def runFastQCparallel(inputList, outDir=os.getcwd(), zipOut=True, threads=1, quiteMode=False, inFormat=None, debug=False):
    '''Execute a system call of fastqc on the input raw reads, and runs it in parallel.'''
    import subprocess as sbprc
    #first of all check if the file exists
    if len(inputList) == 0:
        sys.stderr.write('ERROR: you must provide at least one input file')
        sys.exit(-2)
    #check the existence of the input files
    for rpath in inputList:
        if not os.path.isfile(rpath):
            sys.stderr.write('ERROR: the input file %s does not exist '%rpath)
            sys.exit(-2)
    #let's create the output directory if it does not exist and if possible
    systools.makedir(outDir)
    if debug:
        print('\nINPUT FILES:')
        for rpath in inputList:
            print(rpath)
        print('outDir ::\t%s'%outDir)
        print('zipOut ::\t%s'%str(zipOut))
        print('threads ::\t%s'%str(threads))
        print('quiteMode ::\t%s'%str(quiteMode))
        print('inFormat ::\t%s'%inFormat)
    #now we prepare the command to run
    cmdParams = ' --dir %s '%outDir #set the directory for the temporary files
    if zipOut:
        cmdParams = cmdParams + ' --noextract '
    if quiteMode:
        cmdParams = cmdParams + ' --quiet '
    #specify which test to run using a configuration file
    fastqcRoot = os.path.dirname(fastqcPath)
    fastqLimitFile = '%s/Configuration/iMet_limits.txt'%fastqcRoot
    cmdParams = '%s --limit %s '%(cmdParams, fastqLimitFile)
    #format, if specified
    if inFormat != None:
        cmdParams = cmdParams + ' --format %s '%inFormat
    if threads <= 1:
        threads = 1
    cmdParams = cmdParams + ' --threads %s '%str(threads)
    inputReads = ' '.join(inputList) #create the string that will be used in the command
    fastqcCmd = '%s %s -o %s %s'%(fastqcPath, inputReads, outDir, cmdParams)
    if debug:
        print('FASTQC CMD:\n%s'%fastqcCmd)
    process = sbprc.Popen(fastqcCmd, shell=True, stdout=sbprc.PIPE, stderr=sbprc.PIPE)
    stdout_value, stderr_value = process.communicate() #get stdout and stderr
    process.wait()
    #match the output names and paths
    archvPaths = []
    for path in inputList:
        archiveName = os.path.basename(path) #will contain the output name
        #remove '.gz' if present in the input reads
        if archiveName[-3:] == '.gz':
            archiveName = archiveName[:-3]
        if '.fastq' in archiveName:
            archiveName = archiveName.replace('.fastq', '_fastqc.zip')
            archvPaths.append(outDir + archiveName)
    if debug:
        print('OUTPUT ARCHIVES :\n')
        for rpath in archvPaths:
            print(rpath)
        print('STDOUT ::\n', repr(stdout_value))
        print('STDERR :: \n', repr(stderr_value))
    return (archvPaths, stdout_value, stderr_value)



def extractFastQCreport(archivePath, outDir=None, debug=False):
    '''
    Extract the the report file inside a FastQC generated zip archive
    Returns the path to the extract file
    '''
    import zipfile
    if debug:
        print('INPUT ARCHIVE:%s'%archivePath)
        print('OUTPUT DIR:\t%s'%outDir)
    if outDir != None:
        if outDir[-1] != '/':
            outDir = outDir + '/'
    repName = None
    outReportFile = None
    outRepPrefix = os.path.basename(archivePath)
    #remove the zip part and the fastqc substring
    outRepPrefix = outRepPrefix.replace('_fastqc.zip', '')
    fh = open(archivePath, 'rb')
    z = zipfile.ZipFile(fh)
    for name in z.namelist():
        repName = name[-15:]
        if repName == 'fastqc_data.txt':
            outReportFile = outDir+ outRepPrefix + '_' + repName
            outfile = open(outReportFile, 'wb')
            outfile.write(z.read(name))
            outfile.close()
            break
        else: continue
    fh.close()
    #print an error message in case the report file was not found
    if outReportFile[-15:] != 'fastqc_data.txt':
        sys.stderr.write('\nThe fastqc report was not found in the archive %s'%(archivePath))
    if debug:
        print('EXTRACTED FASTQC REPORT:\t%s'%outReportFile)
    return outReportFile



def extractFastQCMulti(archivePathsList, outDir=None, debug=False):
    '''
    Extract the report file inside FastQC generated zip archives
    Returns a list with the paths to the extracted report files
    '''
    #archivePaths :: a list with the paths to the archives
    import zipfile
    if debug:
        print('archivePath:')
        for path in archivePathsList:
            print('\t%s'%path)
        print('outDir ::\t%s'%outDir)
    if outDir != None:
        if outDir[-1] != '/':
            outDir = outDir + '/'
    reportsList = [] #will contain the paths to the reports
    #let's extract the report files
    for path in archivePathsList:
        repName = None
        outReportFile = None
        outRepPrefix = os.path.basename(path)
        #remove the zip part and the fastqc substring
        outRepPrefix = outRepPrefix.replace('_fastqc.zip', '')
        fh = open(path, 'rb')
        z = zipfile.ZipFile(fh)
        for name in z.namelist():
            repName = name[-15:]
            if repName == 'fastqc_data.txt':
                outReportFile = outDir+ outRepPrefix + '_' + repName
                if debug:
                    print(name)
                    print(outReportFile)
                outfile = open(outReportFile, 'wb')
                outfile.write(z.read(name))
                outfile.close()
                #add the report file path to the list
                reportsList.append(outReportFile)
                break
            else: continue
        fh.close()
        #print an error message in case the report file was not found
        if outReportFile[-15:] != 'fastqc_data.txt':
            sys.stderr.write('The fastqc report was not found in the archive %s'%(path))
    #return the path to the extracted report file
    return reportsList



def extractFastQCimages(archivePath, outDir=os.getcwd(), imgList=None, copyAll=False, debug=False):
    '''
    Extracts the images from a FastQC generated zip archive and save it in a specific directory
    imgList contains the list of images that should be copied to the output directory
    valid values for image names
    'per_base_quality.png',
    'per_tile_quality.png',
    'per_sequence_quality.png',
    'per_base_sequence_content.png',
    'per_sequence_gc_content.png',
    'per_base_n_content.png',
    'sequence_length_distribution.png',
    'duplication_levels.png',
    'adapter_content.png',
    'kmer_profiles.png'
    '''
    import zipfile
    if debug:
        print('INPUT ARCHIVE:%s'%archivePath)
        print('OUTPUT DIR:\t%s'%outDir)
        print('Chosen images:\t%s'%str(imgList))
        print('copyAll:\t%s'%str(copyAll))
    imgName = None
    imgFoundList = []
    fh = open(archivePath, 'rb')
    z = zipfile.ZipFile(fh)
    for name in z.namelist():
        if name[-1] == '/': #then it is a directory
            continue
        if 'fastqc/Images/' in name: #the it should be a file in the directory with the images
            imgName = os.path.basename(name)
            outImg = outDir+imgName #use only the basename
            if debug:
                print('outfile :: %s'%outImg)
            if copyAll or imgName in imgList:
                outfile = open(outImg, 'wb')
                outfile.write(z.read(name))
                outfile.close()
                imgFoundList.append(outImg)
            elif imgName not in imgList:
                if debug:
                    print('the image %s will not be copied...'%imgName)
    fh.close()
    if debug:
        print('%d image files found!'%len(imgFoundList))
        print('%d image file/s requested!'%len(imgList))



def extractFastQCimagesMulti(archivePathsList, outDir=os.getcwd(), imgList=None, copyAll=False, debug=False):
    '''
    Extracts the images from FastQC generated zip archives and save them in a specific directory
    imgList contains the list of images that should be copied to the output directory
    valid values for image names
    'per_base_quality.png',
    'per_tile_quality.png',
    'per_sequence_quality.png',
    'per_base_sequence_content.png',
    'per_sequence_gc_content.png',
    'per_base_n_content.png',
    'sequence_length_distribution.png',
    'duplication_levels.png',
    'adapter_content.png',
    'kmer_profiles.png'
    '''
    import zipfile
    from collections import OrderedDict
    if debug:
        print('INPUT ARCHIVES:')
        for path in archivePathsList:
            print('\t%s'%path)
        print('OUTPUT DIR:\t%s'%outDir)
        print('Chosen images:\t%s'%str(imgList))
        print('copyAll:\t%s'%str(copyAll))
    imgName = None
    tmpDict = OrderedDict() #used to store the name of the images
    for path in archivePathsList:
        outImgPrefix = os.path.basename(path)
        #remove the zip part and the fastqc substring
        outImgPrefix = outImgPrefix.replace('fastqc.zip', '')
        fh = open(path, 'rb')
        z = zipfile.ZipFile(fh)
        for name in z.namelist():
            if name[-1] == '/': #then it is a directory
                continue
            if 'fastqc/Images/' in name: #the it should be a file in the directory with the images
                imgName = os.path.basename(name)
                outImg = outDir+ outImgPrefix + imgName
                if debug:
                    print('outImg :: %s'%outImg)
                if copyAll or imgName in imgList:
                    outfile = open(outImg, 'wb')
                    outfile.write(z.read(name))
                    outfile.close()
                    if not outImgPrefix[:-1] in tmpDict:
                        tmpDict[outImgPrefix[:-1]] = []
                    tmpDict[outImgPrefix[:-1]].append(outImg)
                elif imgName not in imgList:
                    if debug:
                        print('the image %s will not be copied...'%imgName)
        fh.close()
    if debug:
        print('\nEXTRACTED IMAGES:')
        for k in tmpDict:
            print('%s\t%s'%(k, tmpDict[k]))
        print('%d image file/s requested!\n'%len(imgList))
    return tmpDict



def splitFastQCreport(reportPath, outDir=os.getcwd(), dataFilePrefix='', removeInputFile=False, debug=False):
    '''
    Reads a fastqc report file and creates a data file for each part of the report
    '''
    if debug:
        print('reportPath ::\t%s'%reportPath)
        print('outDir ::\t%s'%outDir)
        print('dataFilePrefix ::\t%s'%dataFilePrefix)
        print('removeInputFile ::\t%s'%removeInputFile)
    if not os.path.isfile(reportPath):
        sys.stderr.write('ERROR: report file %s is not a valid file '%reportPath)
        sys.exit(-2)
    #check if the output directory exist
    if not os.path.isdir(outDir):
        sys.stderr.write('ERROR: the directory %s is does not exist '%outDir)
        sys.exit(-2)
    else:
        if outDir[-1] != '/':
            outDir = outDir + '/'
    #now let's open the report file and parse it
    fd = open(reportPath)
    fdOut = None
    #will contain information about each module with the flag Passed, error or warning from FastQC
    summaryList = []
    outFilesList = [] #will contain the paths to the extracted data files
    for ln in fd:
        outfname = None
        ln = ln.strip()
        if ln[:2] == '##':
            continue
        elif ln[:2] == '>>':
            if ln[2:5] == 'END': #then we have to stop writing in case we where doing it
                fdOut = None
            else:
                ln = ln[2:] #trim the >> part
                flds = ln.split('\t')
                outfname = flds[0].replace(' ', '_')
                outfname = outfname.lower()
                outfname = outfname + '.txt'
                summaryList.append('%s\t%s'%(flds[0], flds[-1])) #store the summary line
                if dataFilePrefix != '':
                    outfname = dataFilePrefix + '_' + outfname
                if debug:
                    print(outfname)
                    print(outDir + outfname)
                #let close the output file if already open
                if fdOut != None:
                    fdOut.close()
                #let's now create the output files
                fdOut = open(outDir + outfname, 'w')
                outFilesList.append(outDir + outfname) #add the file path to the list
        else: #this is the part in which the current open file should written
            if fdOut != None:
                if ln[0] != '#':
                    fdOut.write(ln+'\n')
                else:
                    fdOut.write(ln[1:]+'\n')
    fd.close()
    #let's write the summary file from fastqc
    outSummary = 'fastqc_summary.txt'
    if dataFilePrefix != '':
        outSummary = dataFilePrefix + '_' + outSummary
    fdOut = open(outDir + outSummary, 'w')
    outFilesList.append(outDir + outSummary) #add the file path to the list
    for el in summaryList:
        fdOut.write('%s\n'%el)
    fdOut.close()
    #let's remove the input file if needed
    if removeInputFile:
        os.remove(reportPath)
    return outFilesList



def splitFastQCreportMulti(reportPathsList, outDir=os.getcwd(), dataFilePrefixList=None, removeInputFile=False, debug=False):
    '''
    Reads a list of fastqc report files and creates a data files for each each report
    '''
    #reportPathsList :: a list with file paths
    if reportPathsList != None:
        if not type(reportPathsList) == list:
            sys.stderr.write('ERROR: you must provide as input a list with complete paths to the fastqc report files')
            sys.exit(-2)
    dataPointsDict = {} #will contain prefix names as keys and list of data-file paths as values
    if (dataFilePrefixList == None) or (len(dataFilePrefixList) == 0):
        dataFilePrefixList = []
        #then we create the prefixes based on the path names
        for path in reportPathsList:
            prefName = os.path.basename(path)
            if prefName[-15:] == 'fastqc_data.txt':
                prefName = prefName[:-15]
                if prefName[-1] == '_':
                    prefName = prefName[:-1]
                dataFilePrefixList.append(prefName)
    if debug:
        print('\nREPORT PATHS:')
        for path in reportPathsList:
            print('\t%s'%path)
        print('outDir ::\t%s'%outDir)
        print('dataFilePrefixList:' + str(dataFilePrefixList))
        print('removeInputFile ::\t%s'%str(removeInputFile))
        print('dataFilePrefixList :: %d reportPathsList :: %d'%(len(dataFilePrefixList), len(reportPathsList)))
    #let's run let's extract the datapoints
    for i, path in  enumerate(reportPathsList):
        if debug:
            print(dataFilePrefixList[i])
            print(path)
        fList = splitFastQCreport(path, outDir, dataFilePrefixList[i], removeInputFile, debug)
        if debug:
            print(fList)
        #add the element in the output dictionary
        if not dataFilePrefixList[i] in dataPointsDict:
            dataPointsDict[dataFilePrefixList[i]] = fList
    if debug:
        print('\nEXTRACTED DATA POINTS:')
        for k in dataPointsDict:
            print('%s\t%s'%(k, dataPointsDict[k]))
    return dataPointsDict



def plotAvgBaseQualComparison(datDict, outDir=os.getcwd(), outImgNamePrefix=None, outImgFormat='pdf', imgTitle=None, qualityType='Quality', debug=False):
    '''
    Creates a plot showing the average quality per read for each read quality datapoint file
    (from FastQC) in the input Dictionary
    '''
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import pandas as pd
    if debug:
        print('DAT FILES:')
        for k in datDict:
            print('%s ::\t%s'%(k, datDict[k]))
        print('outDir ::\t%s'%outDir)
        print('imgTitle ::\t%s'%imgTitle)
        print('qualityType ::\t%s'%qualityType)
    dfTot = pd.DataFrame() #will contain the data that will be plotted
    #let's load and prepare the dataset for plotting
    for k in datDict:
        if debug:
            print('%s ::\t%s'%(k, datDict[k]))
        #load the datapoints
        tmpDf = pd.read_table(datDict[k])
        #subset the df and set the index
        tmpDf_mean = tmpDf.ix[0:, ['Base', 'Mean']]
        tmpDf_mean = tmpDf_mean.set_index(['Base'])
        #let's now add tmpDf to dfTot
        if dfTot.empty:
            dfTot = tmpDf_mean
            dfTot = dfTot.rename(columns={'Mean':k}) #rename the 'Mean' column to the key in k
            if debug:
                print(dfTot.describe())
                print(dfTot.head())
            continue
        #let's continue adding columns to dfTot
        dfTot[k] = tmpDf_mean['Mean']
    #create the figure object
    plt.figure()
    #plot command
    dfTot.plot()
    plt.rc('xtick', labelsize=10)
    plt.rc('ytick', labelsize=10)
    if imgTitle == None:
        imgTitle = 'Per base quality comparison'
    plt.title(imgTitle, fontsize=14)
    plt.xlabel('base position', fontsize=12)
    plt.ylabel(qualityType, fontsize=12)
    plt.legend(fontsize=9, loc='best')
    #set output file name
    outImgFileName = ''
    if outImgNamePrefix == None:
        outImgNamePrefix = ''
        outImgFileName = 'per_base_quality.' + outImgFormat
    else:
        outImgFileName = outImgNamePrefix + 'per_base_quality.' + outImgFormat
    plt.savefig(outDir + outImgFileName, bbox_inches='tight', dpi=160)
    plt.close()



def plotAvgReadQualComparison(datDict, outDir=os.getcwd(), outImgNamePrefix=None, outImgFormat='pdf', imgTitle=None, qualityType='Quality', debug=False):
    '''
    Creates a plot showing the average quality per read for each read quality datapoint file (from FastQC)
    in the input Dictionary
    '''
    import matplotlib as mpl
    mpl.use('pdf')
    import matplotlib.pyplot as plt
    import pandas as pd
    if debug:
        print('DAT FILES:')
        for k in datDict:
            print('%s ::\t%s'%(k, datDict[k]))
        print('outDir ::\t%s'%outDir)
        print('imgTitle ::\t%s'%imgTitle)
        print('qualityType ::\t%s'%qualityType)
    #create the figure object
    plt.figure()
    #let's loop inside the dictionary and load the data point in pandas data frames
    for k in datDict:
        if debug:
            print('%s ::\t%s'%(k, datDict[k]))
        #load the datapoints
        tmpDf = pd.read_table(datDict[k])
        #plot the datapoints
        plt.plot(tmpDf['Quality'], tmpDf['Count'], label=k)
    #let's now set the other parameters for the plot
    plt.rc('xtick', labelsize=10)
    plt.rc('ytick', labelsize=10)
    #set the title
    if imgTitle == None:
        imgTitle = 'Average per read quality comparison'
    plt.title(imgTitle, fontsize=14)
    plt.xlabel(qualityType, fontsize=12)
    plt.ylabel('Reads Count', fontsize=12)
    plt.legend(fontsize=9, loc='best')
    plt.grid(True)
    #set output file name
    outImgFileName = ''
    if outImgNamePrefix == None:
        outImgNamePrefix = ''
        outImgFileName = 'per_read_avg_quality.' + outImgFormat
    else:
        outImgFileName = outImgNamePrefix + 'per_read_avg_quality.' + outImgFormat
    #let's write the output file
    plt.savefig(outDir + outImgFileName, bbox_inches='tight', dpi=160)
    plt.close()



def test_basicStats2Dat(debug=True):
    '''
    Test the extraction of basic FastQC stats
    '''
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/ill_pe_test_no_dedupe/preprocess/'
    in1 = '/user/gen-info/salvocos/test_directory/pipeline_test/ill_pe_test_no_dedupe/preprocess/trimmed/fastqc/dnmk_m180_cw2763_r1_trimmo_trf_fastqc_data.txt'
    in2 = '/user/gen-info/salvocos/test_directory/pipeline_test/ill_pe_test_no_dedupe/preprocess/trimmed/fastqc/dnmk_m180_cw2763_r2_trimmo_trf_fastqc_data.txt'
    in3 = '/user/gen-info/salvocos/test_directory/pipeline_test/ill_pe_test_no_dedupe/preprocess/original/fastqc/dnmk_m180_cw2763_r1.indexq10_fastqc_data.txt'
    in4 = '/user/gen-info/salvocos/test_directory/pipeline_test/ill_pe_test_no_dedupe/preprocess/original/fastqc/dnmk_m180_cw2763_r2.indexq10_fastqc_data.txt'
    #test the extraction
    #single file
    #basicStats2Dat([in1], outDir, None, debug)
    #more than one file
    basicStats2Dat([in1, in2, in3, in4], outDir, 'minchia2.dat', debug)



def test_runFastQC(debug=False):
    '''
    Test the execution of FastQC
    '''
    inputRaw = '/user/gen-info/salvocos/test_directory/pipeline_test/f1.fastq'
    outDir = '/user/gen-info/salvocos/test_directory/'
    os.chdir(outDir)
    if debug:
        print('inputRaw ::\t%s'%inputRaw)
        print('outDir ::\t%s'%outDir)
    runFastQC(inputRaw, outDir, True, 2, False, None, debug)



def test_runFastQCMulti(debug=False):
    '''
    Test the execution FastQC on multiple read files
    '''
    #the last file is a bigger complete read
    inputRawList = ['/user/gen-info/salvocos/test_directory/pipeline_test/f1.fastq']
    inputRawList.append('/user/gen-info/salvocos/test_directory/pipeline_test/f2.fastq')
    inputRawList.append('/user/gen-info/salvocos/test_directory/pipeline_test/f3.fastq')
    outDir = '/user/gen-info/salvocos/test_directory/'
    if debug:
        print('INPUT FILES:')
        for el in inputRawList:
            print('\t%s'%el)
        print('OUTPUT DIR ::\t%s'%outDir)
    #Run FastQC
    runFastQCMulti(inputRawList, outDir, True, 4, False, None, debug)



def test_runFastQCparallel(debug=False):
    '''Test the execution FastQC in parallel on multiple reads files.'''
    import time
    #the last file is a bigger complete read
    start_time = time.time()
    outDir = '/user/gen-info/salvocos/tmp/test_fastqc_handler/'
    in1 = '%sinput/cw2160_r1_trim.fastq'%outDir
    in2 = '%sinput/cw2160_r2_trim.fastq'%outDir
    #binPath = getFastqcPath()
    #print(binPath)
    #binPath = setFastqcPath('/user/gen-info/salvocos/projects/metapatfinder/FastQC/fastqc')
    #print(binPath)
    #Run FastQC
    runFastQCparallel([in1, in2], outDir=outDir, zipOut=True, threads=4, quiteMode=False, inFormat=None, debug=debug)
    execTime = round(time.time() - start_time, 2)
    print('test executed in %s seconds'%execTime)



def test_extractFastQCreport(debug=False):
    '''
    test the extraction of a fastqc report from a zip file
    '''
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    os.chdir(outDir)
    testArchive = 'fastqc1.zip'
    testArchive = outDir + testArchive
    print('testArchive ::\t%s'%testArchive)
    extractFastQCreport(testArchive, outDir, debug)



def test_extractFastQCMulti(debug=False):
    '''
    Test all the extraction of fastqc report files from multiple archives
    '''
    #list with the paths to the archives
    archiveList = ['/user/gen-info/salvocos/test_directory/pipeline_test/fastqc1.zip']
    archiveList.append('/user/gen-info/salvocos/test_directory/pipeline_test/fastqc2.zip')
    archiveList.append('/user/gen-info/salvocos/test_directory/pipeline_test/fastqc3.zip')
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    if debug:
        print('INPUT ARCHIVES:')
        for el in archiveList:
            print('\t%s'%el)
        print('OUTPUT DIR ::\t%s'%outDir)
    #list with the report paths
    reportPaths = extractFastQCMulti(archiveList, outDir, debug)
    if debug:
        print('EXTRACTED ARCHIVES:')
        for el in reportPaths:
            print('\t%s'%el)
    return reportPaths



def test_extractFastQCimages(debug=False):
    '''
    test the extraction of images from a fastqc created archive
    '''
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    os.chdir(outDir)
    testArchive = '/user/gen-info/salvocos/test_directory/pipeline_test/fastqc1.zip'
    testArchive = outDir + testArchive
    if debug:
        print('testArchive ::\t%s'%testArchive)
    extractFastQCimages(testArchive, outDir, ['per_base_quality.png'], True, debug)



def test_extractFastQCimagesMulti(debug=False):
    '''
    Test all the extraction of image files from multiple fastqc report archives
    '''
    #list with the paths to the archives
    archiveList = ['/user/gen-info/salvocos/test_directory/pipeline_test/fastqc1.zip']
    archiveList.append('/user/gen-info/salvocos/test_directory/pipeline_test/fastqc2.zip')
    archiveList.append('/user/gen-info/salvocos/test_directory/pipeline_test/fastqc3.zip')
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    if debug:
        print('INPUT ARCHIVES:')
        for el in archiveList:
            print('\t%s'%el)
        print('OUTPUT DIR ::\t%s'%outDir)
    #list with the report paths
    imgDict = extractFastQCimagesMulti(archiveList, outDir, ['per_base_n_content.png', 'per_base_quality.png'], False, debug)
    return imgDict



def test_splitFastQCreport(debug=False):
    '''
    test the parsing of a fastqc report file
    '''
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    os.chdir(outDir)
    testArchive = 'test_fastqc.zip'
    testArchive = outDir + testArchive
    if debug:
        print('testArchive ::\t%s'%testArchive)
    fastqcReport = extractFastQCreport(testArchive, outDir, True)
    splitFastQCreport(fastqcReport, outDir, 'minchia', False, True)



def test_splitFastQCreportMulti(debug=False):
    '''
    Test all the extraction of datapoint files from multiple fastqc report archives
    '''
    #list with the paths to the report files
    reportPaths = ['/user/gen-info/salvocos/test_directory/pipeline_test/fastqc1.txt']
    reportPaths.append('/user/gen-info/salvocos/test_directory/pipeline_test/fastqc2.txt')
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    if debug:
        print('INPUT REPORTS:')
        for el in reportPaths:
            print('\t%s'%el)
        print('OUTPUT DIR ::\t%s'%outDir)
    #list with the report paths
    dataPointsDict = splitFastQCreportMulti(reportPaths, outDir, None, False, False)
    return dataPointsDict



def test_plotAvgBaseQualComparison(debug=True):
    '''
    Test the plotting per base quality comparison
    '''
    datDict = {'dat1':'/user/gen-info/salvocos/test_directory/pipeline_test/f1.dat'}
    datDict['dat2'] = '/user/gen-info/salvocos/test_directory/pipeline_test/f2.dat'
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    if debug:
        print('DATAPOINTS:')
        for k in datDict:
            print('%s :: \t%s'%(k, datDict[k]))
        print('OUTPUT DIR ::\t%s'%outDir)
    #create the plot
    plotAvgBaseQualComparison(datDict, outDir, None, 'pdf', None, 'Illumina phred33 quality', debug)



def test_plotAvgReadQualComparison(debug=True):
    '''
    Test the plotting capabilities of the module
    '''
    datDict = {'dat1':'/user/gen-info/salvocos/test_directory/pipeline_test/f1.dat'}
    datDict['dat2'] = '/user/gen-info/salvocos/test_directory/pipeline_test/f2.dat'
    outDir = '/user/gen-info/salvocos/test_directory/pipeline_test/'
    if debug:
        print('DATAPOINTS:')
        for k in datDict:
            print('%s :: \t%s'%(k, datDict[k]))
        print('OUTPUT DIR ::\t%s'%outDir)
    #create the plot
    plotAvgReadQualComparison(datDict, outDir, None, 'pdf', None, 'Illumina phred33 quality', debug)
