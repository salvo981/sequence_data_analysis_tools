'''Run main test functions for the reads_cleaner.py module.'''
import sys, os
import reads_cleaner as rcleaner

debug = False

rcleaner.info()

#test parallel execution
#rcleaner.test_update_paths(debug)

#run trimmomatic
rcleaner.test_trimmomaticFilter(debug=debug)
