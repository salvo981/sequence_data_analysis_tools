'''Clean samples and run fastqc to assess the quality after trimming'''
import sys
import os
from collections import OrderedDict
from operator import itemgetter
import pandas as pd

# module files
srcDir = os.path.dirname(os.path.abspath(__file__))
srcDir += '/src/'
sys.path.append(srcDir)
import sys_tools as systools
import reads_cleaner as rcleaner
import fastqc_handler as fastqc

# FUNCTIONS
def load_seq_paths(rawInDir, debug=False):
    '''Reads directory with raw reads and returns a dictionary
    sequence paths for each sample.
    '''
    pathsDict = {}
    flist = os.listdir(rawInDir)
    flist.sort()

    for fname in flist:
        smpl, d1, d2, rId, d3 = fname.split('_', 4)
        #print(smpl, rId)
        if not smpl in pathsDict:
            pR1 = os.path.join(rawInDir, fname)
            if not os.path.isfile(pR1):
                print('ERROR: the path to read 1 is not valid!\n{:s}'.format(pR1))
            pR2 = os.path.join(rawInDir, fname.replace('_R1_', '_R2_'))
            if not os.path.isfile(pR2):
                print('ERROR: the path to read 2 is not valid!\n{:s}'.format(pR2))
            pathsDict[smpl] = (pR1, pR2)

    '''
    for k, val in pathsDict.items():
        print(k)
        print(val[0])
        print(val[1])
    '''
    return pathsDict


# MAIN
debug = True
cpu = 12

prjRoot = '/home/salvocos/projects/naist_ampliconseq/analysis_correct_primers_jan2018/'

analysisRoot = os.path.join(prjRoot, 'fastqc_analysis/')
trimRoot = os.path.join(prjRoot, 'trimmed_reads/default_trimmomatic_nextera_adapters/')
finalTrimOut = os.path.join(trimRoot, 'usable_reads/')
systools.makedir(finalTrimOut)
rawFqcOutDir = os.path.join(analysisRoot, 'raw/')
trimFqcOutDir = os.path.join(analysisRoot, 'trimmed/')
metaTbl = os.path.join(prjRoot, 'tables/sample_sheet_hiruma_jan2018_qiime2_edit.tsv')
rawReadsDir = '/home/salvocos/projects/naist_ampliconseq/data_correct_primers_nov2017/raw_reads/NAISTHiruma/'

# set the path with the adapter sequences
adapterDir = os.path.join(prjRoot, 'scripts/src/adapters/')
# NexteraPE-PE.fa
adapterFile = os.path.join(adapterDir, 'NexteraPE-PE.fa')
# NexteraPE-PE Hiruma (only 1 forward and 1 reverse adapter sequence)
#adapterFile = os.path.join(adapterDir, 'NexteraPE-PE_hiruma.fa')

# load raw-reads paths
rawReadsPaths = load_seq_paths(rawReadsDir)

i = 0

# perform fastqc analysis
for smpl, pathsTpl in rawReadsPaths.items():
    i += 1
    r1, r2 = pathsTpl
    outDir = os.path.join(rawFqcOutDir, '{:s}/'.format(smpl))
    if not os.path.isdir(outDir):
        systools.makedir(outDir)

    # analyze raw reads
    # Raw 1
    #fastqc.runFastQC(r1, outDir=outDir, zipOut=True, threads=4, quiteMode=True, inFormat='fastq', debug=debug)
    # Raw 2
    #fastqc.runFastQC(r2, outDir=outDir, zipOut=True, threads=4, quiteMode=True, inFormat='fastq', debug=debug)


    outTrimDir = os.path.join(trimRoot, '{:s}/'.format(smpl))
    if not os.path.isdir(outTrimDir):
        systools.makedir(outTrimDir)
    trim1 = os.path.join(outTrimDir, os.path.basename(r1))
    trim2 = os.path.join(outTrimDir, os.path.basename(r2))
    # unpaired read files
    trim1unp = os.path.join(outTrimDir, 'unpaired.{:s}'.format(os.path.basename(r1)))
    trim2unp = os.path.join(outTrimDir, 'unpaired.{:s}'.format(os.path.basename(r2)))

    # trim and filter reads using trimmomatic
    trimLogPath = os.path.join(outTrimDir, 'trimmomatic_{:s}.log'.format(smpl))
    rcleaner.trimmomaticQuality(r1, r2, out1=os.path.basename(trim1), out2=os.path.basename(trim2), outDir=outTrimDir, logFile=trimLogPath, adapterSeqFile=adapterFile, minlen=35, minAvgQual=26, zipOut=True, threads=cpu, layout='pe', debug=debug)


    # check reads quality for flat files using fastqc
    # Trim 1
    fastqc.runFastQC(trim1, outDir=outTrimDir, zipOut=True, threads=cpu, quiteMode=True, inFormat='fastq', debug=debug)
    # Trim 2
    #fastqc.runFastQC(trim2, outDir=outTrimDir, zipOut=True, threads=cpu, quiteMode=True, inFormat='fastq', debug=debug)

    # move the usable reads to a common directory
    systools.move(trim1, finalTrimOut)
    systools.move(trim2, finalTrimOut)

    #sys.exit('DEBUG')

    # remove uncompressed reads
    os.remove(trim1unp)
    os.remove(trim2unp)

    #if i == 1:
    #    break
